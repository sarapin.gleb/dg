package ru.digitalgeneration.dgserver.security.logic.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.digitalgeneration.dgserver.business.annotations.validation.UsernameValid;
import ru.digitalgeneration.dgserver.business.entities.enums.LoginType;
import ru.digitalgeneration.dgserver.business.interfaces.HasUsernameAndType;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@UsernameValid
@Schema(title = "Авторизация запрос")
public class AuthRequest implements HasUsernameAndType {

    @NotNull
    @Schema(title = "Тип авторизации", required = true, example = "email")
    private LoginType loginType;

    @NotBlank
    @Schema(title = "Логин пользователя", description = "E-mail или телефон", required = true, example = "test@test.test")
    private String username;

    @NotBlank
    @Schema(title = "Пароль", required = true, example = "qwerty")
    private String password;
}