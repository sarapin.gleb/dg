package ru.digitalgeneration.dgserver.security.logic.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Value;

import javax.validation.constraints.NotBlank;

@Value
@Schema(title = "Авторизация через VK")
public class VkAuthRequest {

    @NotBlank
    @Schema(title = "Переадресация на адрес", required = true, example = "https://digitalgeneration.ru/")
    String redirectUri;

    @NotBlank
    @Schema(title = "Код", required = true, example = "6b23233580ed551080")
    String code;

    @NotBlank
    @Schema(title = "Роль", required = true, example = "STUDENT")
    String userRole;

    @Schema(title = "Название рекламной площадки", example = "google")
    String utmSource;

    @Schema(title = "Тип рекламы", example = "social_cpc")
    String utmMedium;

    @Schema(title = "Название рекламной кампании", example = "test")
    String utmCampaign;

    @Schema(title = "Ключевое слово, с которого начался показ объявления", example = "test")
    String utmTerm;

    @Schema(title = "Дополнительная информация", example = "test")
    String utmContent;
}