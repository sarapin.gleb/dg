package ru.digitalgeneration.dgserver.security.logic.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@Schema(title = "Дополнительная информация")
public class TokenPayload {

    @Schema(title = "Роли пользователя", example = "[ROLE_STUDENT]")
    private Set<String> roles;

    @Schema(title = "Начислен ли бонус", example = "true")
    private boolean isBonusAccrued;

    @Schema(title = "Произошла ли в результате регистрация через VK впервые", example = "true")
    boolean isFirstVkRegistration;

    public TokenPayload(Set<String> roles) {
        this.roles = roles;
        this.isBonusAccrued = false;
        this.isFirstVkRegistration = false;
    }
}