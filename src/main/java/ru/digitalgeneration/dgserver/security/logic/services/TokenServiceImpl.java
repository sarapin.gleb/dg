package ru.digitalgeneration.dgserver.security.logic.services;

import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;
import ru.digitalgeneration.dgserver.business.exceptions.InvalidLoginFormatException;
import ru.digitalgeneration.dgserver.business.exceptions.not_found_exceptions.UserNotFoundException;
import ru.digitalgeneration.dgserver.business.projection.UserJwtIdProjection;
import ru.digitalgeneration.dgserver.business.repositories.jpa_repositories.UserJpaRepository;
import ru.digitalgeneration.dgserver.business.repositories.repositories.UserRepository;
import ru.digitalgeneration.dgserver.security.logic.dto.AuthRequest;
import ru.digitalgeneration.dgserver.security.logic.dto.AuthResponse;
import ru.digitalgeneration.dgserver.security.logic.dto.TokenPayload;
import ru.digitalgeneration.dgserver.security.logic.jwt.JwtTokenService;

import javax.validation.constraints.NotBlank;
import java.util.function.Function;

@Service
@Validated
@RequiredArgsConstructor
public class TokenServiceImpl implements TokenService {

    private final AuthenticationManager authenticationManager;
    private final UserDetailsService userDetailsService;
    private final JwtTokenService jwtService;
    private final UserRepository userRepository;
    private final UserJpaRepository userJpaRepository;

    @Override
    public AuthResponse getAuthResponseByUsername(AuthRequest request) {
        switch (request.getLoginType()) {
            case EMAIL:
                return getAuthResponse(
                        request.getUsername(),
                        request.getPassword(),
                        userJpaRepository::findUserJwtIdByEmail,
                        userRepository::getTokenPayloadByEmail
                );
            case PHONE:
                return getAuthResponse(
                        request.getUsername(),
                        request.getPassword(),
                        userJpaRepository::findUserJwtIdByPhone,
                        userRepository::getTokenPayloadByPhone
                );
            default:
                throw new InvalidLoginFormatException("Login (" + request.getLoginType() + ") format is wrong");
        }
    }

    @Override
    public String getJwtTokenByJwtIdAndPassword(@NotBlank String jwtId, @NotBlank String password) {
        try {
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(jwtId, password)
            );
            UserDetails userDetails = (UserDetails) authentication.getPrincipal();
            return jwtService.generateToken(userDetails);
        } catch (BadCredentialsException e) {
            throw new RuntimeException("Incorrect jwtId or password", e);
        }
    }

    @Override
    public String getJwtTokenByJwtId(@NotBlank String jwtId) {
        final UserDetails userDetails = userDetailsService.loadUserByUsername(jwtId);
        return jwtService.generateToken(userDetails);
    }

    private AuthResponse getAuthResponse(String username,
                                         String password,
                                         Function<String, UserJwtIdProjection> jwtFunction,
                                         Function<String, TokenPayload> payloadFunction) {
        UserJwtIdProjection jwtIdProjection = jwtFunction.apply(username);
        if (jwtIdProjection == null) {
            throw new UserNotFoundException();
        }
        String jwt = getJwtTokenByJwtIdAndPassword(jwtFunction.apply(username).getJwtId(), password);
        TokenPayload tokenPayload = payloadFunction.apply(username);
        return new AuthResponse(jwt, tokenPayload);
    }
}