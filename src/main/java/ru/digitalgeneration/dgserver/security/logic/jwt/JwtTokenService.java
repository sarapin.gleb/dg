package ru.digitalgeneration.dgserver.security.logic.jwt;

import org.springframework.security.core.userdetails.UserDetails;

public interface JwtTokenService {
    String generateToken(UserDetails userDetails);
}