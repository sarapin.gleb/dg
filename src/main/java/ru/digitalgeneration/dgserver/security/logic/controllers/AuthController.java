package ru.digitalgeneration.dgserver.security.logic.controllers;

import com.mashape.unirest.http.exceptions.UnirestException;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ru.digitalgeneration.dgserver.business.annotations.JwtAuthSwagger;
import ru.digitalgeneration.dgserver.business.annotations.validation.Phone;
import ru.digitalgeneration.dgserver.business.dto.requests.ChangePasswordInPersonalAreaRequest;
import ru.digitalgeneration.dgserver.business.dto.requests.ChangePasswordRequest;
import ru.digitalgeneration.dgserver.business.dto.requests.PasswordRecoveryRequest;
import ru.digitalgeneration.dgserver.business.dto.responses.BonusInfoResponse;
import ru.digitalgeneration.dgserver.business.dto.responses.CodeComparingForPasswordRecoveryResponse;
import ru.digitalgeneration.dgserver.business.services.UserService;
import ru.digitalgeneration.dgserver.business.services.VkService;
import ru.digitalgeneration.dgserver.security.logic.dto.AuthRequest;
import ru.digitalgeneration.dgserver.security.logic.dto.AuthResponse;
import ru.digitalgeneration.dgserver.security.logic.dto.VkAuthRequest;
import ru.digitalgeneration.dgserver.security.logic.services.TokenService;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.io.IOException;

@RestController
@RequiredArgsConstructor
@RequestMapping("/rest")
@Validated
@Tag(name = "Аутентификация")
public class AuthController {

    private final TokenService tokenService;
    private final UserService userService;
    private final VkService vkService;

    @Operation(summary = "Аутентификация")
    @ApiResponse(responseCode = "200", description = "Successful operation",
            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = AuthResponse.class)))
    @PutMapping("/authenticate")
    public AuthResponse createAuthenticationToken(@RequestBody @Valid AuthRequest authRequest) {
        return tokenService.getAuthResponseByUsername(authRequest);
    }

    @Operation(summary = "Отправка письма со ссылкой на страницу восстановления пароля")
    @PostMapping(path = "/password/recovery-email")
    public void sendPasswordRecoveryEmail(@RequestBody @Valid PasswordRecoveryRequest passwordRecoveryRequest) {
        userService.sendPasswordRecoveryEmail(passwordRecoveryRequest);
    }

    @Operation(summary = "Изменение пароля")
    @ApiResponse(responseCode = "200", description = "Successful operation",
            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = AuthResponse.class)))
    @PutMapping(path = "/password/changing")
    public AuthResponse changePassword(@RequestBody @Valid ChangePasswordRequest changePasswordRequest) throws IOException {
        return userService.changePassword(
                changePasswordRequest.getLogin(),
                changePasswordRequest.getLoginType(),
                changePasswordRequest.getNewPassword(),
                changePasswordRequest.getUuid()
        );
    }

    @Secured(value = {"ROLE_STUDENT", "ROLE_ENTREPRENEUR"})
    @JwtAuthSwagger
    @Operation(summary = "Изменение пароля пользователя в ЛК")
    @PutMapping(path = "/users/password/changing")
    public void changePasswordInPersonalArea(@RequestBody @Valid ChangePasswordInPersonalAreaRequest changePasswordRequest) {
        userService.changePasswordInPersonalArea(changePasswordRequest);
    }

    @Operation(summary = "Генерация и отправка кода для восстановления пароля по телефону")
    @ApiResponse(responseCode = "204", description = "Successful operation")
    @PutMapping(path = "/codes/password-recovery")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void generatePasswordRecoveryCode(@RequestParam @Phone String phone) {
        userService.generatePasswordRecoveryCode(phone);
    }

    @Operation(summary = "Сравнение кода, введенного пользователем, с сохраненным в базе для восстановления пароля по телефону")
    @ApiResponse(responseCode = "200", description = "Successful operation",
            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = CodeComparingForPasswordRecoveryResponse.class)))
    @GetMapping(path = "/codes/password-recovery")
    public CodeComparingForPasswordRecoveryResponse compareCodes(@RequestParam @Phone String phone, @NotBlank String code) {
        return userService.compareCodes(phone, code);
    }

    @Operation(summary = "Аутентификация через VK")
    @ApiResponse(responseCode = "200", description = "Successful operation",
            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = AuthResponse.class)))
    @PutMapping("/authenticate/vk")
    public AuthResponse vkAuthentication(@RequestBody @Valid VkAuthRequest authRequest) throws UnirestException, IOException {
        return vkService.getAuthResponseByVk(authRequest);
    }

    @Secured(value = {"ROLE_STUDENT", "ROLE_ENTREPRENEUR"})
    @JwtAuthSwagger
    @Operation(summary = "Привязка учетной записи VK")
    @ApiResponse(responseCode = "200", description = "Successful operation",
            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = BonusInfoResponse.class)))
    @PutMapping(path = "/users/vk")
    public BonusInfoResponse addVkToUser(@RequestBody @Valid VkAuthRequest authRequest) throws UnirestException, IOException {
        return vkService.addVkToUser(authRequest);
    }
}