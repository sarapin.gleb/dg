package ru.digitalgeneration.dgserver.security.logic.services;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.digitalgeneration.dgserver.business.exceptions.not_found_exceptions.UserNotFoundException;
import ru.digitalgeneration.dgserver.business.repositories.jpa_repositories.UserJpaRepository;
import ru.digitalgeneration.dgserver.security.config.DbUserDetails;

@Service
@RequiredArgsConstructor
public class DbUserDetailsService implements UserDetailsService {

    private final UserJpaRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String jwtId) throws UsernameNotFoundException {
        try {
            return new DbUserDetails(userRepository.findByJwtId(jwtId).orElseThrow(UserNotFoundException::new));
        } catch (RuntimeException e) {
            throw new UsernameNotFoundException(e.getMessage());
        }
    }

    public static Authentication getAuthentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }
}