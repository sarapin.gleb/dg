package ru.digitalgeneration.dgserver.security.logic.services;

import ru.digitalgeneration.dgserver.security.logic.dto.AuthRequest;
import ru.digitalgeneration.dgserver.security.logic.dto.AuthResponse;

import javax.validation.constraints.NotBlank;

public interface TokenService {

    AuthResponse getAuthResponseByUsername(AuthRequest authRequest);

    String getJwtTokenByJwtIdAndPassword(@NotBlank String username, @NotBlank String password);

    String getJwtTokenByJwtId(@NotBlank String jwtId);
}