package ru.digitalgeneration.dgserver.security.logic.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Value;

@Value
@Schema(title = "Авторизация ответ")
public class AuthResponse {

    @Schema(title = "Токен")
    String jwt;

    @Schema(title = "Дополнительная информация")
    TokenPayload tokenPayload;
}