package ru.digitalgeneration.dgserver.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "mixpanel")
@Data
public class MixpanelProperties {

    private String token;
}