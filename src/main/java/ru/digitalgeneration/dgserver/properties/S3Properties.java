package ru.digitalgeneration.dgserver.properties;

import lombok.*;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "s3")
@Data
public class S3Properties {

    private String serviceEndpoint;
    private String signingRegion;
    private Credentials credentials = new Credentials();

    @Data
    public static class Credentials {
        private String accessKey;
        private String secretKey;
    }
}