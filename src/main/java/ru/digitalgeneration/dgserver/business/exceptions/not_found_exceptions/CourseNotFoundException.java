package ru.digitalgeneration.dgserver.business.exceptions.not_found_exceptions;

import org.webjars.NotFoundException;

public class CourseNotFoundException extends NotFoundException {

    private static final String msg = "Course not found";

    public CourseNotFoundException() {
        super(msg);
    }
}