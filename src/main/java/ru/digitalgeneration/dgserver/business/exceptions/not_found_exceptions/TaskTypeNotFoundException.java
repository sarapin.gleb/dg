package ru.digitalgeneration.dgserver.business.exceptions.not_found_exceptions;

import org.webjars.NotFoundException;

public class TaskTypeNotFoundException extends NotFoundException {

    private static final String msg = "Task type not found";

    public TaskTypeNotFoundException() {
        super(msg);
    }
}