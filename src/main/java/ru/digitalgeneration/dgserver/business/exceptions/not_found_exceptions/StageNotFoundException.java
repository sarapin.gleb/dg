package ru.digitalgeneration.dgserver.business.exceptions.not_found_exceptions;

import org.webjars.NotFoundException;

public class StageNotFoundException extends NotFoundException {

    private static final String msg = "Stage not found";

    public StageNotFoundException() {
        super(msg);
    }
}