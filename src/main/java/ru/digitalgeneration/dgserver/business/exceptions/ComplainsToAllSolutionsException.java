package ru.digitalgeneration.dgserver.business.exceptions;

public class ComplainsToAllSolutionsException extends RuntimeException {
    public ComplainsToAllSolutionsException(String msg) {
        super(msg);
    }
}