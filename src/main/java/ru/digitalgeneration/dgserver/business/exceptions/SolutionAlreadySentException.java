package ru.digitalgeneration.dgserver.business.exceptions;

public class SolutionAlreadySentException extends RuntimeException {
    public SolutionAlreadySentException(String msg) {
        super(msg);
    }
}