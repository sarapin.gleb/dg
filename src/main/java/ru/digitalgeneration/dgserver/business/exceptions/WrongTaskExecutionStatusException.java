package ru.digitalgeneration.dgserver.business.exceptions;

public class WrongTaskExecutionStatusException extends RuntimeException {
    public WrongTaskExecutionStatusException(String msg) {
        super(msg);
    }
}