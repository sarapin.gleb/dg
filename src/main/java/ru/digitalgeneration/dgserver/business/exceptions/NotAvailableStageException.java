package ru.digitalgeneration.dgserver.business.exceptions;

public class NotAvailableStageException extends RuntimeException {
    public NotAvailableStageException(String msg) {
        super(msg);
    }
}