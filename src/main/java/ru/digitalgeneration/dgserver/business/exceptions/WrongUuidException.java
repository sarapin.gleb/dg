package ru.digitalgeneration.dgserver.business.exceptions;

public class WrongUuidException extends RuntimeException {
    public WrongUuidException(String msg) {
        super(msg);
    }
}