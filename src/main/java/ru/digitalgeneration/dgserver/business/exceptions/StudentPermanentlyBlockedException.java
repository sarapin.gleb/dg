package ru.digitalgeneration.dgserver.business.exceptions;

public class StudentPermanentlyBlockedException extends RuntimeException {
    public StudentPermanentlyBlockedException(String msg) {
        super(msg);
    }
}