package ru.digitalgeneration.dgserver.business.exceptions;

public class InsufficientFundsException extends RuntimeException {
    public InsufficientFundsException(String msg) {
        super(msg);
    }
}