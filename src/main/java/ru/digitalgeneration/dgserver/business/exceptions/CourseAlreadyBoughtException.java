package ru.digitalgeneration.dgserver.business.exceptions;

public class CourseAlreadyBoughtException extends RuntimeException {
    public CourseAlreadyBoughtException(String msg) {
        super(msg);
    }
}