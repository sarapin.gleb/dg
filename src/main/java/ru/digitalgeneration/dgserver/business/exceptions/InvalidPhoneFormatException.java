package ru.digitalgeneration.dgserver.business.exceptions;

public class InvalidPhoneFormatException extends RuntimeException {
    public InvalidPhoneFormatException(String msg) {
        super(msg);
    }
}