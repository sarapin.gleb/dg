package ru.digitalgeneration.dgserver.business.exceptions;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class StudentTemporarilyBlockedException extends RuntimeException {

    private Integer secondsToUnlock;

    public StudentTemporarilyBlockedException(String msg, Integer secondsToUnlock) {
        super(msg);
        this.secondsToUnlock = secondsToUnlock;
    }
}