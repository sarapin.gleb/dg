package ru.digitalgeneration.dgserver.business.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import ru.digitalgeneration.dgserver.business.dto.NotEnoughPointsExceptionPayload;
import ru.digitalgeneration.dgserver.business.dto.StudentTemporarilyBlockedExceptionPayload;
import ru.digitalgeneration.dgserver.business.exceptions.not_found_exceptions.*;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
public class CustomExceptionHandler {
    @ExceptionHandler({
            WrongUuidException.class,
            CourseAlreadyBoughtException.class,
            NotAvailableStageException.class,
            TinkoffException.class,
            InvalidPhoneFormatException.class,
            InvalidLoginFormatException.class,
            UserDataValidationException.class,
            TaskAlreadyTakenException.class,
            TaskAssignmentException.class,
            WrongEntrepreneurException.class,
            SolutionEvaluationException.class,
            SolutionAlreadySentException.class,
            SolutionSentAfterDeadlineException.class,
            ComplaintAlreadyExistException.class,
            FileActionException.class
    })
    public ResponseEntity<String> badRequestException(RuntimeException ex, HttpServletRequest request) {
        String payload = "path: " + request.getRequestURI() + "; " + ex.getMessage();
        return new ResponseEntity<>(payload, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({NotEnoughPointsException.class})
    public ResponseEntity<NotEnoughPointsExceptionPayload> notEnoughPointsException(NotEnoughPointsException ex, HttpServletRequest request) {
        String message = "path: " + request.getRequestURI() + "; " + ex.getMessage();
        NotEnoughPointsExceptionPayload payload = new NotEnoughPointsExceptionPayload(message, ex.getServiceTitle(), ex.getMissingPoints());
        return new ResponseEntity<>(payload, HttpStatus.PAYMENT_REQUIRED);
    }

    @ExceptionHandler({InsufficientFundsException.class})
    public ResponseEntity<String> paymentRequiredException(RuntimeException ex, HttpServletRequest request) {
        String payload = "path: " + request.getRequestURI() + "; " + ex.getMessage();
        return new ResponseEntity<>(payload, HttpStatus.PAYMENT_REQUIRED);
    }

    @ExceptionHandler({
            UserNotFoundException.class,
            StageNotFoundException.class,
            CourseNotFoundException.class,
            StudentCourseNotFoundException.class,
            TaskNotFoundException.class,
            TaskTypeNotFoundException.class,
            SolutionNotFoundException.class
    })
    public ResponseEntity<String> notFoundException(RuntimeException ex, HttpServletRequest request) {
        String payload = "path: " + request.getRequestURI() + "; " + ex.getMessage();
        return new ResponseEntity<>(payload, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({
            VkIdAlreadyExistsException.class,
            LoginAlreadyExistException.class,
            StudentAlreadySolvingTaskException.class,
            BestSolutionAlreadyChosenException.class,
            WrongStudentException.class
    })
    public ResponseEntity<String> conflictException(RuntimeException ex, HttpServletRequest request) {
        String payload = "path: " + request.getRequestURI() + "; " + ex.getMessage();
        return new ResponseEntity<>(payload, HttpStatus.CONFLICT);
    }

    @ExceptionHandler({UserNotVerifiedException.class})
    public ResponseEntity<String> forbiddenException(RuntimeException ex, HttpServletRequest request) {
        String payload = "path: " + request.getRequestURI() + "; " + ex.getMessage();
        return new ResponseEntity<>(payload, HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler({StudentPermanentlyBlockedException.class})
    public ResponseEntity<String> lockedException(RuntimeException ex, HttpServletRequest request) {
        String payload = "path: " + request.getRequestURI() + "; " + ex.getMessage();
        return new ResponseEntity<>(payload, HttpStatus.LOCKED);
    }

    @ExceptionHandler({ComplainsToAllSolutionsException.class})
    public ResponseEntity<String> notAcceptableException(RuntimeException ex, HttpServletRequest request) {
        String payload = "path: " + request.getRequestURI() + "; " + ex.getMessage();
        return new ResponseEntity<>(payload, HttpStatus.NOT_ACCEPTABLE);
    }

    @ExceptionHandler({StudentTemporarilyBlockedException.class})
    public ResponseEntity<StudentTemporarilyBlockedExceptionPayload> studentTemporarilyBlockedException(StudentTemporarilyBlockedException ex, HttpServletRequest request) {
        String message = "path: " + request.getRequestURI() + "; " + ex.getMessage();
        StudentTemporarilyBlockedExceptionPayload payload = new StudentTemporarilyBlockedExceptionPayload(message, ex.getSecondsToUnlock());
        return new ResponseEntity<>(payload, HttpStatus.NOT_ACCEPTABLE);
    }

    @ExceptionHandler({WrongTaskExecutionStatusException.class})
    public ResponseEntity<String> unprocessableEntityException(RuntimeException ex, HttpServletRequest request) {
        String payload = "path: " + request.getRequestURI() + "; " + ex.getMessage();
        return new ResponseEntity<>(payload, HttpStatus.UNPROCESSABLE_ENTITY);
    }

    @ExceptionHandler({TaskIsNotSpecialException.class})
    public ResponseEntity<String> preconditionFailedException(RuntimeException ex, HttpServletRequest request) {
        String payload = "path: " + request.getRequestURI() + "; " + ex.getMessage();
        return new ResponseEntity<>(payload, HttpStatus.PRECONDITION_FAILED);
    }
}