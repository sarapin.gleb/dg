package ru.digitalgeneration.dgserver.business.exceptions;

public class FileActionException extends RuntimeException {
    public FileActionException(String msg) {
        super(msg);
    }
}