package ru.digitalgeneration.dgserver.business.exceptions;

public class UserDataValidationException extends RuntimeException {
    public UserDataValidationException(String msg) {
        super(msg);
    }
}