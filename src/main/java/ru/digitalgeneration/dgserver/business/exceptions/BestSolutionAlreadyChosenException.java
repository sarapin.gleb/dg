package ru.digitalgeneration.dgserver.business.exceptions;

public class BestSolutionAlreadyChosenException extends RuntimeException {
    public BestSolutionAlreadyChosenException(String msg) {
        super(msg);
    }
}