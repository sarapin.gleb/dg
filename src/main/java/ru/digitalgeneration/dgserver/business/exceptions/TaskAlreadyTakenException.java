package ru.digitalgeneration.dgserver.business.exceptions;

public class TaskAlreadyTakenException extends RuntimeException {
    public TaskAlreadyTakenException(String msg) {
        super(msg);
    }
}