package ru.digitalgeneration.dgserver.business.exceptions;

public class LoginAlreadyExistException extends RuntimeException {
    public LoginAlreadyExistException(String msg) {
        super(msg);
    }
}