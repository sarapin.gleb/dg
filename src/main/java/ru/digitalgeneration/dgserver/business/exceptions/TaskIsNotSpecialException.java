package ru.digitalgeneration.dgserver.business.exceptions;

public class TaskIsNotSpecialException extends RuntimeException {
    public TaskIsNotSpecialException(String msg) {
        super(msg);
    }
}