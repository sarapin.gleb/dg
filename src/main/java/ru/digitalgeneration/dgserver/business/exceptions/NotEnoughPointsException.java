package ru.digitalgeneration.dgserver.business.exceptions;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class NotEnoughPointsException extends RuntimeException {
    private String serviceTitle;
    private Integer missingPoints;

    public NotEnoughPointsException(String msg, String serviceTitle, Integer missingPoints) {
        super(msg);
        this.serviceTitle = serviceTitle;
        this.missingPoints = missingPoints;
    }
}