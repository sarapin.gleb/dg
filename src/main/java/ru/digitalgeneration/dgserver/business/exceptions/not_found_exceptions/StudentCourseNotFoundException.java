package ru.digitalgeneration.dgserver.business.exceptions.not_found_exceptions;

import org.webjars.NotFoundException;

public class StudentCourseNotFoundException extends NotFoundException {

    private static final String msg = "StudentCourse not found";

    public StudentCourseNotFoundException() {
        super(msg);
    }
}