package ru.digitalgeneration.dgserver.business.exceptions.not_found_exceptions;

import org.webjars.NotFoundException;

public class UserNotFoundException extends NotFoundException {

    private static final String msg = "User not found";

    public UserNotFoundException() {
        super(msg);
    }
}