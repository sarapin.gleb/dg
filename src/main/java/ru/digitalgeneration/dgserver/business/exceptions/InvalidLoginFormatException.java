package ru.digitalgeneration.dgserver.business.exceptions;

public class InvalidLoginFormatException extends RuntimeException {
    public InvalidLoginFormatException(String msg) {
        super(msg);
    }
}