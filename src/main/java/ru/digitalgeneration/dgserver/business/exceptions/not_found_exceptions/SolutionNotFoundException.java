package ru.digitalgeneration.dgserver.business.exceptions.not_found_exceptions;

import org.webjars.NotFoundException;

public class SolutionNotFoundException extends NotFoundException {

    private static final String msg = "Solution not found";

    public SolutionNotFoundException() {
        super(msg);
    }
}