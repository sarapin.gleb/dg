package ru.digitalgeneration.dgserver.business.exceptions;

public class VkIdAlreadyExistsException extends RuntimeException {
    public VkIdAlreadyExistsException(String msg) {
        super(msg);
    }
}