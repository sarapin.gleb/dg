package ru.digitalgeneration.dgserver.business.exceptions;

public class ComplaintAlreadyExistException extends RuntimeException {
    public ComplaintAlreadyExistException(String msg) {
        super(msg);
    }
}