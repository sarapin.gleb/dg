package ru.digitalgeneration.dgserver.business.exceptions;

public class UserNotVerifiedException extends RuntimeException {
    public UserNotVerifiedException(String msg) {
        super(msg);
    }
}