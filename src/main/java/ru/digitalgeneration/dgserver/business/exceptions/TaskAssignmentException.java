package ru.digitalgeneration.dgserver.business.exceptions;

public class TaskAssignmentException extends RuntimeException {
    public TaskAssignmentException(String msg) {
        super(msg);
    }
}