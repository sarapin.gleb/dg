package ru.digitalgeneration.dgserver.business.exceptions;

public class SolutionEvaluationException extends RuntimeException {
    public SolutionEvaluationException(String msg) {
        super(msg);
    }
}