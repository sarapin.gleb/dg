package ru.digitalgeneration.dgserver.business.exceptions;

public class WrongEntrepreneurException extends RuntimeException {
    public WrongEntrepreneurException(String msg) {
        super(msg);
    }
}