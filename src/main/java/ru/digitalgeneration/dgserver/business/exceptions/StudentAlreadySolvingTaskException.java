package ru.digitalgeneration.dgserver.business.exceptions;

public class StudentAlreadySolvingTaskException extends RuntimeException {
    public StudentAlreadySolvingTaskException(String msg) {
        super(msg);
    }
}