package ru.digitalgeneration.dgserver.business.exceptions;

public class TinkoffException extends RuntimeException {
    public TinkoffException(String msg) {
        super(msg);
    }
}