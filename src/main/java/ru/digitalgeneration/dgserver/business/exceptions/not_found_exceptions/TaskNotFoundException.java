package ru.digitalgeneration.dgserver.business.exceptions.not_found_exceptions;

import org.webjars.NotFoundException;

public class TaskNotFoundException extends NotFoundException {

    private static final String msg = "Task not found";

    public TaskNotFoundException() {
        super(msg);
    }
}