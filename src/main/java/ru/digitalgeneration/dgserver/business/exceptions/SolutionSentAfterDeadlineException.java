package ru.digitalgeneration.dgserver.business.exceptions;

public class SolutionSentAfterDeadlineException extends RuntimeException {
    public SolutionSentAfterDeadlineException(String msg) {
        super(msg);
    }
}