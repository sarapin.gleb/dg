package ru.digitalgeneration.dgserver.business.exceptions;

public class WrongStudentException extends RuntimeException {
    public WrongStudentException(String msg) {
        super(msg);
    }
}