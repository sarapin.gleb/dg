package ru.digitalgeneration.dgserver.business.exceptions;

public class FileUploadException extends RuntimeException {
    public FileUploadException(String msg) {
        super(msg);
    }
}