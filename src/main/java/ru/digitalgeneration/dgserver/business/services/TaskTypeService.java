package ru.digitalgeneration.dgserver.business.services;

import com.amazonaws.services.s3.AmazonS3Client;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;
import ru.digitalgeneration.dgserver.business.dto.responses.TaskTypeForEntrepreneurResponse;
import ru.digitalgeneration.dgserver.business.dto.responses.TaskTypeForStudentResponse;
import ru.digitalgeneration.dgserver.business.dto.responses.TaskTypeFormResponse;
import ru.digitalgeneration.dgserver.business.projection.TaskTypeProjection;
import ru.digitalgeneration.dgserver.business.repositories.repositories.TaskTypeRepository;
import ru.digitalgeneration.dgserver.business.utils.AuthUtils;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Validated
@RequiredArgsConstructor
public class TaskTypeService {

    private final TaskTypeRepository taskTypeRepository;
    private final AmazonS3Client s3Client;

    public List<TaskTypeForEntrepreneurResponse> getTaskTypeListByEntrepreneur() {
        return taskTypeRepository.getTaskTypeList()
                .stream()
                .map(taskType -> new TaskTypeForEntrepreneurResponse(
                        taskType.getId(),
                        taskType.getBriefDescription(),
                        s3Client.getResourceUrl(taskType.getImage().getImagePath(), taskType.getImage().getImageFileName()),
                        taskType.getTitle(),
                        taskType.getTaskValue().getId(),
                        taskType.getPrice()
                ))
                .collect(Collectors.toList());
    }

    public List<TaskTypeForStudentResponse> getTaskTypeListByStudent() {
        return taskTypeRepository.getTaskTypeList()
                .stream()
                .map(taskType -> new TaskTypeForStudentResponse(
                        taskType.getId(),
                        s3Client.getResourceUrl(taskType.getImage().getImagePath(), taskType.getImage().getImageFileName()),
                        taskType.getTitle(),
                        taskType.getTaskValue().getValue(),
                        taskType.getStudents()
                                .stream()
                                .anyMatch(user -> user.getJwtId().equals(AuthUtils.getCurrentJwtId()))))
                .sorted(Comparator.comparing(TaskTypeForStudentResponse::isOpenForStudent).reversed())
                .collect(Collectors.toList());
    }

    public TaskTypeFormResponse getTaskTypeForm(Long taskTypeId) {
        TaskTypeProjection projection = taskTypeRepository.getTaskTypeProjectionById(taskTypeId);
        return new TaskTypeFormResponse(
                projection.getTitle(),
                projection.getFormulation(),
                projection.getPrice()
        );
    }
}