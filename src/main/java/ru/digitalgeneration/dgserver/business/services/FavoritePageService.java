package ru.digitalgeneration.dgserver.business.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.digitalgeneration.dgserver.business.dto.requests.FavoritePageRequest;
import ru.digitalgeneration.dgserver.business.repositories.repositories.FavoritePageRepository;
import ru.digitalgeneration.dgserver.business.utils.AuthUtils;

@Service
@RequiredArgsConstructor
public class FavoritePageService {

    private final FavoritePageRepository favoritePageRepository;

    public void saveEmailAndUserFavoritePage(FavoritePageRequest favoritePageRequest) {
        favoritePageRepository.saveEmailAndUserFavoritePage(favoritePageRequest);
    }

    public void saveAuthorizedUserFavoritePage(String pageUrl) {
        favoritePageRepository.saveAuthorizedUserFavoritePage(pageUrl, AuthUtils.getCurrentJwtId());
    }
}