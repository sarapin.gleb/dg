package ru.digitalgeneration.dgserver.business.services;

import com.amazonaws.services.chime.model.UnprocessableEntityException;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import lombok.RequiredArgsConstructor;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.digitalgeneration.dgserver.business.dto.responses.BonusInfoResponse;
import ru.digitalgeneration.dgserver.business.entities.User;
import ru.digitalgeneration.dgserver.business.entities.UtmParameters;
import ru.digitalgeneration.dgserver.business.exceptions.VkIdAlreadyExistsException;
import ru.digitalgeneration.dgserver.business.repositories.jpa_repositories.UserJpaRepository;
import ru.digitalgeneration.dgserver.business.repositories.repositories.UserRepository;
import ru.digitalgeneration.dgserver.business.repositories.repositories.UtmParametersRepository;
import ru.digitalgeneration.dgserver.business.utils.AuthUtils;
import ru.digitalgeneration.dgserver.security.logic.dto.AuthResponse;
import ru.digitalgeneration.dgserver.security.logic.dto.TokenPayload;
import ru.digitalgeneration.dgserver.security.logic.dto.VkAuthRequest;
import ru.digitalgeneration.dgserver.security.logic.services.TokenService;

import java.io.IOException;
import java.util.Objects;
import java.util.concurrent.ThreadLocalRandom;

@Service
@RequiredArgsConstructor
public class VkService {

    private final UserJpaRepository userJpaRepository;
    private final UserRepository userRepository;
    private final UtmParametersRepository utmParametersRepository;
    private final TokenService tokenService;
    private final MixpanelService mixpanelService;

    @Value("${bonuses.profile_verification_bonus}")
    private Integer profileVerificationBonus;

    @Value("${vk.urls.get_access_token}")
    private String getAccessTokenUrl;
    @Value("${vk.urls.get_user_data}")
    private String getUserDataUrl;
    @Value("${vk.client_id}")
    private String clientId;
    @Value("${vk.client_secret}")
    private String clientSecret;
    @Value("${vk.version}")
    private String version;

    @Transactional
    public AuthResponse getAuthResponseByVk(VkAuthRequest authRequest) throws UnirestException, IOException {
        HttpResponse<JsonNode> response = getAccessDataFromVk(authRequest.getRedirectUri(), authRequest.getCode());
        int statusCode = response.getStatus();
        if (statusCode != 200) {
            throw new UnprocessableEntityException("Unprocessable Entity: " + response.getBody().getObject().toString());
        } else {
            Integer vkUserId = (Integer) response.getBody().getObject().get("user_id");
            String vkAccessToken = (String) response.getBody().getObject().get("access_token");
            String email = null;
            try {
                email = (String) response.getBody().getObject().get("email");
            } catch (JSONException ignored) {
            }
            if (!isVkUserIdAlreadyExist(vkUserId)) {
                if (userJpaRepository.existsByEmail(email)) {
                    User user = userRepository.getUserByEmail(email);
                    boolean isBonusAccrued = addVkIdAndBonusForUser(vkUserId, user);
                    return getAuthResponseWithBonusDataByVkId(vkUserId, isBonusAccrued, true);
                }
                UtmParameters utmParameters = new UtmParameters(
                        authRequest.getUtmSource(),
                        authRequest.getUtmMedium(),
                        authRequest.getUtmCampaign(),
                        authRequest.getUtmTerm(),
                        authRequest.getUtmContent()
                );
                return createUserGetAuthResponse(vkAccessToken, vkUserId, authRequest.getUserRole(), email, utmParameters);
            } else {
                return getAuthResponseWithBonusDataByVkId(vkUserId, false, false);
            }
        }
    }

    private boolean isVkUserIdAlreadyExist(Integer vkUserId) {
        return userJpaRepository.existsByVkId(vkUserId);
    }

    private boolean addVkIdAndBonusForUser(Integer vkUserId, User user) throws IOException {
        Integer bonusPoints = 0;
        if (!user.isPhoneFirstConfirmation() && user.getVkId() == null) {
            bonusPoints = profileVerificationBonus;
            mixpanelService.createBonusAccrualEvent(user);
        }
        userJpaRepository.addVkIdAndBonusForUser(vkUserId, bonusPoints, user.getJwtId());
        mixpanelService.createUserVKConfirmationEvent(user);
        return bonusPoints != 0;
    }

    private AuthResponse createUserGetAuthResponse(String vkAccessToken, Integer vkUserId, String userRole, String email, UtmParameters utmParameters) throws UnirestException, IOException {
        HttpResponse<JsonNode> responseJson = getUserDataFromVk(vkAccessToken, vkUserId);
        JSONArray responseArr = (JSONArray) responseJson.getBody().getObject().get("response");
        JSONObject response = (JSONObject) responseArr.get(0);
        String firstName = (String) response.get("first_name");
        String lastName = (String) response.get("last_name");
        String jwtId;
        do {
            jwtId = Long.toString(ThreadLocalRandom.current().nextLong());
        }
        while (isJwtIdAlreadyExist(jwtId));
        utmParametersRepository.saveUtmParameters(utmParameters);
        User user = new User(firstName, lastName, email, vkUserId, userRole, profileVerificationBonus, jwtId, utmParameters);
        user = userRepository.saveUserChanges(user);
        mixpanelService.createUserRegistrationByVkEvent(user);
        return new AuthResponse(
                getJwtTokenByVkId(vkUserId),
                new TokenPayload(user.getRoles(), true, true)
        );
    }

    private boolean isJwtIdAlreadyExist(String jwtId) {
        return userJpaRepository.existsByJwtId(jwtId);
    }

    private AuthResponse getAuthResponseWithBonusDataByVkId(Integer vkUserId, boolean isBonusAccrued, boolean isFirstVkRegistration) {
        String jwt = getJwtTokenByVkId(vkUserId);
        TokenPayload tokenPayload = userRepository.getTokenPayloadByVkId(vkUserId);
        tokenPayload.setBonusAccrued(isBonusAccrued);
        tokenPayload.setFirstVkRegistration(isFirstVkRegistration);
        return new AuthResponse(jwt, tokenPayload);
    }

    private String getJwtTokenByVkId(Integer vkUserId) {
        String jwtId = getJwtIdByVkId(vkUserId);
        return tokenService.getJwtTokenByJwtId(jwtId);
    }

    private String getJwtIdByVkId(Integer vkUserId) {
        return userJpaRepository.findUserJwtIdByVkId(vkUserId).getJwtId();
    }

    public BonusInfoResponse addVkToUser(VkAuthRequest authRequest) throws UnirestException, IOException {
        HttpResponse<JsonNode> response = getAccessDataFromVk(authRequest.getRedirectUri(), authRequest.getCode());
        int statusCode = response.getStatus();
        if (statusCode != 200) {
            throw new UnprocessableEntityException("Unprocessable Entity: " + response.getBody().getObject().toString());
        } else {
            Integer vkUserId = (Integer) response.getBody().getObject().get("user_id");
            if (userJpaRepository.existsByVkId(vkUserId)) {
                throw new VkIdAlreadyExistsException("User with VK id = " + vkUserId + " already exists");
            }
            boolean isBonusAccrued = addVkIdAndBonusForUser(vkUserId, Objects.requireNonNull(AuthUtils.getCurrentUser()));
            return new BonusInfoResponse(isBonusAccrued);
        }
    }

    public HttpResponse<JsonNode> getAccessDataFromVk(String redirectUri, String code) throws UnirestException {
        String getAccessDataFromVkUrl = getAccessTokenUrl + "?client_id=" + clientId + "&client_secret=" + clientSecret + "&redirect_uri=" + redirectUri + "&code=" + code;
        return Unirest.post(getAccessDataFromVkUrl)
                .header(HttpHeaders.COOKIE, "remixQUIC=1; remixaudio_show_alert_today=0; remixff=0; remixlang=99; remixlgck=b2a02acb4a0ca9b1ae; remixstid=2127630664_mXD4eAKN1WaHbFW7XSZtgAfAtZmQgVBQzcmdxYgbQVX; remixua=-1%7C-1%7C-1%7C2988955906")
                .asJson();
    }

    public HttpResponse<JsonNode> getUserDataFromVk(String vkAccessToken, Integer vkUserId) throws UnirestException {
        String getUserDataFromVkUrl = getUserDataUrl + "?access_token=" + vkAccessToken + "&user_id=" + vkUserId + "&v=" + version;
        return Unirest.post(getUserDataFromVkUrl)
                .header(HttpHeaders.COOKIE, "remixQUIC=1; remixaudio_show_alert_today=0; remixff=0; remixlang=99; remixlgck=b2a02acb4a0ca9b1ae; remixstid=2127630664_mXD4eAKN1WaHbFW7XSZtgAfAtZmQgVBQzcmdxYgbQVX; remixua=-1%7C-1%7C-1%7C2988955906")
                .asJson();
    }
}