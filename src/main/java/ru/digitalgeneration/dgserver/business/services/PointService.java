package ru.digitalgeneration.dgserver.business.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.digitalgeneration.dgserver.business.dto.responses.PointPriceResponse;
import ru.digitalgeneration.dgserver.business.entities.enums.PointPrice;

@Service
@RequiredArgsConstructor
public class PointService {

    public PointPriceResponse getPointPrice() {
        return new PointPriceResponse(PointPrice.POINT_PRICE.getValue());
    }
}