package ru.digitalgeneration.dgserver.business.services;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import lombok.RequiredArgsConstructor;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.digitalgeneration.dgserver.business.connections.BrokerConnection;
import ru.digitalgeneration.dgserver.business.connections.MailServiceConnection;
import ru.digitalgeneration.dgserver.business.dto.requests.UpdateTransactionRequest;
import ru.digitalgeneration.dgserver.business.dto.responses.BalanceResponse;
import ru.digitalgeneration.dgserver.business.dto.responses.PaymentUrlResponse;
import ru.digitalgeneration.dgserver.business.dto.responses.TinkoffInitResponse;
import ru.digitalgeneration.dgserver.business.entities.Transaction;
import ru.digitalgeneration.dgserver.business.entities.User;
import ru.digitalgeneration.dgserver.business.entities.enums.PaymentStatus;
import ru.digitalgeneration.dgserver.business.entities.enums.PointPrice;
import ru.digitalgeneration.dgserver.business.exceptions.TinkoffException;
import ru.digitalgeneration.dgserver.business.repositories.repositories.BillingRepository;
import ru.digitalgeneration.dgserver.business.repositories.repositories.UserRepository;
import ru.digitalgeneration.dgserver.business.utils.AuthUtils;

import javax.mail.MessagingException;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class BillingService {

    private final UserRepository userRepository;
    private final BillingRepository billingRepository;
    private final MailServiceConnection mailServiceConnection;
    private final BrokerConnection brokerConnection;
    private final MixpanelService mixpanelService;

    private static final Logger log = LogManager.getLogger();

    @Value("${tinkoff.urls.init}")
    private String initUrl;
    @Value("${tinkoff.urls.get_state}")
    private String getStateUrl;
    @Value("${tinkoff.terminal_key}")
    private String terminalKey;
    @Value("${tinkoff.password}")
    private String password;

    @Transactional
    public PaymentUrlResponse createTransaction(Integer pointsAmount) throws ExecutionException, InterruptedException {
        User user = userRepository.getUserByJwtId(AuthUtils.getCurrentJwtId());
        Integer amount = pointsAmount * PointPrice.POINT_PRICE.getValue() * 100;
        Transaction transaction = billingRepository.saveTransaction(new Transaction(amount, pointsAmount, user));
        TinkoffInitResponse tinkoffInitResponse = getTinkoffInitResponse(amount, transaction);
        transaction.setTinkoffPaymentStatus(tinkoffInitResponse.getStatus());
        transaction.setErrorCode(tinkoffInitResponse.getErrorCode());
        transaction.setPaymentId(tinkoffInitResponse.getPaymentId());
        transaction.setPaymentStatus(getStatusByTinkoffStatus(tinkoffInitResponse.getStatus()));
        return new PaymentUrlResponse(tinkoffInitResponse.getPaymentUrl());
    }

    private TinkoffInitResponse getTinkoffInitResponse(Integer amount, Transaction transaction) throws ExecutionException, InterruptedException {
        return CompletableFuture.supplyAsync(() -> {
            log.info("Send async Tinkoff Init request: transaction #" + transaction.getId() + ", method start");
            try {
                return sendOrderDataGetTinkoffInitResponse(amount, transaction);
            } catch (UnirestException e) {
                e.printStackTrace();
            }
            return null;
        }).get();
    }

    private TinkoffInitResponse sendOrderDataGetTinkoffInitResponse(Integer amount, Transaction transaction) throws UnirestException {
        HttpResponse<JsonNode> response = Unirest.post(initUrl)
                .header(HttpHeaders.CONTENT_TYPE, "application/json")
                .header(HttpHeaders.COOKIE, "merchant_api_cookieId=4f133629-300e-4224-949c-33b89c1c1876; JSESSIONID=node0pbj4c26kwkhjl05pzbvx8t5q3878944.node0")
                .body("{\r\n\"TerminalKey\": \"" + terminalKey + "\",\r\n\"Amount\": \"" + amount + "\",\r\n\"OrderId\": \"" + (transaction.getId()) + "\"\r\n}")
                .asJson();
        boolean requestSuccess = (boolean) response.getBody().getObject().get("Success");
        if (!requestSuccess) {
            String errorCode = (String) response.getBody().getObject().get("ErrorCode");
            throw new TinkoffException("ErrorCode: " + errorCode + " Message: " + response.getBody().getObject().get("Message") + " Details: " + response.getBody().getObject().get("Details"));
        } else {
            return new TinkoffInitResponse(
                    (String) response.getBody().getObject().get("ErrorCode"),
                    (String) response.getBody().getObject().get("Status"),
                    (String) response.getBody().getObject().get("PaymentId"),
                    (String) response.getBody().getObject().get("PaymentURL")
            );
        }
    }

    @Transactional
    public BalanceResponse updateBalance() {
        User user = userRepository.getUserByJwtId(AuthUtils.getCurrentJwtId());
        List<Transaction> transactionsInProcess = user.getTransactions()
                .stream()
                .filter(transaction -> transaction.getPaymentStatus() != null)
                .filter(transaction -> transaction.getPaymentStatus().equals(PaymentStatus.IN_PROCESS.getValue()))
                .collect(Collectors.toList());
        updateTransactionStatuses(user, transactionsInProcess);
        return new BalanceResponse(user.getPointBalance());
    }

    private void updateTransactionStatuses(User user, List<Transaction> transactionsInProcess) {
        transactionsInProcess.forEach(transaction -> {
            try {
                String paymentStatus = updateTinkoffState(transaction);
                if (paymentStatus.equals(PaymentStatus.PAID.getValue())) {
                    user.setPointBalance(user.getPointBalance() + transaction.getPoints());
                    user.setLastPointsPurchaseDate(LocalDateTime.now());
                    userRepository.saveUserChanges(user);
                    sendMessages(user, transaction);
                }
            } catch (UnirestException e) {
                e.printStackTrace();
            }
        });
    }

    public String updateTinkoffState(Transaction transaction) throws UnirestException {
        String paymentId = transaction.getPaymentId();
        String token = DigestUtils.sha256Hex(password + paymentId + terminalKey);
        HttpResponse<JsonNode> response = Unirest.post(getStateUrl)
                .header(HttpHeaders.CONTENT_TYPE, "application/json")
                .header(HttpHeaders.COOKIE, "merchant_api_cookieId=4f133629-300e-4224-949c-33b89c1c1876; JSESSIONID=node01la3hauyo90ta1cq9p3touo8ao6916948.node0")
                .body("{\r\n\"TerminalKey\": \"" + terminalKey + "\",\r\n\"PaymentId\": \"" + paymentId + "\",\r\n\"Token\": \"" + token + "\"\r\n}")
                .asJson();
        boolean requestSuccess = (boolean) response.getBody().getObject().get("Success");
        if (!requestSuccess) {
            String errorCode = (String) response.getBody().getObject().get("ErrorCode");
            transaction.setErrorCode(errorCode);
            billingRepository.saveTransaction(transaction);
            throw new TinkoffException("ErrorCode: " + errorCode + " Message: " + response.getBody().getObject().get("Message") + " Details: " + response.getBody().getObject().get("Details"));
        } else {
            String tinkoffPaymentStatus = (String) response.getBody().getObject().get("Status");
            String paymentStatus = getStatusByTinkoffStatus(tinkoffPaymentStatus);
            transaction.setTinkoffPaymentStatus(tinkoffPaymentStatus);
            transaction.setPaymentStatus(paymentStatus);
            transaction.setErrorCode((String) response.getBody().getObject().get("ErrorCode"));
            billingRepository.saveTransaction(transaction);
            return paymentStatus;
        }
    }

    private void sendMessages(User user, Transaction transaction) {
        if (user.getEmail() == null && user.getPhone() == null) {
            return;
        }
        if (user.getEmail().isBlank()) {
            if (!user.getPhone().isBlank()) {
                sendSuccessfulPointsPurchaseSms(user.getPhone(), transaction.getPoints());
            }
        } else {
            sendSuccessfulPointsPurchaseEmail(user.getEmail(), transaction.getPoints());
            if (user.getPhoneVerified() && !user.getEmailVerified()) {
                sendSuccessfulPointsPurchaseSms(user.getPhone(), transaction.getPoints());
            }
        }
    }

    private void sendSuccessfulPointsPurchaseSms(String phone, Integer points) {
        try {
            brokerConnection.sendSuccessfulPointsPurchaseSms(phone, points);
        } catch (UnirestException e) {
            e.printStackTrace();
        }
    }

    private void sendSuccessfulPointsPurchaseEmail(String email, Integer points) {
        try {
            mailServiceConnection.sendSuccessfulPointsPurchaseMail(email, points);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    @Transactional
    public void updateTransaction(UpdateTransactionRequest updateTransactionRequest) throws IOException {
        Transaction transaction = billingRepository.getTransactionByPaymentId(String.valueOf(updateTransactionRequest.getPaymentId()));
        String tinkoffPaymentStatus = updateTransactionRequest.getStatus();
        String paymentStatus = getStatusByTinkoffStatus(tinkoffPaymentStatus);
        transaction.setTinkoffPaymentStatus(tinkoffPaymentStatus);
        transaction.setPaymentStatus(paymentStatus);
        transaction.setErrorCode(updateTransactionRequest.getErrorCode());
        if (transaction.getAmount() != updateTransactionRequest.getAmount()) {
            transaction.setAmount(updateTransactionRequest.getAmount());
            log.error("The incoming amount does not match DB transaction amount");
        }
        billingRepository.saveTransaction(transaction);
        if (paymentStatus != null && paymentStatus.equals(PaymentStatus.PAID.getValue())) {
            User user = transaction.getUser();
            user.setPointBalance(user.getPointBalance() + transaction.getPoints());
            user.setLastPointsPurchaseDate(LocalDateTime.now());
            userRepository.saveUserChanges(user);
            if (user.getTransactions().stream().filter(tr -> tr.getPaymentStatus().equals(PaymentStatus.PAID.getValue())).count() == 1) {
                mixpanelService.createFirstPointsPurchaseEvent(user, transaction.getId(), transaction.getPoints());
            } else {
                mixpanelService.createPointsPurchaseEvent(user, transaction.getId(), transaction.getPoints());
            }
        }
    }

    private String getStatusByTinkoffStatus(String status) {
        switch (status) {
            case "CONFIRMED":
                return PaymentStatus.PAID.getValue();
            case "NEW":
            case "PREAUTHORIZING":
            case "FORM_SHOWED":
            case "AUTHORIZING":
            case "3DS_CHECKING":
            case "3DS_CHECKED":
            case "AUTH_FAIL":
            case "AUTHORIZED":
            case "REVERSING":
            case "CONFIRMING":
            case "REFUNDING":
            case "ASYNK_REFUNDING":
            case "UNKNOWN":
                return PaymentStatus.IN_PROCESS.getValue();
            case "CANCELED":
            case "REVERSED":
            case "PARTIAL_REFUNDED":
            case "REFUNDED":
            case "REJECTED":
                return PaymentStatus.CANCELED.getValue();
            default:
                return null;
        }
    }
}