package ru.digitalgeneration.dgserver.business.services;

import com.amazonaws.services.s3.AmazonS3Client;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.digitalgeneration.dgserver.business.dto.StageDto;
import ru.digitalgeneration.dgserver.business.dto.responses.*;
import ru.digitalgeneration.dgserver.business.entities.*;
import ru.digitalgeneration.dgserver.business.entities.sql_mapping_entities.StageResponseEntity;
import ru.digitalgeneration.dgserver.business.exceptions.NotAvailableStageException;
import ru.digitalgeneration.dgserver.business.projection.StudentCourseDataProjection;
import ru.digitalgeneration.dgserver.business.repositories.repositories.CourseRepository;
import ru.digitalgeneration.dgserver.business.repositories.repositories.StudentCourseRepository;
import ru.digitalgeneration.dgserver.business.utils.AuthUtils;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor
public class CourseService {

    private final CourseRepository courseRepository;
    private final StudentCourseRepository studentCourseRepository;
    private final AmazonS3Client s3Client;

    public CourseResponse getCourseById(Long courseId) {
        String jwtId = AuthUtils.getCurrentJwtId();
        Course course = courseRepository.getCourseById(courseId);
        boolean isCourseBought = courseRepository.isCourseBoughtByUser(courseId, jwtId);
        StudentCourseDataProjection projection = null;
        if (isCourseBought) {
            projection = courseRepository.getStudentCourseData(courseId, jwtId);
        }
        return new CourseResponse(
                course.getTitle(),
                course.getIntroVideo(),
                course.getAuthor(),
                course.getTags()
                        .stream()
                        .map(Tag::getValue)
                        .sorted(String::compareTo)
                        .collect(Collectors.toList()),
                course.getDuration(),
                course.getPrice(),
                course.getBriefDescription(),
                course.getReceivedSkills(),
                course.getStages()
                        .stream()
                        .sorted(Comparator.comparing(Stage::getOrderNumber))
                        .map(stage -> new StageDto(
                                stage.getTitle(),
                                stage.getDescription(),
                                stage.getDuration().plus(Duration.ofMinutes(1))))
                        .collect(Collectors.toList()),
                isCourseBought,
                isCourseBought && projection.isCompletenessStatus(),
                !isCourseBought ? null : projection.getLastOpenStageId()
        );
    }

    public StageResponse getStageById(Long stageId) {
        String jwtId = AuthUtils.getCurrentJwtId();
        StageResponseEntity response = courseRepository.getStageById(stageId, jwtId);
        if (!courseRepository.isCourseBoughtByUser(response.getCourseId(), jwtId)) {
            throw new NotAvailableStageException("The stage is not available to the user");
        }
        String stageType = response.getStageType();
        String content = null;
        if (stageType.equals("lecture")) {
            content = response.getVideo();
        }
        if (stageType.equals("test")) {
            content = response.getQuestions();
        }
        if (stageType.equals("congratulation_page")) {
            content = response.getCourseTitle();
        }
        return new StageResponse(
                response.getTitle(),
                stageType,
                response.getDescription(),
                content,
                response.getTaskTypeId(),
                response.getPrevStageId(),
                response.getPrevStageTitle(),
                response.getNextStageId(),
                response.getNextStageTitle(),
                response.getCourseId(),
                response.getLastOpenStage(),
                response.getLastViewedStage() == null || !response.getLastViewedStage().equals(stageId) ? null : response.getTiming()
        );
    }

    public List<CourseResponseFilteredByTagsForUser> getUserCourseList(String[] tags) {
        return courseRepository.getUserCoursesByTags(tags, AuthUtils.getCurrentJwtId())
                .stream()
                .map(courseTag -> new CourseResponseFilteredByTagsForUser(
                        courseTag.getCourseId(),
                        courseTag.getTitle(),
                        s3Client.getResourceUrl(courseTag.getImagePath(), courseTag.getImageFileName()),
                        courseTag.getAuthor(),
                        new ArrayList<>(Stream.of(courseTag.getTags().split(", "))
                                .sorted(String::compareTo)
                                .collect(Collectors.toList())),
                        courseTag.getCompletenessStatus(),
                        courseTag.getLastActionDate()))
                .collect(Collectors.toList());
    }

    public List<CourseResponseFilteredByTags> getCourseList(String[] tags) {
        return courseRepository.getCoursesByTags(tags, AuthUtils.getCurrentJwtId())
                .stream()
                .map(courseTag -> new CourseResponseFilteredByTags(
                        courseTag.getCourseId(),
                        courseTag.getTitle(),
                        s3Client.getResourceUrl(courseTag.getImagePath(), courseTag.getImageFileName()),
                        courseTag.getAuthor(),
                        new ArrayList<>(Stream.of(courseTag.getTags().split(", "))
                                .sorted(String::compareTo)
                                .collect(Collectors.toList())),
                        courseTag.getPrice(),
                        courseTag.isPurchaseStatus(),
                        courseTag.getCompletenessStatus()))
                .collect(Collectors.toList());
    }

    public List<CourseResponseForHomePage> getCourses() {
        List<Course> courses = courseRepository.getCourses();
        List<StudentCourse> studentCourseList = studentCourseRepository.getStudentCoursesByJwtId(AuthUtils.getCurrentJwtId());
        return courses
                .stream()
                .map(course -> new CourseResponseForHomePage(
                        course.getId(),
                        course.getTitle(),
                        s3Client.getResourceUrl(course.getImage().getImagePath(), course.getImage().getImageFileName()),
                        course.getBriefDescription(),
                        course.getTags()
                                .stream()
                                .map(Tag::getValue)
                                .collect(Collectors.toList()),
                        course.getPrice(),
                        (int) course.getStages()
                                .stream()
                                .map(Stage::getTest)
                                .filter(Objects::nonNull)
                                .map(Test::getTaskTypes)
                                .flatMap(List::stream)
                                .distinct()
                                .count(),
                        (int) course.getStages()
                                .stream()
                                .map(Stage::getLecture)
                                .filter(Objects::nonNull)
                                .count(),
                        (int) course.getStages()
                                .stream()
                                .map(Stage::getTest)
                                .filter(Objects::nonNull)
                                .count(),
                        getPassedStagesNumber(studentCourseList, course),
                        getRequiredTime(studentCourseList, course)
                ))
                .collect(Collectors.toList());
    }

    private Duration getRequiredTime(List<StudentCourse> studentCourseList, Course course) {
        StudentCourse studentCourse = studentCourseList
                .stream()
                .filter(sc -> sc.getCourse().getId().equals(course.getId()))
                .findFirst().orElse(null);
        if (studentCourse == null) {
            return null;
        } else {
            Integer lastStageOrderNumber = studentCourse.getLastOpenStage().getOrderNumber();
            return studentCourse.getCourse().getStages()
                    .stream()
                    .filter(stage -> stage.getOrderNumber() >= lastStageOrderNumber)
                    .map(Stage::getDuration)
                    .reduce(Duration.ZERO, Duration::plus);
        }
    }

    private Integer getPassedStagesNumber(List<StudentCourse> studentCourseList, Course course) {
        StudentCourse studentCourse = studentCourseList
                .stream()
                .filter(sc -> sc.getCourse().getId().equals(course.getId()))
                .findFirst().orElse(null);
        if (studentCourse == null) {
            return null;
        } else {
            Integer lastStageOrderNumber = studentCourse.getLastOpenStage().getOrderNumber();
            return (int) studentCourse.getCourse().getStages()
                    .stream()
                    .filter(stage -> stage.getOrderNumber() < lastStageOrderNumber)
                    .filter(stage -> stage.getLecture() != null || stage.getTest() != null)
                    .count();
        }
    }
}