package ru.digitalgeneration.dgserver.business.services;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.UnsupportedEncodingException;

@Service
@EnableAsync
@RequiredArgsConstructor
public class MailService {

    private final JavaMailSender javaMailSender;
    private final MailSender mailSender;
    private final TemplateEngine templateEngine;

    @Value("${spring.mail.username}")
    private String username;
    @Value("${spring.mail.personal}")
    private String personal;

    @Async
    public void sendHtmlMail(String email, String newAccountSubject, String templateName, Context context) throws MessagingException, UnsupportedEncodingException {
        MimeMessage mail = javaMailSender.createMimeMessage();
        String body = templateEngine.process(templateName, context);
        MimeMessageHelper helper = new MimeMessageHelper(mail, true);
        try {
            helper.setFrom(username, personal);
        } catch (UnsupportedEncodingException e) {
            throw new UnsupportedEncodingException(e.getMessage() + " error in mail service sendHtmlMail method");
        }
        helper.setTo(email);
        helper.setSubject(newAccountSubject);
        helper.setText(body, true);
        javaMailSender.send(mail);
    }

    @Async
    public void sendTextMail(String to, String subject, String body, String cc, String bcc) throws MessagingException {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(username);
        message.setTo(to);
        message.setText(body);
        message.setSubject(subject);
        if(cc != null) message.setCc(cc);
        if(bcc != null) message.setBcc(bcc);
        mailSender.send(message);
    }
}