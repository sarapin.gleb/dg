package ru.digitalgeneration.dgserver.business.services;

import com.amazonaws.services.s3.AmazonS3Client;
import com.mashape.unirest.http.exceptions.UnirestException;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.webjars.NotFoundException;
import ru.digitalgeneration.dgserver.business.annotations.UserProfileChange;
import ru.digitalgeneration.dgserver.business.annotations.validation.EmailConstraintValidator;
import ru.digitalgeneration.dgserver.business.annotations.validation.PhoneConstraintValidator;
import ru.digitalgeneration.dgserver.business.connections.BrokerConnection;
import ru.digitalgeneration.dgserver.business.connections.MailServiceConnection;
import ru.digitalgeneration.dgserver.business.dto.LeaderboardResponse;
import ru.digitalgeneration.dgserver.business.dto.StageWithTypeDto;
import ru.digitalgeneration.dgserver.business.dto.TaskDto;
import ru.digitalgeneration.dgserver.business.dto.UserEmailUuidDto;
import ru.digitalgeneration.dgserver.business.dto.requests.ChangePasswordInPersonalAreaRequest;
import ru.digitalgeneration.dgserver.business.dto.requests.ChangeUserInfoRequest;
import ru.digitalgeneration.dgserver.business.dto.requests.PasswordRecoveryRequest;
import ru.digitalgeneration.dgserver.business.dto.requests.UserCreationRequest;
import ru.digitalgeneration.dgserver.business.dto.responses.*;
import ru.digitalgeneration.dgserver.business.entities.*;
import ru.digitalgeneration.dgserver.business.entities.enums.GradeTag;
import ru.digitalgeneration.dgserver.business.entities.enums.LoginType;
import ru.digitalgeneration.dgserver.business.entities.enums.TaskExecutionStatus;
import ru.digitalgeneration.dgserver.business.entities.sql_mapping_entities.LeaderboardEntity;
import ru.digitalgeneration.dgserver.business.entities.sql_mapping_entities.UserGradeTagEntity;
import ru.digitalgeneration.dgserver.business.exceptions.*;
import ru.digitalgeneration.dgserver.business.projection.CourseWithStagesProjection;
import ru.digitalgeneration.dgserver.business.repositories.jpa_repositories.UserJpaRepository;
import ru.digitalgeneration.dgserver.business.repositories.repositories.*;
import ru.digitalgeneration.dgserver.business.utils.AuthUtils;
import ru.digitalgeneration.dgserver.security.logic.dto.AuthResponse;
import ru.digitalgeneration.dgserver.security.logic.dto.TokenPayload;
import ru.digitalgeneration.dgserver.security.logic.services.TokenService;

import javax.mail.MessagingException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;
    private final UserJpaRepository userJpaRepository;
    private final StageRepository stageRepository;
    private final StudentCourseRepository studentCourseRepository;
    private final CourseRepository courseRepository;
    private final SolutionRepository solutionRepository;
    private final UtmParametersRepository utmParametersRepository;
    private final BestSolutionRepository bestSolutionRepository;
    private final MailServiceConnection mailServiceConnection;
    private final BrokerConnection brokerConnection;
    private final TokenService tokenService;
    private final PasswordEncoder encoder;
    private final PhoneConstraintValidator phoneConstraintValidator;
    private final EmailConstraintValidator emailConstraintValidator;
    private final MixpanelService mixpanelService;
    private final AmazonS3Client s3Client;

    @Value("${bonuses.profile_verification_bonus}")
    private Integer profileVerificationBonus;
    @Value("${feature.fake_leaderboard}")
    private boolean FAKE_LEADERBOARD;

    @Transactional
    public AuthResponse addUser(UserCreationRequest userCreationRequest) throws IOException {
        String login = userCreationRequest.getUsername();
        LoginType loginType = userCreationRequest.getLoginType();
        if (!(
                (loginType.equals(LoginType.EMAIL) && emailConstraintValidator.emailValidation(login)) ||
                        (loginType.equals(LoginType.PHONE) && phoneConstraintValidator.phoneValidation(login))
        )) {
            throw new InvalidLoginFormatException("Login (" + loginType + ") format is wrong");
        }
        if (isLoginExist(loginType, login)) {
            throw new LoginAlreadyExistException("Login " + login + " already exists");
        }
        UUID uuid = UUID.randomUUID();
        String password = userCreationRequest.getPassword();
        String hashedPassword = encoder.encode(password);
        String jwtId;
        do {
            jwtId = Long.toString(ThreadLocalRandom.current().nextLong());
        }
        while (isJwtIdAlreadyExist(jwtId));
        UtmParameters utmParameters = new UtmParameters(
                userCreationRequest.getUtmSource(),
                userCreationRequest.getUtmMedium(),
                userCreationRequest.getUtmCampaign(),
                userCreationRequest.getUtmTerm(),
                userCreationRequest.getUtmContent()
        );
        utmParametersRepository.saveUtmParameters(utmParameters);
        User user = new User(
                loginType,
                login,
                hashedPassword,
                userCreationRequest.getRole(),
                uuid,
                jwtId,
                utmParameters
        );
        user = userRepository.saveUserChanges(user);
        AuthResponse authResponse = new AuthResponse(
                tokenService.getJwtTokenByJwtIdAndPassword(user.getJwtId(), password),
                new TokenPayload(user.getRoles())
        );
        if (loginType.equals(LoginType.EMAIL)) {
            mixpanelService.createUserRegistrationByEmailEvent(user);
            try {
                mailServiceConnection.sendVerificationMail(userCreationRequest.getLinkToConfirmationPage(), userCreationRequest.getRole(), authResponse.getJwt(), uuid, login);
            } catch (MessagingException | UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        } else {
            mixpanelService.createUserRegistrationByPhoneEvent(user);
        }
        return authResponse;
    }

    private boolean isJwtIdAlreadyExist(String jwtId) {
        return userJpaRepository.existsByJwtId(jwtId);
    }

    private boolean isLoginExist(LoginType loginType, String login) {
        if (loginType.equals(LoginType.EMAIL)) {
            return userJpaRepository.existsByEmail(login);
        } else if (loginType.equals(LoginType.PHONE)) {
            return userJpaRepository.existsByPhone(login);
        } else {
            throw new InvalidLoginFormatException("Invalid login type (" + loginType + ")");
        }
    }

    @Transactional
    public void verifyUserEmailByUuid(UUID uuid) throws IOException {
        User user = userRepository.getUserByUuid(uuid);
        user.setUuid(UUID.randomUUID());
        user.setEmailVerified(true);
        mixpanelService.createUserEmailConfirmationEvent(user);
    }

    public void sendPasswordRecoveryEmail(PasswordRecoveryRequest passwordRecoveryRequest) {
        String email = passwordRecoveryRequest.getEmail();
        UUID uuid = userRepository.getUuidByEmail(email);
        String passwordRecoveryUrl = passwordRecoveryRequest.getUrl() + "/" + uuid + "/" + email;
        try {
            mailServiceConnection.sendPasswordRecoveryEmail(email, passwordRecoveryUrl);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    @Transactional
    @UserProfileChange
    public AuthResponse changePassword(String login, LoginType loginType, String newPassword, UUID uuid) throws IOException {
        String newHashedPassword = encoder.encode(newPassword);
        User user;
        boolean isBonusAccrued = false;
        if (loginType.equals(LoginType.EMAIL)) {
            user = userRepository.getUserByEmail(login);
            uuidComparing(user, uuid);
            if (!user.getEmailVerified()) {
                verifyEmail(user);
            }
            user = changePasswordGetUser(newHashedPassword, user);
        } else if (loginType.equals(LoginType.PHONE)) {
            user = userRepository.getUserByPhone(login);
            uuidComparing(user, uuid);
            if (!user.getPhoneVerified()) {
                isBonusAccrued = verifyPhoneGetBonusInfo(user);
            }
            user = changePasswordGetUser(newHashedPassword, user);
        } else {
            throw new InvalidLoginFormatException("Login (" + loginType + ") format is wrong");
        }
        sendMessages(user);
        return new AuthResponse(
                tokenService.getJwtTokenByJwtIdAndPassword(user.getJwtId(), newPassword),
                new TokenPayload(user.getRoles(), isBonusAccrued, false)
        );
    }

    private void verifyEmail(User user) throws IOException {
        user.setEmailVerified(true);
        mixpanelService.createUserEmailConfirmationEvent(user);
    }

    private boolean verifyPhoneGetBonusInfo(User user) throws IOException {
        user.setPhoneVerified(true);
        mixpanelService.createUserPhoneConfirmationEvent(user);
        if (!user.isPhoneFirstConfirmation()) {
            user.setPhoneFirstConfirmation(true);
            if (user.getVkId() == null) {
                user.setPointBalance(user.getPointBalance() + profileVerificationBonus);
                mixpanelService.createBonusAccrualEvent(user);
                return true;
            }
        }
        return false;
    }

    private void uuidComparing(User user, UUID uuid) {
        if (!user.getUuid().equals(uuid)) {
            throw new WrongUuidException("Wrong UUID");
        }
    }

    private void sendMessages(User user) {
        if (user.getEmail() == null && user.getPhone() == null) {
            return;
        }
        if (user.getEmail() == null || user.getEmail().isBlank()) {
            if (!user.getPhone().isBlank()) {
                sendPasswordChangeConfirmationSms(user.getPhone());
            }
        } else {
            sendPasswordChangeConfirmationMail(user.getEmail());
            if (user.getPhoneVerified() && !user.getEmailVerified()) {
                sendPasswordChangeConfirmationSms(user.getPhone());
            }
        }
    }

    private void sendPasswordChangeConfirmationSms(String phone) {
        try {
            brokerConnection.sendPasswordChangeConfirmationSms(phone);
        } catch (UnirestException e) {
            e.printStackTrace();
        }
    }

    private void sendPasswordChangeConfirmationMail(String email) {
        try {
            mailServiceConnection.sendPasswordChangeConfirmationMail(email);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    private User changePasswordGetUser(String newPassword, User user) {
        user.setUuid(UUID.randomUUID());
        user.setPassword(newPassword);
        return userRepository.saveUserChanges(user);
    }

    @Transactional
    public BalanceResponse coursePurchase(Long courseId) throws IOException {
        String jwtId = AuthUtils.getCurrentJwtId();
        User user = userRepository.getUserByJwtId(jwtId);
        Course course = courseRepository.getCourseById(courseId);
        if (courseRepository.isCourseBoughtByUser(courseId, jwtId)) {
            throw new CourseAlreadyBoughtException("The course \"" + course.getTitle() + "\" has already been purchased before");
        } else {
            Stage firstStage = course.getStages()
                    .stream()
                    .filter(stage -> stage.getPrevStageId() == null)
                    .findFirst()
                    .orElseThrow(() -> new NotFoundException("There are no stages in this course"));
            Integer newBalance = user.getPointBalance() - course.getPrice();
            LocalDateTime now = LocalDateTime.now();
            if (newBalance < 0) {
                throw new NotEnoughPointsException("There are not enough points on the balance", course.getTitle(), -newBalance);
            } else {
                user.setPointBalance(newBalance);
            }
            StudentCourse studentCourse = new StudentCourse(user, course, false, firstStage, null, null, now);
            userRepository.saveUserChanges(user);
            studentCourseRepository.saveStudentCourse(studentCourse);
            sendMessages(user, course);
            if (user.getStudentCourses().size() == 1) {
                mixpanelService.createFirstCoursePurchaseEvent(user, course.getId());
            }
            return new BalanceResponse(newBalance);
        }
    }

    private void sendMessages(User user, Course course) {
        if ((user.getEmail() == null || user.getEmail().isBlank()) && (user.getPhone() == null || user.getPhone().isBlank())) {
            return;
        }
        if (user.getEmail() == null || user.getEmail().isBlank()) {
            if (!user.getPhone().isBlank()) {
                sendNewCoursePurchasingSms(user.getPhone(), course.getTitle());
            }
        } else {
            sendNewCoursePurchasingMail(user.getEmail(), course);
            if (user.getPhoneVerified() && !user.getEmailVerified()) {
                sendNewCoursePurchasingSms(user.getPhone(), course.getTitle());
            }
        }
    }

    private void sendNewCoursePurchasingSms(String phone, String courseTitle) {
        try {
            brokerConnection.sendNewCoursePurchasingSms(phone, courseTitle);
        } catch (UnirestException e) {
            e.printStackTrace();
        }
    }

    private void sendNewCoursePurchasingMail(String email, Course course) {
        try {
            mailServiceConnection.sendNewCoursePurchasingMail(
                    email,
                    course.getId(),
                    course.getTitle()
            );
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    public CourseWithStagesResponse getStagesInCourse(Long courseId) {
        CourseWithStagesProjection projection = studentCourseRepository
                .getCourseWithStagesByCourseIdAndJwtId(courseId, AuthUtils.getCurrentJwtId());
        return new CourseWithStagesResponse(
                projection.getCourseTitle(),
                projection.getLastOpenStage().getId(),
                projection.getStages()
                        .stream()
                        .map(stage -> {
                            String type = getTypeByStage(stage);
                            return new StageWithTypeDto(
                                    stage.getId(),
                                    stage.getTitle(),
                                    type,
                                    stage.getOrderNumber());
                        })
                        .sorted(Comparator.comparing(StageWithTypeDto::getOrderNumber))
                        .collect(Collectors.toList())
        );
    }

    public BalanceResponse getBalance() {
        return userRepository.getBalance(AuthUtils.getCurrentJwtId());
    }

    public void changePasswordInPersonalArea(ChangePasswordInPersonalAreaRequest changePasswordRequest) {
        String newPassword = changePasswordRequest.getNewPassword();
        String newHashedPassword = encoder.encode(newPassword);
        User updatedUser = userRepository.changePasswordInPersonalArea(AuthUtils.getCurrentJwtId(), newHashedPassword);
        sendMessages(updatedUser);
    }

    public void sendVerificationEmail(String linkToConfirmationPage) {
        UserEmailUuidDto userDto = userRepository.getUserEmailAndUuidByJwtId(AuthUtils.getCurrentJwtId());
        try {
            mailServiceConnection.sendVerificationMailFromPersonalArea(userDto.getEmail(), linkToConfirmationPage, userDto.getUuid());
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    public UserResponse getUserInfo() {
        return getUserData(AuthUtils.getCurrentJwtId());
    }

    @Transactional
    @UserProfileChange
    public VerificationDataResponse changeUserInfo(ChangeUserInfoRequest changeUserInfoRequest) {
        User user = userRepository.getUserByJwtId(AuthUtils.getCurrentJwtId());
        if (!isUserDataValid(changeUserInfoRequest.getEmail(), changeUserInfoRequest.getPhone(), user.getVkId())) {
            throw new UserDataValidationException("The email, phone and vk fields can't be empty at the same time and must be in the correct format");
        }
        if ((user.getPhone() == null || !user.getPhone().equals(changeUserInfoRequest.getPhone())) && !changeUserInfoRequest.getPhone().isEmpty() && isPhoneExist(changeUserInfoRequest.getPhone())) {
            throw new LoginAlreadyExistException("Phone " + changeUserInfoRequest.getPhone() + " already exists");
        }
        if (user.getEmail() == null || !user.getEmail().equals(changeUserInfoRequest.getEmail())) {
            user.setEmail(changeUserInfoRequest.getEmail());
            user.setEmailVerified(false);
        }
        if ((user.getPhone() == null || !user.getPhone().equals(changeUserInfoRequest.getPhone())) && !changeUserInfoRequest.getPhone().isEmpty()) {
            user.setPhone(changeUserInfoRequest.getPhone());
            user.setPhoneVerified(false);
        }
        user.setFName(changeUserInfoRequest.getFName());
        user.setSName(changeUserInfoRequest.getSName());
        user.setMName(changeUserInfoRequest.getMName());
        userRepository.saveUserChanges(user);
        return new VerificationDataResponse(user.getEmailVerified(), user.getPhoneVerified());
    }

    private boolean isPhoneExist(String phone) {
        return userRepository.isPhoneExist(phone);
    }

    private boolean isUserDataValid(String email, String phone, Integer vkId) {
        if (vkId == null) {
            if (email != null && !email.equals("") && phone != null && !phone.equals("")) {
                return phoneConstraintValidator.phoneValidation(phone) && emailConstraintValidator.emailValidation(email);
            } else if (email == null || email.equals("")) {
                return phoneConstraintValidator.phoneValidation(phone);
            } else {
                return emailConstraintValidator.emailValidation(email);
            }
        } else {
            return (phoneConstraintValidator.phoneValidation(phone) || phone == null || phone.equals("")) && (emailConstraintValidator.emailValidation(email) || email == null || email.equals(""));
        }
    }

    public void generatePhoneVerificationCode() {
        userRepository.generatePhoneVerificationCode(AuthUtils.getCurrentJwtId());
    }

    @Transactional
    public CodeComparingForPhoneVerificationResponse compareCodes(String code) throws IOException {
        User user = userRepository.getUserByJwtId(AuthUtils.getCurrentJwtId());
        boolean comparingResult = false;
        boolean isBonusAccrued = false;
        if (user.getLastNonceVerificationCode() != null && user.getLastNonceVerificationCode().equals(code)) {
            user.setPhoneVerified(true);
            user.setLastNonceVerificationCode(null);
            comparingResult = true;
            if (!user.isPhoneFirstConfirmation()) {
                user.setPhoneFirstConfirmation(true);
                if (user.getVkId() == null) {
                    user.setPointBalance(user.getPointBalance() + profileVerificationBonus);
                    mixpanelService.createBonusAccrualEvent(user);
                    isBonusAccrued = true;
                }
            }
            mixpanelService.createUserPhoneConfirmationEvent(user);
        }
        return new CodeComparingForPhoneVerificationResponse(comparingResult, isBonusAccrued);
    }

    public void generatePasswordRecoveryCode(String phone) {
        userRepository.generatePasswordRecoveryCode(phone);
    }

    public CodeComparingForPasswordRecoveryResponse compareCodes(String phone, String code) {
        return userRepository.compareCodesForPasswordRecovering(phone, code);
    }

    @Transactional
    public void completeStage(Long stageId) {
        String jwtId = AuthUtils.getCurrentJwtId();
        Stage stage = stageRepository.getStageById(stageId);
        StudentCourse studentCourse = studentCourseRepository.getStudentCourseByCourseIdAndUserJwtId(stage.getCourse().getId(), jwtId);
        if (studentCourse.getLastOpenStage().getId().equals(stageId)) {
            if (stage.getNextStageId() == null) {
                studentCourse.setCompletenessStatus(true);
            } else {
                Stage nextStage = stageRepository.getStageById(stage.getNextStageId());
                studentCourse.setLastOpenStage(nextStage);
            }
        }
        studentCourse.setLastActionDate(LocalDateTime.now());
        Test test = stage.getTest();
        if (test != null) {
            User student = userRepository.getUserByJwtId(jwtId);
            Set<TaskType> taskTypes = student.getTaskTypes();
            taskTypes.addAll(test.getTaskTypes());
            student.setTaskTypes(taskTypes);
        }
    }

    @Transactional
    public void saveTiming(Long stageId, Integer time) {
        Stage stage = stageRepository.getStageById(stageId);
        StudentCourse studentCourse = studentCourseRepository.getStudentCourseByCourseIdAndUserJwtId(stage.getCourse().getId(), AuthUtils.getCurrentJwtId());
        studentCourse.setLastViewedStage(stage);
        studentCourse.setTiming(time);
        studentCourse.setLastActionDate(LocalDateTime.now());
        studentCourseRepository.saveStudentCourse(studentCourse);
    }

    @Transactional
    public String cancelSubscription(String email) {
        User user = userRepository.getUserByEmail(email);
        user.setUnsubscribed(true);
        return "Вы отписались от рассылки"; //TODO
    }

    public StudentStatResponse getStudentStatistics() {
        String jwtId = AuthUtils.getCurrentJwtId();
        User user = userRepository.getUserWithSolutionsByJwtId(jwtId);
        List<Solution> submittedSolutions = user.getSolutions()
                .stream()
                .filter(solution -> !solution.isDraft())
                .filter(solution -> solution.getDeliveryDate() != null)
                .collect(Collectors.toList());
        List<StudentCourse> studentCourseList = studentCourseRepository.getStudentCoursesByJwtId(jwtId);
        return new StudentStatResponse(
                user.getFName(),
                submittedSolutions.size(),
                (int) studentCourseList
                        .stream()
                        .filter(StudentCourse::isCompletenessStatus)
                        .count(),
                (int) studentCourseList
                        .stream()
                        .map(studentCourse -> {
                            Course course = studentCourse.getCourse();
                            Duration courseDuration;
                            if (studentCourse.isCompletenessStatus()) {
                                courseDuration = course.getStages()
                                        .stream()
                                        .map(Stage::getDuration)
                                        .reduce(Duration.ZERO, Duration::plus);
                            } else {
                                Integer lastStageOrderNumber = studentCourse.getLastOpenStage().getOrderNumber();
                                courseDuration = course.getStages()
                                        .stream()
                                        .filter(stage -> stage.getOrderNumber() < lastStageOrderNumber)
                                        .map(Stage::getDuration)
                                        .reduce(Duration.ZERO, Duration::plus);
                            }
                            return courseDuration;
                        })
                        .reduce(Duration.ZERO, Duration::plus).toHours(),
                submittedSolutions
                        .stream()
                        .map(solution -> solution.getTask().getTaskType().getReward())
                        .reduce(Integer::sum).orElse(0)
        );
    }

    public EntrepreneurStatResponse getEntrepreneurStatistics() {
        User user = userRepository.getUserWithTasksByJwtId(AuthUtils.getCurrentJwtId());
        List<Task> tasks = user.getEntrepreneurTasks()
                .stream()
                .filter(task -> !task.getExecutionStatus().equals(TaskExecutionStatus.CANCELED.toString()))
                .collect(Collectors.toList());
        List<Long> taskIdList = tasks
                .stream()
                .map(Task::getId)
                .collect(Collectors.toList());
        List<Solution> solutions = solutionRepository.getSolutionsByTaskIdList(taskIdList);
        List<Solution> receivedSolutions = solutions
                .stream()
                .filter(solution -> !solution.isDraft())
                .filter(solution -> solution.getDeliveryDate() != null)
                .collect(Collectors.toList());
        return new EntrepreneurStatResponse(
                user.getFName(),
                tasks
                        .stream()
                        .map(Task::getDecisionsRequiredNumber)
                        .reduce(Integer::sum).orElse(0),
                receivedSolutions.size(),
                (int) receivedSolutions
                        .stream()
                        .filter(solution -> solution.getGrade() != null)
                        .count()
        );
    }

    public EvaluatedTasksResponse getEvaluatedTasks(Integer amount) {
        User user = userRepository.getUserWithSolutionsWithGradesByJwtId(AuthUtils.getCurrentJwtId());
        Set<Solution> solutions = new HashSet<>(user.getSolutions());
        return new EvaluatedTasksResponse(
                !user.getTaskTypes().isEmpty(),
                solutions
                        .stream()
                        .filter(solution -> solution.getGrade() != null)
                        .sorted((s1, s2) -> s2.getId().compareTo(s1.getId()))
                        .map(solution -> new TaskDto(
                                solution.getTask().getId(),
                                solution.getTask().getTaskType().getTitle(),
                                solution.getGrade().getScore(),
                                solution.getGrade().getTags()
                                        .stream()
                                        .map(tag -> tag.getGradeTag().getValue())
                                        .collect(Collectors.toList()),
                                solution.getGrade().getFeedback() != null,
                                solution.getTask().getBestSolution() != null && solution.getTask().getBestSolution().getSolution().getId().equals(solution.getId())
                        ))
                        .limit(amount)
                        .collect(Collectors.toList())
        );
    }

    public List<CourseWithLastStageResponse> getIncompleteCourses() {
        User user = userRepository.getUserWithStudentCoursesByJwtId(AuthUtils.getCurrentJwtId());
        return user.getStudentCourses()
                .stream()
                .filter(studentCourse -> !studentCourse.isCompletenessStatus())
                .sorted((sc1, sc2) -> sc2.getLastActionDate().compareTo(sc1.getLastActionDate()))
                .map(studentCourse -> {
                    String type = getTypeByStage(studentCourse.getLastOpenStage());
                    return new CourseWithLastStageResponse(
                            studentCourse.getCourse().getId(),
                            studentCourse.getCourse().getTitle(),
                            s3Client.getResourceUrl(studentCourse.getCourse().getImage().getImagePath(), studentCourse.getCourse().getImage().getImageFileName()),
                            studentCourse.getLastOpenStage().getId(),
                            type,
                            studentCourse.getLastOpenStage().getTitle()
                    );
                })
                .collect(Collectors.toList());
    }

    private String getTypeByStage(Stage stage) {
        String type = null;
        if (stage.getLecture() != null) type = "lecture";
        if (stage.getTest() != null) type = "test";
        if (stage.getIntermediatePage() != null) type = "intermediate_page";
        if (stage.getCongratulationPage() != null) type = "congratulation_page";
        return type;
    }

    public List<BestSolutionResponse> getBestSolutions(Integer amount, Integer userId) {
        List<BestSolution> bestSolutions;
        if (userId == null) {
            bestSolutions = bestSolutionRepository.getUserBestSolutions(AuthUtils.getCurrentJwtId());
        } else {
            bestSolutions = bestSolutionRepository.getUserBestSolutions(userId);
        }
        amount = amount == null ? bestSolutions.size() : amount;
        return bestSolutions
                .stream()
                .map(bestSolution -> new BestSolutionResponse(
                        bestSolution.getSolution().getTask().getId(),
                        bestSolution.getSolution().getTask().getTaskType().getTitle(),
                        bestSolution.getSolution().getGrade().getScore(),
                        bestSolution.getSolution().getGrade().getTags()
                                .stream()
                                .map(tag -> tag.getGradeTag().getValue())
                                .collect(Collectors.toList()),
                        bestSolution.getSolution().getGrade().getFeedback() != null))
                .limit(amount)
                .collect(Collectors.toList());
    }

    public UserResponse getAnotherUserInfo(String userId) {
        Integer openId = Integer.parseInt(userId);
        UserResponse user = getUserData(openId);
        return new UserResponse(
                null,
                null,
                null,
                null,
                user.getFName(),
                user.getSName(),
                user.getMName(),
                null,
                null,
                user.getAnalyst(),
                user.getTelepath(),
                user.getEntrepreneur(),
                user.getInsider(),
                user.getRatingPosition(),
                user.getBestSolutionsCount(),
                openId
        );
    }

    private UserResponse getUserData(String jwtId) {
        User user = userRepository.getUserByJwtId(jwtId);
        return getUserData(user);
    }

    private UserResponse getUserData(Integer openId) {
        User user = userRepository.getUserByOpenId(openId);
        return getUserData(user);
    }

    private UserResponse getUserData(User user) {
        List<UserGradeTagEntity> entityList;
        List<LeaderboardEntity> leaderboard = userRepository.getLeaderboard();
        LeaderboardEntity userLeaderboardRow = leaderboard
                .stream()
                .filter(leaderboardRow -> leaderboardRow.getOpenId().equals(user.getOpenId()))
                .findFirst().orElse(null);
        UserGradeTagEntity analyst = null;
        UserGradeTagEntity telepath = null;
        UserGradeTagEntity entrepreneur = null;
        UserGradeTagEntity insider = null;
        try {
            entityList = userRepository.getUserInfoWithGradeTagCount(user.getJwtId());
            analyst = entityList
                    .stream()
                    .filter(entity -> entity.getTag().equals(GradeTag.ANALYST.getValue()))
                    .findFirst().orElse(null);
            telepath = entityList
                    .stream()
                    .filter(entity -> entity.getTag().equals(GradeTag.TELEPATH.getValue()))
                    .findFirst().orElse(null);
            entrepreneur = entityList
                    .stream()
                    .filter(entity -> entity.getTag().equals(GradeTag.ENTREPRENEUR.getValue()))
                    .findFirst().orElse(null);
            insider = entityList
                    .stream()
                    .filter(entity -> entity.getTag().equals(GradeTag.INSIDER.getValue()))
                    .findFirst().orElse(null);
        } catch (EmptyResultDataAccessException ignored) {
        }
        return new UserResponse(
                user.getEmail(),
                user.getEmailVerified(),
                user.getPhone(),
                user.getPhoneVerified(),
                user.getFName(),
                user.getSName(),
                user.getMName(),
                user.getVkId() != null,
                user.isPhoneFirstConfirmation(),
                analyst == null ? 0 : analyst.getCount(),
                telepath == null ? 0 : telepath.getCount(),
                entrepreneur == null ? 0 : entrepreneur.getCount(),
                insider == null ? 0 : insider.getCount(),
                userLeaderboardRow == null ? null : userLeaderboardRow.getPosition(),
                userLeaderboardRow == null ? null : userLeaderboardRow.getBestSolutionsCount(),
                user.getOpenId()
        );
    }

    public LeaderboardResponse getLeaderboard(Integer amount) {
        List<LeaderboardEntity> leaderboard = userRepository.getLeaderboard();
        return getLeaderboardResponse(amount, leaderboard);
    }

    private LeaderboardResponse getLeaderboardResponse(Integer amount, List<LeaderboardEntity> leaderboard) {
        User user = AuthUtils.getCurrentUser();
        LeaderboardEntity currentUser = leaderboard
                .stream()
                .filter(leaderboardRow -> leaderboardRow.getOpenId().equals(Objects.requireNonNull(user).getOpenId()))
                .findFirst().orElse(null);
        amount = amount == null ? leaderboard.size() : amount;
        Double moneyEarnedAmount = 1.0; // деньги, которые еще никто нигде не заработал и заработать не мог, поэтому пока у всех 1.0

        List<LeaderboardRowResponse> users;

        if (FAKE_LEADERBOARD) {
            users = getFakeLeaderboardUsers(amount);
        } else {
            users = leaderboard
                    .stream()
                    .limit(amount)
                    .map(leaderboardRow -> new LeaderboardRowResponse(
                            leaderboardRow.getOpenId(),
                            leaderboardRow.getFName(),
                            leaderboardRow.getSName(),
                            leaderboardRow.getMName(),
                            leaderboardRow.getBestSolutionsCount(),
                            moneyEarnedAmount
                    ))
                    .collect(Collectors.toList());
        }

        if (currentUser == null) {
            return new LeaderboardResponse(
                    Objects.requireNonNull(user).getFName(),
                    user.getSName(),
                    user.getMName(),
                    user.getOpenId(),
                    users
            );
        }

        return new LeaderboardResponse(
                currentUser.getPosition(),
                currentUser.getFName(),
                currentUser.getSName(),
                currentUser.getMName(),
                currentUser.getBestSolutionsCount(),
                moneyEarnedAmount,
                currentUser.getOpenId(),
                users
        );
    }

    private List<LeaderboardRowResponse> getFakeLeaderboardUsers(Integer amount) {
        return Stream.of(new LeaderboardRowResponse(
                        19,
                        "Айгуль",
                        null,
                        null,
                        31,
                        2100*31D),
                new LeaderboardRowResponse(
                        13,
                        "Саня",
                        "Суворов",
                        null,
                        23,
                        2000*23D),
                new LeaderboardRowResponse(
                        9,
                        "Катя",
                        "Прищепа",
                        null,
                        14,
                        2200*14D),
                new LeaderboardRowResponse(
                        2,
                        "Глеб",
                        "Сарапин",
                        null,
                        13,
                        2100*13D),
                new LeaderboardRowResponse(
                        4,
                        "Аня",
                        "Коннова",
                        null,
                        12,
                        2000*12D),
                new LeaderboardRowResponse(
                        28,
                        "Всеволод",
                        "Лысенко",
                        null,
                        7,
                        2000*7D),
                new LeaderboardRowResponse(
                        67,
                        null,
                        null,
                        null,
                        3,
                        1800*3D),
                new LeaderboardRowResponse(
                        70,
                        "Arseniy",
                        "The",
                        "Slayer",
                        3,
                        1900*3D),
                new LeaderboardRowResponse(
                        51,
                        "Вика",
                        null,
                        null,
                        2,
                        2100*2D),
                new LeaderboardRowResponse(
                        11,
                        null,
                        null,
                        null,
                        1,
                        2000*1D),
                new LeaderboardRowResponse(
                        1,
                        "Валерия",
                        null,
                        null,
                        1,
                        1900*1D)
        ).limit(amount == 0 ? 11 : amount).collect(Collectors.toList());
    }

    public LeaderboardResponse getMonthlyLeaderboard(Integer amount) {
        List<LeaderboardEntity> leaderboard = userRepository.getMonthlyLeaderboard();
        return getLeaderboardResponse(amount, leaderboard);
    }
}