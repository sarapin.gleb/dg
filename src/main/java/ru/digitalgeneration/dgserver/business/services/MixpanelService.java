package ru.digitalgeneration.dgserver.business.services;

import com.mixpanel.mixpanelapi.ClientDelivery;
import com.mixpanel.mixpanelapi.MessageBuilder;
import com.mixpanel.mixpanelapi.MixpanelAPI;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;
import ru.digitalgeneration.dgserver.business.entities.User;
import ru.digitalgeneration.dgserver.properties.MixpanelProperties;

import java.io.IOException;

@Service
@EnableAsync
@RequiredArgsConstructor
public class MixpanelService {

    private final MixpanelProperties mixpanelProperties;

    private final ClientDelivery delivery = new ClientDelivery();
    private final MixpanelAPI mixpanel = new MixpanelAPI();
    private static final Logger log = LogManager.getLogger();

    @Async
    public void createUserRegistrationByEmailEvent(User user) throws IOException {
        log.info("Create Mixpanel event: user registration by email " + user.getEmail() + ", method start");
        MessageBuilder messageBuilder = new MessageBuilder(mixpanelProperties.getToken());
        String jwtId = user.getJwtId();
        JSONObject props = new JSONObject();
        putUserInfoProps(user, props);
        JSONObject createUser = messageBuilder.set(jwtId, props);
        mixpanel.sendMessage(createUser);
        JSONObject userRegistrationByEmailEvent = messageBuilder.event(jwtId, "The user registered by email", props);
        delivery.addMessage(userRegistrationByEmailEvent);
        mixpanel.deliver(delivery);
        log.info("Create Mixpanel event: user registration by email " + user.getEmail() + ", method completed");
    }

    @Async
    public void createUserRegistrationByPhoneEvent(User user) throws IOException {
        log.info("Create Mixpanel event: user registration by phone " + user.getPhone() + ", method start");
        MessageBuilder messageBuilder = new MessageBuilder(mixpanelProperties.getToken());
        String jwtId = user.getJwtId();
        JSONObject props = new JSONObject();
        putUserInfoProps(user, props);
        JSONObject createUser = messageBuilder.set(jwtId, props);
        mixpanel.sendMessage(createUser);
        JSONObject userRegistrationByPhoneEvent = messageBuilder.event(jwtId, "The user registered by phone", props);
        delivery.addMessage(userRegistrationByPhoneEvent);
        mixpanel.deliver(delivery);
        log.info("Create Mixpanel event: user registration by phone " + user.getPhone() + ", method completed");
    }

    @Async
    public void createUserRegistrationByVkEvent(User user) throws IOException {
        log.info("Create Mixpanel event: user registration by VK " + user.getVkId() + ", method start");
        MessageBuilder messageBuilder = new MessageBuilder(mixpanelProperties.getToken());
        String jwtId = user.getJwtId();
        JSONObject props = new JSONObject();
        putUserInfoProps(user, props);
        JSONObject createUser = messageBuilder.set(jwtId, props);
        mixpanel.sendMessage(createUser);
        JSONObject userRegistrationByVkEvent = messageBuilder.event(jwtId, "The user registered by VK", props);
        delivery.addMessage(userRegistrationByVkEvent);
        mixpanel.deliver(delivery);
        log.info("Create Mixpanel event: user registration by VK " + user.getVkId() + ", method completed");
    }

    @Async
    public void createFirstCoursePurchaseEvent(User user, Long courseId) throws IOException {
        String jwtId = user.getJwtId();
        log.info("Create Mixpanel event: user, jwtId = " + jwtId + ", purchased the course, id = " + courseId + ", for the first time, method start");
        MessageBuilder messageBuilder = new MessageBuilder(mixpanelProperties.getToken());
        JSONObject userProps = new JSONObject();
        putUserInfoProps(user, userProps);
        JSONObject eventProps = new JSONObject(userProps, JSONObject.getNames(userProps));
        eventProps.put("Course ID", courseId);
        JSONObject updateUser = messageBuilder.set(jwtId, userProps);
        mixpanel.sendMessage(updateUser);
        JSONObject firstCoursePurchaseEvent = messageBuilder.event(jwtId, "The user purchased the course for the first time", eventProps);
        delivery.addMessage(firstCoursePurchaseEvent);
        mixpanel.deliver(delivery);
        log.info("Create Mixpanel event: user, jwtId = " + jwtId + ", purchased the course, id = " + courseId + ", for the first time, method completed");
    }

    @Async
    public void createFirstPointsPurchaseEvent(User user, Long transactionId, Integer pointsNumber) throws IOException {
        String jwtId = user.getJwtId();
        log.info("Create Mixpanel event: user, jwtId = " + jwtId + ", purchased points for the first time, method start");
        MessageBuilder messageBuilder = new MessageBuilder(mixpanelProperties.getToken());
        JSONObject userProps = new JSONObject();
        putUserInfoProps(user, userProps);
        JSONObject eventProps = new JSONObject(userProps, JSONObject.getNames(userProps));
        eventProps.put("Transaction ID", transactionId);
        eventProps.put("Number Of Points", pointsNumber);
        JSONObject updateUser = messageBuilder.set(jwtId, userProps);
        mixpanel.sendMessage(updateUser);
        JSONObject userFirstPointsPurchaseEvent = messageBuilder.event(jwtId, "The user purchased points for the first time", eventProps);
        delivery.addMessage(userFirstPointsPurchaseEvent);
        mixpanel.deliver(delivery);
        log.info("Create Mixpanel event: user, jwtId = " + jwtId + ", purchased points for the first time, method completed");
    }

    public void createPointsPurchaseEvent(User user, Long transactionId, Integer pointsNumber) throws IOException {
        String jwtId = user.getJwtId();
        log.info("Create Mixpanel event: user, jwtId = " + jwtId + ", purchased points, method start");
        MessageBuilder messageBuilder = new MessageBuilder(mixpanelProperties.getToken());
        JSONObject userProps = new JSONObject();
        putUserInfoProps(user, userProps);
        JSONObject eventProps = new JSONObject(userProps, JSONObject.getNames(userProps));
        eventProps.put("Transaction ID", transactionId);
        eventProps.put("Number Of Points", pointsNumber);
        JSONObject updateUser = messageBuilder.set(jwtId, userProps);
        mixpanel.sendMessage(updateUser);
        JSONObject userPointsPurchaseEvent = messageBuilder.event(jwtId, "The user purchased points", eventProps);
        delivery.addMessage(userPointsPurchaseEvent);
        mixpanel.deliver(delivery);
        log.info("Create Mixpanel event: user, jwtId = " + jwtId + ", purchased points, method completed");
    }

    @Async
    public void createPermanentlyBlockingEvent(User user, Integer conclusionWordsNumber, boolean areFilesAttached, int solvedTasksInLastHourNumber, double solutionTimeAsPercentageOfAllotted, boolean isBlockedEarlier, int solvedTasksInLastDayNumber) throws IOException {
        String jwtId = user.getJwtId();
        log.info("Create Mixpanel event: user, jwtId = " + jwtId + ", has been permanently blocked, method start");
        MessageBuilder messageBuilder = new MessageBuilder(mixpanelProperties.getToken());
        JSONObject userProps = new JSONObject();
        putUserInfoProps(user, userProps);
        JSONObject eventProps = new JSONObject(userProps, JSONObject.getNames(userProps));
        eventProps.put("Conclusion Words Number", conclusionWordsNumber);
        eventProps.put("Files Availability", areFilesAttached);
        eventProps.put("Solved Tasks In Last Hour", solvedTasksInLastHourNumber);
        eventProps.put("Solution Time As Percentage Of Allotted", solutionTimeAsPercentageOfAllotted);
        eventProps.put("Previously Blocked", isBlockedEarlier);
        eventProps.put("Solved Tasks In Last Day", solvedTasksInLastDayNumber);
        JSONObject updateUser = messageBuilder.set(jwtId, userProps);
        mixpanel.sendMessage(updateUser);
        JSONObject permanentlyBlockingEvent = messageBuilder.event(jwtId, "The user has been permanently blocked", eventProps);
        delivery.addMessage(permanentlyBlockingEvent);
        mixpanel.deliver(delivery);
        log.info("Create Mixpanel event: user, jwtId = " + jwtId + ", has been permanently blocked, method completed");
    }

    @Async
    public void createTemporaryBlockingEvent(User user, Integer conclusionWordsNumber, boolean areFilesAttached, int solvedTasksInLastDayNumber, double solutionTimeAsPercentageOfAllotted) throws IOException {
        String jwtId = user.getJwtId();
        log.info("Create Mixpanel event: user, jwtId = " + jwtId + ", has been temporary blocked, method start");
        MessageBuilder messageBuilder = new MessageBuilder(mixpanelProperties.getToken());
        JSONObject userProps = new JSONObject();
        putUserInfoProps(user, userProps);
        JSONObject eventProps = new JSONObject(userProps, JSONObject.getNames(userProps));
        eventProps.put("Conclusion Words Number", conclusionWordsNumber);
        eventProps.put("Files Availability", areFilesAttached);
        eventProps.put("Solution Time As Percentage Of Allotted", solutionTimeAsPercentageOfAllotted);
        eventProps.put("Solved Tasks In Last Day", solvedTasksInLastDayNumber);
        JSONObject updateUser = messageBuilder.set(jwtId, userProps);
        mixpanel.sendMessage(updateUser);
        JSONObject temporaryBlockingEvent = messageBuilder.event(jwtId, "The user has been temporary blocked", eventProps);
        delivery.addMessage(temporaryBlockingEvent);
        mixpanel.deliver(delivery);
        log.info("Create Mixpanel event: user, jwtId = " + jwtId + ", has been temporary blocked, method completed");
    }

    @Async
    public void createTaskAssignmentEvent(User user, Long taskId) throws IOException {
        String jwtId = user.getJwtId();
        log.info("Create Mixpanel event: task, id = " + taskId + ", was assigned to the student, jwtId = " + jwtId + ", method start");
        MessageBuilder messageBuilder = new MessageBuilder(mixpanelProperties.getToken());
        JSONObject userProps = new JSONObject();
        putUserInfoProps(user, userProps);
        JSONObject eventProps = new JSONObject(userProps, JSONObject.getNames(userProps));
        eventProps.put("Task ID", taskId);
        JSONObject updateUser = messageBuilder.set(jwtId, userProps);
        mixpanel.sendMessage(updateUser);
        JSONObject taskAssignmentEvent = messageBuilder.event(jwtId, "The task was assigned to the student", eventProps);
        delivery.addMessage(taskAssignmentEvent);
        mixpanel.deliver(delivery);
        log.info("Create Mixpanel event: task, id = " + taskId + ", was assigned to the student, jwtId = " + jwtId + ", method completed");
    }

    @Async
    public void createSendSolutionEvent(User user, Long solutionId) throws IOException {
        log.info("Create Mixpanel event: solution, id = " + solutionId + ", has been sent, method start");
        MessageBuilder messageBuilder = new MessageBuilder(mixpanelProperties.getToken());
        String jwtId = user.getJwtId();
        JSONObject userProps = new JSONObject();
        putUserInfoProps(user, userProps);
        JSONObject eventProps = new JSONObject(userProps, JSONObject.getNames(userProps));
        eventProps.put("Solution ID", solutionId);
        JSONObject updateUser = messageBuilder.set(jwtId, userProps);
        mixpanel.sendMessage(updateUser);
        JSONObject sendSolutionEvent = messageBuilder.event(jwtId, "The solution has been sent", eventProps);
        delivery.addMessage(sendSolutionEvent);
        mixpanel.deliver(delivery);
        log.info("Create Mixpanel event: solution, id = " + solutionId + ", has been sent, method completed");
    }

    @Async
    public void createUserVKConfirmationEvent(User user) throws IOException {
        String jwtId = user.getJwtId();
        log.info("Create Mixpanel event: user, jwtId = " + jwtId + ", confirmed VK, method start");
        MessageBuilder messageBuilder = new MessageBuilder(mixpanelProperties.getToken());
        JSONObject props = new JSONObject();
        putUserInfoProps(user, props);
        JSONObject updateUser = messageBuilder.set(jwtId, props);
        mixpanel.sendMessage(updateUser);
        JSONObject userVKConfirmationEvent = messageBuilder.event(jwtId, "The user confirmed VK", props);
        delivery.addMessage(userVKConfirmationEvent);
        mixpanel.deliver(delivery);
        log.info("Create Mixpanel event: user, jwtId = " + jwtId + ", confirmed VK, method completed");
    }

    @Async
    public void createUserPhoneConfirmationEvent(User user) throws IOException {
        String jwtId = user.getJwtId();
        log.info("Create Mixpanel event: user, jwtId = " + jwtId + ", verified phone, method start");
        MessageBuilder messageBuilder = new MessageBuilder(mixpanelProperties.getToken());
        JSONObject props = new JSONObject();
        putUserInfoProps(user, props);
        JSONObject updateUser = messageBuilder.set(jwtId, props);
        mixpanel.sendMessage(updateUser);
        JSONObject userPhoneConfirmationEvent = messageBuilder.event(jwtId, "The user verified phone", props);
        delivery.addMessage(userPhoneConfirmationEvent);
        mixpanel.deliver(delivery);
        log.info("Create Mixpanel event: user, jwtId = " + jwtId + ", verified phone, method completed");
    }

    @Async
    public void createUserEmailConfirmationEvent(User user) throws IOException {
        String jwtId = user.getJwtId();
        log.info("Create Mixpanel event: user, jwtId = " + jwtId + ", verified email, method start");
        MessageBuilder messageBuilder = new MessageBuilder(mixpanelProperties.getToken());
        JSONObject props = new JSONObject();
        putUserInfoProps(user, props);
        JSONObject updateUser = messageBuilder.set(jwtId, props);
        mixpanel.sendMessage(updateUser);
        JSONObject userEmailConfirmationEvent = messageBuilder.event(jwtId, "The user verified email", props);
        delivery.addMessage(userEmailConfirmationEvent);
        mixpanel.deliver(delivery);
        log.info("Create Mixpanel event: user, jwtId = " + jwtId + ", verified email, method completed");
    }

    @Async
    public void createBonusAccrualEvent(User user) throws IOException {
        String jwtId = user.getJwtId();
        log.info("Create Mixpanel event: bonus was accrued to the user, jwtId = " + jwtId + ", method start");
        MessageBuilder messageBuilder = new MessageBuilder(mixpanelProperties.getToken());
        JSONObject props = new JSONObject();
        putUserInfoProps(user, props);
        JSONObject updateUser = messageBuilder.set(jwtId, props);
        mixpanel.sendMessage(updateUser);
        JSONObject bonusAccrualEvent = messageBuilder.event(jwtId, "The bonus was accrued to the user", props);
        delivery.addMessage(bonusAccrualEvent);
        mixpanel.deliver(delivery);
        log.info("Create Mixpanel event: bonus was accrued to the user, jwtId = " + jwtId + ", method completed");
    }

    private void putUserInfoProps(User user, JSONObject props) {
        props.put("Name", user.getFName());
        props.put("Surname", user.getSName());
        props.put("Patronymic", user.getMName());
        props.put("Registration Date", user.getRegistrationDate());
        props.put("Email", user.getEmail());
        props.put("Email Verification Status", user.getEmailVerified());
        props.put("Email Unsubscribe Status", user.isUnsubscribed());
        props.put("Phone", user.getPhone());
        props.put("Phone Verification Status", user.getPhoneVerified());
        props.put("Phone First Confirmation Status", user.isPhoneFirstConfirmation());
        props.put("VK ID", user.getVkId());
        props.put("Last Changing Profile Date", user.getLastChangingProfileDate());
        props.put("Roles", user.getRoles());
        props.put("Last Points Purchase Date", user.getLastPointsPurchaseDate());
        props.put("Point Balance", user.getPointBalance());
        props.put("Black List Status", user.isPermanentlyBlocked());
        props.put("Gray List Unlocking Date", user.getUnlockDate());
        props.put("Last Login Date", user.getLastLoginDate());
        props.put("Last Action Date", user.getLastActionDate());
        props.put("UTM Source", user.getUtmParameters() == null ? null : user.getUtmParameters().getUtmSource());
        props.put("UTM Medium", user.getUtmParameters() == null ? null : user.getUtmParameters().getUtmMedium());
        props.put("UTM Campaign", user.getUtmParameters() == null ? null : user.getUtmParameters().getUtmCampaign());
        props.put("UTM Term", user.getUtmParameters() == null ? null : user.getUtmParameters().getUtmTerm());
        props.put("UTM Content", user.getUtmParameters() == null ? null : user.getUtmParameters().getUtmContent());
    }
}