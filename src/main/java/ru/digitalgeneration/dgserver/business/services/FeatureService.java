package ru.digitalgeneration.dgserver.business.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;
import ru.digitalgeneration.dgserver.business.dto.responses.FeatureResponse;
import ru.digitalgeneration.dgserver.business.repositories.repositories.FeatureRepository;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Validated
@RequiredArgsConstructor
public class FeatureService {

    private final FeatureRepository featureRepository;

    public List<FeatureResponse> getFeatureList() {
        return featureRepository.getFeatureListOrderedByReleaseDate()
                .stream()
                .map(feature -> new FeatureResponse(
                        feature.getReleaseDate(),
                        feature.getTitle(),
                        feature.getDescription()))
                .collect(Collectors.toList());
    }
}