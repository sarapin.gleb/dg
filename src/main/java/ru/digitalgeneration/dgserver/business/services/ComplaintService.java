package ru.digitalgeneration.dgserver.business.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.digitalgeneration.dgserver.business.connections.MailServiceConnection;
import ru.digitalgeneration.dgserver.business.dto.requests.SolutionComplaintRequest;
import ru.digitalgeneration.dgserver.business.dto.requests.TaskComplaintRequest;
import ru.digitalgeneration.dgserver.business.entities.*;
import ru.digitalgeneration.dgserver.business.entities.enums.TaskExecutionStatus;
import ru.digitalgeneration.dgserver.business.exceptions.ComplaintAlreadyExistException;
import ru.digitalgeneration.dgserver.business.exceptions.WrongEntrepreneurException;
import ru.digitalgeneration.dgserver.business.repositories.repositories.*;
import ru.digitalgeneration.dgserver.business.utils.AuthUtils;

import javax.mail.MessagingException;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ComplaintService {

    private final TaskRepository taskRepository;
    private final TaskComplaintTagRepository taskComplaintTagRepository;
    private final TaskComplaintRepository taskComplaintRepository;
    private final SolutionRepository solutionRepository;
    private final SolutionComplaintTagRepository solutionComplaintTagRepository;
    private final SolutionComplaintRepository solutionComplaintRepository;
    private final TaskService taskService;
    private final MailServiceConnection mailServiceConnection;

    @Transactional
    public void createTaskComplaint(TaskComplaintRequest complaintRequest) {
        if (taskComplaintRepository.isComplaintExist(AuthUtils.getCurrentJwtId(), complaintRequest.getTaskId())) {
            throw new ComplaintAlreadyExistException("The student has already complained about this task before");
        }
        User student = Objects.requireNonNull(AuthUtils.getCurrentUser());
        Task task = taskRepository.getTaskById(complaintRequest.getTaskId());
        Set<TaskComplaintTag> tags = new HashSet<>(taskComplaintTagRepository.getTaskComplaintTagsByIdList(complaintRequest.getTagIdList()));
        TaskComplaint complaint = new TaskComplaint(student, task, complaintRequest.getValue(), tags);
        taskComplaintRepository.saveComplaint(complaint);
        sendTaskComplaintMailToAdmin(
                student.getJwtId(),
                complaintRequest.getTaskId(),
                tags
                        .stream()
                        .map(TaskComplaintTag::getValue)
                        .collect(Collectors.toList()),
                complaintRequest.getValue());
    }

    private void sendTaskComplaintMailToAdmin(String studentJwtId, Long taskId, List<String> complaintTags, String value) {
        try {
            mailServiceConnection.sendTaskComplaintMailToAdmin(studentJwtId, taskId, complaintTags, value);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    @Transactional
    public void createSolutionComplaint(SolutionComplaintRequest complaintRequest) {
        Long solutionId = complaintRequest.getSolutionId();
        if (!solutionRepository.isTaskCreatedByEntrepreneur(AuthUtils.getCurrentJwtId(), solutionId)) {
            throw new WrongEntrepreneurException("The task was created by another entrepreneur");
        }
        Solution solution = solutionRepository.getSolutionById(solutionId);
        if (solution.getSolutionComplaint() != null) {
            throw new ComplaintAlreadyExistException("The entrepreneur has already complained about this solution before");
        }
        Set<SolutionComplaintTag> tags = new HashSet<>(solutionComplaintTagRepository.getSolutionComplaintTagsByIdList(complaintRequest.getTagIdList()));
        SolutionComplaint complaint = new SolutionComplaint(complaintRequest.getValue(), tags);
        solutionComplaintRepository.saveComplaint(complaint);
        solution.setSolutionComplaint(complaint);
        sendSolutionComplaintMailToAdmin(
                solution.getTask().getEntrepreneur().getJwtId(),
                solutionId,
                tags
                        .stream()
                        .map(SolutionComplaintTag::getValue)
                        .collect(Collectors.toList()),
                complaintRequest.getValue());
        if (taskService.isThisCheckLast(solution.getTask())) {
            solution.getTask().setExecutionStatus(TaskExecutionStatus.GRADED.toString());
        }
    }

    private void sendSolutionComplaintMailToAdmin(String entrepreneurJwtId, Long solutionId, List<String> complaintTags, String value) {
        try {
            mailServiceConnection.sendSolutionComplaintMailToAdmin(entrepreneurJwtId, solutionId, complaintTags, value);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }
}