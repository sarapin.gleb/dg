package ru.digitalgeneration.dgserver.business.services;

import com.amazonaws.services.s3.AmazonS3Client;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import ru.digitalgeneration.dgserver.business.entities.enums.BucketName;
import ru.digitalgeneration.dgserver.business.dto.requests.RemoveFilesRequest;
import ru.digitalgeneration.dgserver.business.dto.responses.FileResponse;
import ru.digitalgeneration.dgserver.business.entities.File;
import ru.digitalgeneration.dgserver.business.entities.Solution;
import ru.digitalgeneration.dgserver.business.entities.Task;
import ru.digitalgeneration.dgserver.business.exceptions.FileActionException;
import ru.digitalgeneration.dgserver.business.exceptions.SolutionSentAfterDeadlineException;
import ru.digitalgeneration.dgserver.business.exceptions.WrongStudentException;
import ru.digitalgeneration.dgserver.business.repositories.repositories.FileRepository;
import ru.digitalgeneration.dgserver.business.repositories.repositories.SolutionRepository;
import ru.digitalgeneration.dgserver.business.utils.AuthUtils;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class FileService {

    private final FileStore fileStore;
    private final FileRepository fileRepository;
    private final SolutionRepository solutionRepository;
    private final AmazonS3Client s3Client;

    @Transactional
    public List<FileResponse> addFilesToSolution(Long taskId, MultipartFile[] files) {
        Solution solution = solutionRepository.getSolutionByStudentJwtIdAndTaskIdAndDraftIsTrue(AuthUtils.getCurrentJwtId(), taskId);
        if (solution.getReceiptDate()
                .plusSeconds(solution.getTask().getTaskType().getSolutionTime().getSeconds())
                .plusMinutes(2)
                .isBefore(LocalDateTime.now())) {
            throw new SolutionSentAfterDeadlineException("Time to complete the task is over");
        }
        int maxOrderNumber = solution.getFiles().stream().mapToInt(File::getOrderNumber).max().orElse(0);
        List<File> fileEntityList = saveFiles(files, maxOrderNumber, null, solution);
        List<File> savedFiles = fileRepository.saveFiles(fileEntityList);
        return savedFiles
                .stream()
                .map(file -> new FileResponse(
                        file.getId(),
                        s3Client.getResourceUrl(file.getFilePath(), file.getFileName()),
                        file.getFileName())
                )
                .collect(Collectors.toList());
    }

    protected List<File> saveFiles(MultipartFile[] files, int maxOrderNumber, Task task, Solution solution) {
        AtomicInteger displayingOrder = new AtomicInteger(maxOrderNumber);
        return Arrays.stream(files)
                .map(file -> {
                    String path = String.format("%s/%s", BucketName.DG_BUCKET.getBucketName(), UUID.randomUUID());
                    String fileName = String.format("%s", file.getOriginalFilename());
                    saveFile(file, path, fileName);
                    if (solution == null && task != null) {
                        return new File(path, fileName, displayingOrder.incrementAndGet(), task);
                    } else if (task == null && solution != null) {
                        return new File(path, fileName, displayingOrder.incrementAndGet(), solution);
                    } else {
                        throw new FileActionException("The file must belong to either a task or a solution");
                    }
                })
                .collect(Collectors.toList());
    }

    @Transactional
    public void saveFile(MultipartFile file, String path, String fileName) {
        if (file.isEmpty()) {
            throw new IllegalStateException("Cannot upload empty file");
        }
        Map<String, String> metadata = new HashMap<>();
        metadata.put(HttpHeaders.CONTENT_TYPE, file.getContentType());
        metadata.put(HttpHeaders.CONTENT_LENGTH, String.valueOf(file.getSize()));
        try {
            fileStore.upload(path, fileName, Optional.of(metadata), file.getInputStream());
        } catch (IOException e) {
            throw new IllegalStateException("Failed to upload file", e);
        }
    }

    @Transactional
    public void removeFiles(RemoveFilesRequest removeFilesRequest) {
        List<File> files = fileRepository.getFilesByIdList(removeFilesRequest.getFiles());
        if (files.stream().anyMatch(file -> !file.getSolution().getTask().getId().equals(removeFilesRequest.getTaskId()))) {
            throw new FileActionException("At least one of the files to be deleted does not belong to the specified task");
        }
        String jwtId = AuthUtils.getCurrentJwtId();
        if (files.stream().anyMatch(file -> !file.getSolution().getStudent().getJwtId().equals(jwtId))) {
            throw new WrongStudentException("At least one of the files to be deleted belongs to another student's solution");
        }
        files.forEach(file -> deleteFile(file.getFilePath(), file.getFileName()));
        fileRepository.removeFiles(files);
    }

    private void deleteFile(String filePath, String fileName) {
        fileStore.deleteFile(filePath, fileName);
    }
}