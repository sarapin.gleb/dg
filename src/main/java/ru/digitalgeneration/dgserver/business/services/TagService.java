package ru.digitalgeneration.dgserver.business.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;
import ru.digitalgeneration.dgserver.business.dto.responses.GradeTagResponse;
import ru.digitalgeneration.dgserver.business.dto.responses.TagComplaintResponse;
import ru.digitalgeneration.dgserver.business.entities.Tag;
import ru.digitalgeneration.dgserver.business.repositories.repositories.SolutionRepository;
import ru.digitalgeneration.dgserver.business.repositories.repositories.TagRepository;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Validated
@RequiredArgsConstructor
public class TagService {

    private final TagRepository tagRepository;
    private final SolutionRepository solutionRepository;

    public List<String> getTagList() {
        return tagRepository.getTagList()
                .stream()
                .map(Tag::getValue)
                .collect(Collectors.toList());
    }

    public List<TagComplaintResponse> getTaskComplaintTags() {
        return tagRepository.getTaskComplaintTags()
                .stream()
                .map(tag -> new TagComplaintResponse(
                        tag.getId(),
                        tag.getValue()
                ))
                .collect(Collectors.toList());
    }

    public List<TagComplaintResponse> getSolutionComplaintTags() {
        return tagRepository.getSolutionComplaintTags()
                .stream()
                .map(tag -> new TagComplaintResponse(
                        tag.getId(),
                        tag.getValue()
                ))
                .collect(Collectors.toList());
    }

    public List<GradeTagResponse> getGradeTags(Long solutionId) {
        return solutionRepository.getSolutionById(solutionId).getTask().getTaskType().getTaskTypeGradeTagList()
                .stream()
                .map(e -> new GradeTagResponse(
                        e.getId(),
                        e.getGradeTag().getValue(),
                        e.getComment()))
                .sorted(Comparator.comparing(GradeTagResponse::getValue))
                .collect(Collectors.toList());
    }
}