package ru.digitalgeneration.dgserver.business.services;

import com.amazonaws.services.s3.AmazonS3Client;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.digitalgeneration.dgserver.business.connections.MailServiceConnection;
import ru.digitalgeneration.dgserver.business.dto.GradeDto;
import ru.digitalgeneration.dgserver.business.dto.requests.SolutionRequest;
import ru.digitalgeneration.dgserver.business.dto.responses.FileResponse;
import ru.digitalgeneration.dgserver.business.dto.responses.SolutionResponseForEntrepreneur;
import ru.digitalgeneration.dgserver.business.dto.responses.SolutionResponseForStudent;
import ru.digitalgeneration.dgserver.business.dto.responses.TagResponse;
import ru.digitalgeneration.dgserver.business.entities.*;
import ru.digitalgeneration.dgserver.business.entities.enums.TaskExecutionStatus;
import ru.digitalgeneration.dgserver.business.exceptions.SolutionAlreadySentException;
import ru.digitalgeneration.dgserver.business.exceptions.SolutionSentAfterDeadlineException;
import ru.digitalgeneration.dgserver.business.exceptions.WrongEntrepreneurException;
import ru.digitalgeneration.dgserver.business.repositories.repositories.SolutionRepository;
import ru.digitalgeneration.dgserver.business.utils.AuthUtils;

import javax.mail.MessagingException;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class SolutionService {

    private final SolutionRepository solutionRepository;
    private final AmazonS3Client s3Client;
    private final MailServiceConnection mailServiceConnection;
    private final MixpanelService mixpanelService;

    @Value("${blocking_triggers.permanent.max_solutions_in_hour}")
    private int maxSolutionsInHour;
    @Value("${blocking_triggers.permanent.min_solution_time_percentage}")
    private int minSolutionTimePercentageForPermanentBlocking;
    @Value("${blocking_triggers.permanent.max_solutions_in_day}")
    private int maxSolutionsInDayForPermanentBlocking;

    @Value("${blocking_triggers.temporary.max_solutions_in_day}")
    private int maxSolutionsInDayForTemporaryBlocking;
    @Value("${blocking_triggers.temporary.min_solution_time_percentage}")
    private int minSolutionTimePercentageForTemporaryBlocking;
    @Value("${blocking_triggers.temporary.min_conclusion_words}")
    private int minConclusionWords;

    public SolutionResponseForStudent getSolutionByTaskId(Long taskId, String userId) {
        String jwtId = userId == null ? AuthUtils.getCurrentJwtId() : userId;
        Solution solution = solutionRepository.getSolutionByStudentJwtIdAndTaskId(jwtId, taskId);
        Grade grade = solution.getGrade();
        List<TaskTypeGradeTag> tagList = grade == null ? new ArrayList<>() : new ArrayList<>(grade.getTags());
        return new SolutionResponseForStudent(
                solution.getTask().getTaskType().getTitle(),
                solution.getDeliveryDate(),
                grade == null ? null : grade.getScore(),
                tagList
                        .stream()
                        .map(tag -> new TagResponse(
                                tag.getGradeTag().getValue(),
                                tag.getComment()))
                        .collect(Collectors.toList()),
                grade == null ? null : grade.getFeedback(),
                solution.getTask().getFormulation(),
                solution.getTask().getFiles()
                        .stream()
                        .map(file -> new FileResponse(
                                file.getId(),
                                s3Client.getResourceUrl(file.getFilePath(), file.getFileName()),
                                file.getFileName()))
                        .collect(Collectors.toList()),
                solution.getTask().getTaskType().getSolutionTime(),
                solution.getTask().getTaskType().getReward(),
                solution.getValue(),
                solution.getFiles()
                        .stream()
                        .map(file -> new FileResponse(
                                file.getId(),
                                s3Client.getResourceUrl(file.getFilePath(), file.getFileName()),
                                file.getFileName()))
                        .collect(Collectors.toList())
        );
    }

    @Transactional
    public void sendSolution(SolutionRequest solutionRequest) throws IOException {
        String jwtId = AuthUtils.getCurrentJwtId();
        Solution solution = solutionRepository.getSolutionByStudentJwtIdAndTaskId(jwtId, solutionRequest.getTaskId());
        if ((solution.getReceiptDate()
                .plusSeconds(solution.getTask().getTaskType().getSolutionTime().getSeconds())
                .plusMinutes(2)
                .isBefore(LocalDateTime.now()))) {
            throw new SolutionSentAfterDeadlineException("Time to complete the task is over");
        }
        if (!solution.isDraft()) {
            throw new SolutionAlreadySentException("The solution has already been sent");
        }
        User student = solution.getStudent();
        solution.setValue(solutionRequest.getSolution());
        if (!solutionRequest.isDraft()) {
            checkBlockingTriggers(student, solution, solutionRequest.getConclusionWordsNumber());
            solution.setDeliveryDate(LocalDateTime.now());
            solution.setDraft(solutionRequest.isDraft());
            Task task = solution.getTask();
            student.setPointBalance(student.getPointBalance() + task.getTaskType().getReward());
            mixpanelService.createSendSolutionEvent(student, solution.getId());
            if (!task.getExecutionStatus().equals(TaskExecutionStatus.COMPLETED.toString()) && isThisSolutionLast(task)) {
                task.setExecutionStatus(TaskExecutionStatus.COMPLETED.toString());
                sendTaskCompletedMail(task.getId(), task.getTaskType().getTitle(), task.getEntrepreneur().getEmail());
            }
        }
    }

    private void checkBlockingTriggers(User student, Solution currentSolution, Integer conclusionWordsNumber) throws IOException {
        LocalDateTime now = LocalDateTime.now();
        int solvedTasksInLastDayNumber = (int) student.getSolutions()
                .stream()
                .filter(solution -> !solution.isDraft())
                .filter(solution -> solution.getDeliveryDate().isAfter(now.minusDays(1)))
                .count();
        int solvedTasksInLastHourNumber = (int) student.getSolutions()
                .stream()
                .filter(solution -> !solution.isDraft())
                .filter(solution -> solution.getDeliveryDate().isAfter(now.minusHours(1)))
                .count();
        double solutionTimeAsPercentageOfAllotted = ChronoUnit.MILLIS.between(currentSolution.getReceiptDate(), now) * 100.0 / currentSolution.getTask().getTaskType().getSolutionTime().toMillis();
        boolean areFilesAttached = !currentSolution.getFiles().isEmpty();
        boolean isBlockedEarlier = student.getUnlockDate() != null;
        if ((conclusionWordsNumber == 0 && !areFilesAttached)
                || solvedTasksInLastHourNumber >= maxSolutionsInHour
                || solutionTimeAsPercentageOfAllotted < minSolutionTimePercentageForPermanentBlocking
                || (isBlockedEarlier && solvedTasksInLastDayNumber >= maxSolutionsInDayForPermanentBlocking)
        ) {
            student.setPermanentlyBlocked(true);
            mixpanelService.createPermanentlyBlockingEvent(
                    student,
                    conclusionWordsNumber,
                    areFilesAttached,
                    solvedTasksInLastHourNumber,
                    solutionTimeAsPercentageOfAllotted,
                    isBlockedEarlier,
                    solvedTasksInLastDayNumber
            );
        } else if (solvedTasksInLastDayNumber >= maxSolutionsInDayForTemporaryBlocking
                || solutionTimeAsPercentageOfAllotted < minSolutionTimePercentageForTemporaryBlocking
                || (conclusionWordsNumber < minConclusionWords && !areFilesAttached)
        ) {
            student.setUnlockDate(now.plusHours(1));
            mixpanelService.createTemporaryBlockingEvent(
                    student,
                    conclusionWordsNumber,
                    areFilesAttached,
                    solvedTasksInLastDayNumber,
                    solutionTimeAsPercentageOfAllotted
            );
        }
    }

    private void sendTaskCompletedMail(Long taskId, String taskTitle, String email) {
        try {
            mailServiceConnection.sendTaskCompletedMail(taskId, taskTitle, email);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    private boolean isThisSolutionLast(Task task) {
        Integer orderedSolutionsNumber = task.getDecisionsRequiredNumber();
        Integer completedSolutionsNumber = (int) task.getSolutions()
                .stream()
                .filter(solution -> !solution.isDraft())
                .count();
        return orderedSolutionsNumber <= completedSolutionsNumber;
    }

    public SolutionResponseForEntrepreneur getSolutionById(Long solutionId) {
        Solution solution = solutionRepository.getSolutionById(solutionId);
        if (!solution.getTask().getEntrepreneur().getJwtId().equals(AuthUtils.getCurrentJwtId())) {
            throw new WrongEntrepreneurException("The task was created by another entrepreneur");
        }
        return new SolutionResponseForEntrepreneur(
                solution.getValue(),
                solution.getFiles()
                        .stream()
                        .sorted(Comparator.comparing(File::getOrderNumber))
                        .map(file -> new FileResponse(
                                file.getId(),
                                s3Client.getResourceUrl(file.getFilePath(), file.getFileName()),
                                file.getFileName()))
                        .collect(Collectors.toList()),
                solution.getGrade() != null ? new GradeDto(
                        solution.getGrade().getScore(),
                        solution.getGrade().getTags()
                                .stream()
                                .map(tag -> new TagResponse(
                                        tag.getGradeTag().getValue(),
                                        tag.getComment()
                                )).collect(Collectors.toList()),
                        solution.getGrade().getFeedback()) : null,
                solution.getSolutionComplaint() != null ? solution.getSolutionComplaint().getValue() : null
        );
    }
}