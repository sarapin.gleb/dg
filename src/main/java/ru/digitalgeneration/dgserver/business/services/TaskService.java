package ru.digitalgeneration.dgserver.business.services;

import com.amazonaws.services.s3.AmazonS3Client;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import ru.digitalgeneration.dgserver.business.connections.MailServiceConnection;
import ru.digitalgeneration.dgserver.business.dto.SolutionDto;
import ru.digitalgeneration.dgserver.business.dto.TaskDtoForSlider;
import ru.digitalgeneration.dgserver.business.dto.requests.GradeRequest;
import ru.digitalgeneration.dgserver.business.dto.requests.TaskRequest;
import ru.digitalgeneration.dgserver.business.dto.responses.*;
import ru.digitalgeneration.dgserver.business.entities.*;
import ru.digitalgeneration.dgserver.business.entities.enums.TaskExecutionStatus;
import ru.digitalgeneration.dgserver.business.entities.sql_mapping_entities.CurrentTaskEntity;
import ru.digitalgeneration.dgserver.business.exceptions.*;
import ru.digitalgeneration.dgserver.business.repositories.repositories.*;
import ru.digitalgeneration.dgserver.business.utils.AuthUtils;

import javax.mail.MessagingException;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TaskService {

    private final TaskRepository taskRepository;
    private final TaskTypeRepository taskTypeRepository;
    private final TaskTypeGradeTagRepository taskTypeGradeTagRepository;
    private final GradeRepository gradeRepository;
    private final FileService fileService;
    private final FileRepository fileRepository;
    private final UserRepository userRepository;
    private final SolutionRepository solutionRepository;
    private final BestSolutionRepository bestSolutionRepository;
    private final AmazonS3Client s3Client;
    private final MixpanelService mixpanelService;
    private final MailServiceConnection mailServiceConnection;

    @Value("${bonuses.profile_verification_bonus}")
    private Integer profileVerificationBonus;

    @Transactional
    public BalanceResponse createTask(TaskRequest taskRequest, MultipartFile[] files) {
        TaskType taskType = taskTypeRepository.getTaskTypeById(taskRequest.getTaskTypeId());
        User entrepreneur = userRepository.getUserByJwtId(AuthUtils.getCurrentJwtId());
        Integer finalPrice = taskType.getPrice() * taskRequest.getDecisionsRequiredNumber();
        int newBalance = entrepreneur.getPointBalance() - finalPrice;
        if (newBalance < 0) {
            throw new NotEnoughPointsException("There are not enough points on the balance", taskType.getTitle(), -newBalance);
        }
        Task task = new Task(
                entrepreneur,
                taskType,
                taskRequest.getFormulation(),
                taskRequest.getDecisionsRequiredNumber(),
                isTaskSpecial(taskRequest.getDecisionsRequiredNumber(), entrepreneur)
        );
        if (!files[0].isEmpty()) {
            addFiles(files, task);
        }
        entrepreneur.setPointBalance(newBalance);
        taskRepository.saveTask(task);
        userRepository.saveUserChanges(entrepreneur);
        return new BalanceResponse(newBalance);
    }

    private boolean isTaskSpecial(Integer decisionsRequiredNumber, User entrepreneur) {
        if (decisionsRequiredNumber < 2) {
            return false;
        }
        if (entrepreneur.isAdmin()) {
            return false;
        }
        boolean isBonusAccrued = entrepreneur.isPhoneFirstConfirmation() || entrepreneur.getVkId() != null;
        if (!isBonusAccrued) {
            return true;
        } else {
            Integer spending = entrepreneur.getEntrepreneurTasks()
                    .stream()
                    .map(task -> task.getTaskType().getPrice() * task.getDecisionsRequiredNumber())
                    .reduce(Integer::sum).orElse(0);
            return spending >= profileVerificationBonus;
        }
    }

    private void addFiles(MultipartFile[] files, Task task) {
        List<File> fileEntityList = fileService.saveFiles(files, 0, task, null);
        fileRepository.saveFiles(fileEntityList);
    }

    public List<TaskResponseForEntrepreneur> getTasksSetByEntrepreneur(Integer amount) {
        List<TaskResponseForEntrepreneur> response = taskRepository.getTasksByEntrepreneurJwtId(AuthUtils.getCurrentJwtId())
                .stream()
                .map(task -> new TaskResponseForEntrepreneur(
                        task.getId(),
                        task.getTaskType().getTitle(),
                        TaskExecutionStatus.valueOf(task.getExecutionStatus()).getValue(),
                        task.getDeadline(),
                        task.getDecisionsRequiredNumber(),
                        (int) task.getSolutions()
                                .stream()
                                .filter(solution -> !solution.isDraft())
                                .filter(solution -> solution.getDeliveryDate() != null)
                                .count(),
                        getCountGradeSolutions(task),
                        task.getBestSolution() == null &&
                                task.getExecutionStatus().equals(TaskExecutionStatus.GRADED.toString()) &&
                                task.isSpecialTask() &&
                                task.getSolutions()
                                        .stream()
                                        .anyMatch(sol -> sol.getGrade() != null),
                        task.getExecutionStatus().equals(TaskExecutionStatus.EVALUATED.toString())
                ))
                .sorted(byStatus(TaskExecutionStatus.COMPLETED)
                        .thenComparing(byStatus(TaskExecutionStatus.GRADED))
                        .thenComparing(byStatus(TaskExecutionStatus.IN_PROGRESS))
                        .thenComparing(byStatus(TaskExecutionStatus.EVALUATED))
                        .thenComparing(byStatus(TaskExecutionStatus.NOT_DONE))
                        .thenComparing(byStatus(TaskExecutionStatus.CANCELED))
                )
                .collect(Collectors.toList());
        if (amount == null) {
            return response;
        } else {
            return response
                    .stream()
                    .limit(amount)
                    .collect(Collectors.toList());
        }
    }

    private Comparator<TaskResponseForEntrepreneur> byStatus(TaskExecutionStatus status) {
        return (t1, t2) -> {
            if (status.equals(TaskExecutionStatus.COMPLETED) || status.equals(TaskExecutionStatus.IN_PROGRESS)) {
                if (t1.getExecutionStatus().equals(status.getValue())) {
                    return t2.getExecutionStatus().equals(status.getValue()) ? byNotEvaluatedTasksNumber(t1, t2) : -1;
                } else {
                    return t2.getExecutionStatus().equals(status.getValue()) ? 1 : 0;
                }
            } else {
                if (t1.getExecutionStatus().equals(status.getValue())) {
                    return t2.getExecutionStatus().equals(status.getValue()) ? 0 : -1;
                } else {
                    return t2.getExecutionStatus().equals(status.getValue()) ? 1 : 0;
                }
            }
        };
    }

    private int byNotEvaluatedTasksNumber(TaskResponseForEntrepreneur t1, TaskResponseForEntrepreneur t2) {
        Integer notEvaluatedTasksNumber1 = t1.getOrderedSolutionsNumber() - t1.getEvaluatedSolutionsNumber();
        Integer notEvaluatedTasksNumber2 = t2.getOrderedSolutionsNumber() - t2.getEvaluatedSolutionsNumber();
        return notEvaluatedTasksNumber2.compareTo(notEvaluatedTasksNumber1);
    }

    private int getCountGradeSolutions(Task task) {
        return (int) task.getSolutions()
                .stream()
                .filter(solution -> solution.getGrade() != null || solution.getSolutionComplaint() != null)
                .count();
    }

    public List<TaskResponseForStudent> getSolvedTasks() {
        return solutionRepository.getSolutionsByStudentJwtIdAndDraftIsFalse(AuthUtils.getCurrentJwtId())
                .stream()
                .sorted(Comparator
                        .comparing(Solution::getDeliveryDate, (s1, s2) -> {
                            if (s1.plusHours(24).isAfter(LocalDateTime.now())) {
                                return s2.plusHours(24).isAfter(LocalDateTime.now()) ? s1.compareTo(s2) : 1;
                            } else {
                                return s2.plusHours(24).isAfter(LocalDateTime.now()) ? -1 : 0;
                            }
                        })
                        .thenComparing((s1, s2) -> {
                            if (s1.getGrade() != null) {
                                return s2.getGrade() != null ? s1.getDeliveryDate().compareTo(s2.getDeliveryDate()) : 1;
                            } else {
                                return s2.getGrade() != null ? -1 : 0;
                            }
                        })
                        .thenComparing(Solution::getDeliveryDate).reversed())
                .map(task -> new TaskResponseForStudent(
                        task.getTask().getId(),
                        task.getTask().getTaskType().getTitle(),
                        !task.isDraft(),
                        task.getGrade() == null ? null : task.getGrade().getScore(),
                        task.getGrade() == null ? null : task.getGrade().getTags()
                                .stream()
                                .map(tag -> new TagResponse(
                                        tag.getGradeTag().getValue(),
                                        tag.getComment()))
                                .collect(Collectors.toList()),
                        task.getGrade() == null ? null : task.getGrade().getFeedback() != null,
                        task.getReceiptDate(),
                        task.getTask().getBestSolution() != null && task.getTask().getBestSolution().getSolution().getId().equals(task.getId())))
                .collect(Collectors.toList());
    }

    @Transactional
    public void cancelTask(Long taskId) {
        Task task = taskRepository.getTaskByIdAndEntrepreneurJwtId(taskId, AuthUtils.getCurrentJwtId());
        if (isTaskTakenByStudents(task)) {
            throw new TaskAlreadyTakenException("The task is already taken by the students");
        }
        task.setExecutionStatus(TaskExecutionStatus.CANCELED.toString());
    }

    private boolean isTaskTakenByStudents(Task task) {
        return task.getSolutions().size() > 0;
    }

    @Transactional
    public void createGrade(GradeRequest gradeRequest) {
        Solution solution = solutionRepository.getSolutionById(gradeRequest.getSolutionId());
        Task task = solution.getTask();
        if (!task.getEntrepreneur().getJwtId().equals(AuthUtils.getCurrentJwtId())) {
            throw new WrongEntrepreneurException("The task was created by another entrepreneur");
        }
        if (solution.getGrade() != null || solution.getSolutionComplaint() != null) {
            throw new SolutionEvaluationException("The solution has already been evaluated");
        }
        if (solution.isDraft()) {
            throw new SolutionEvaluationException("The solution is draft. It is possible to evaluate only solutions that are not a draft");
        }
        Set<TaskTypeGradeTag> tags = new HashSet<>(taskTypeGradeTagRepository.getTaskTypeGradeTags(gradeRequest.getTaskTypeGradeTags()));
        Grade grade = new Grade(gradeRequest.getScore(), gradeRequest.getFeedback(), tags);
        gradeRepository.saveGrade(grade);
        solution.setGrade(grade);
        try {
            mailServiceConnection.sendSolutionEvaluatedMail(solution.getStudent().getEmail(), task.getId(), task.getTaskType().getTitle());
        } catch (MessagingException e) {
            e.printStackTrace();
        }
        if (isThisCheckLast(task)) {
            task.setExecutionStatus(TaskExecutionStatus.GRADED.toString());
        }
    }

    protected boolean isThisCheckLast(Task task) {
        Integer orderedSolutionsNumber = task.getDecisionsRequiredNumber();
        Integer evaluatedSolutionsNumber = getCountGradeSolutions(task);
        return orderedSolutionsNumber.equals(evaluatedSolutionsNumber);
    }

    public SliderResponse getTaskListForSlider(Long taskTypeId, Long[] viewedTasks) {
        User student = userRepository.getUserByJwtId(AuthUtils.getCurrentJwtId());
        TaskType taskType = taskTypeRepository.getTaskTypeWithTestAndTaskValueAndStudentsById(taskTypeId);
        boolean isStudentVerified = AuthUtils.isAccountVerified();
        boolean isTaskTypeOpenForStudent = taskType.getStudents()
                .stream()
                .anyMatch(user -> user.getJwtId().equals(AuthUtils.getCurrentJwtId()));
        return new SliderResponse(
                taskType.getTitle(),
                taskType.getTest().getStage().getCourse().getId(),
                taskType.getTest().getStage().getCourse().getTitle(),
                taskType.getSolutionTime(),
                taskType.getReward(),
                taskType.getTaskValue().getId(),
                taskType.getTaskValue().getValue(),
                isStudentVerified,
                isTaskTypeOpenForStudent,
                getTasks(isStudentVerified, isTaskTypeOpenForStudent, student, taskType.getId(), viewedTasks)
        );
    }

    private List<TaskDtoForSlider> getTasks(boolean isStudentVerified, boolean isTaskTypeOpenForStudent, User student, Long taskTypeId, Long[] viewedTasks) {
        if (!isStudentVerified || !isTaskTypeOpenForStudent) {
            return null;
        } else {
            List<Task> openTasks = taskRepository.getTasksByTaskTypeId(taskTypeId);
            List<Long> takenTaskIdList = student.getSolutions()
                    .stream()
                    .map(solution -> solution.getTask().getId())
                    .collect(Collectors.toList());
            List<Long> complainedTaskIdList = student.getTaskComplaints()
                    .stream()
                    .map(complaint -> complaint.getTask().getId())
                    .collect(Collectors.toList());
            List<Task> filteredTasks = openTasks
                    .stream()
                    .filter(task -> task.getExecutionStatus().equals(TaskExecutionStatus.IN_PROGRESS.toString()))
                    .filter(task -> Arrays.stream(viewedTasks).noneMatch(id -> id.equals(task.getId())))
                    .filter(task -> takenTaskIdList
                            .stream()
                            .noneMatch(id -> id.equals(task.getId())))
                    .filter(task -> complainedTaskIdList
                            .stream()
                            .noneMatch(id -> id.equals(task.getId())))
                    .sorted(Comparator
                            .comparing(Task::isSpecialTask).reversed()
                            .thenComparingDouble(Task::getPriority))
                    .limit(10)
                    .collect(Collectors.toList());
            return filteredTasks
                    .stream()
                    .map(task -> new TaskDtoForSlider(
                            task.getId(),
                            task.getFormulation(),
                            getFileResponse(task),
                            task.isSpecialTask()
                    ))
                    .collect(Collectors.toList());
        }
    }

    private List<FileResponse> getFileResponse(Task task) {
        return task.getFiles()
                .stream()
                .sorted(Comparator.comparing(File::getOrderNumber))
                .map(file -> new FileResponse(
                        file.getId(),
                        s3Client.getResourceUrl(file.getFilePath(), file.getFileName()),
                        file.getFileName()))
                .collect(Collectors.toList());
    }

    public TaskTypeResponse getTaskTypeResponseForSlider(Long taskTypeId) {
        TaskType taskType = taskTypeRepository.getTaskTypeWithTestAndTaskValueAndStudentsById(taskTypeId);
        return new TaskTypeResponse(
                taskType.getTitle(),
                taskType.getTest().getStage().getCourse().getId(),
                taskType.getTest().getStage().getCourse().getTitle(),
                taskType.getSolutionTime(),
                taskType.getReward(),
                taskType.getTaskValue().getId(),
                taskType.getTaskValue().getValue()
        );
    }

    @Transactional
    public TaskAssignmentResponse taskAssignment(Long taskId) throws IOException {
        User student = AuthUtils.getCurrentUser();
        String jwtId = Objects.requireNonNull(student).getJwtId();
        if (solutionRepository.existsByStudentJwtIdAndTaskId(jwtId, taskId)) {
            throw new TaskAssignmentException("The task is already assigned to the student");
        }
        Task task = taskRepository.getTaskById(taskId);
        if (!task.getExecutionStatus().equals(TaskExecutionStatus.IN_PROGRESS.toString())) {
            throw new WrongTaskExecutionStatusException("The task execution status is not \"In progress\"");
        }
        if (!AuthUtils.isAccountVerified()) {
            throw new UserNotVerifiedException("The account is not verified");
        }
        if (isStudentSolvingTask(solutionRepository.getSolutionsByStudentJwtIdAndDraftIsTrue(jwtId))) {
            throw new StudentAlreadySolvingTaskException("The student is already solving another task");
        }
        if (student.isPermanentlyBlocked()) {
            throw new StudentPermanentlyBlockedException("The student has been permanently blocked");
        }
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime unlockDate = student.getUnlockDate();
        if (unlockDate != null && unlockDate.isAfter(now)) {
            int secondsToUnlock = (int) ChronoUnit.SECONDS.between(now, unlockDate);
            throw new StudentTemporarilyBlockedException("The student has been temporarily blocked", secondsToUnlock);
        }
        Solution solution = new Solution(student, task);
        solutionRepository.saveSolution(solution);
        mixpanelService.createTaskAssignmentEvent(student, taskId);
        task.setPriority((double) ChronoUnit.DAYS.between(now, task.getDeadline()) / (task.getDecisionsRequiredNumber() - task.getSolutions().size() - 1));
        taskRepository.saveTask(task);
        return new TaskAssignmentResponse(
                solution.getValue(),
                solution.getReceiptDate()
        );
    }

    private boolean isStudentSolvingTask(List<Solution> userDrafts) {
        return userDrafts
                .stream()
                .anyMatch(draft -> draft
                        .getReceiptDate()
                        .plusSeconds(draft
                                .getTask()
                                .getTaskType()
                                .getSolutionTime()
                                .getSeconds())
                        .isAfter(LocalDateTime.now()));
    }

    public TaskResponseWithSolutionList getTaskWithSolutionList(Long taskId) {
        Task task = taskRepository.getTaskById(taskId);
        if (!task.getEntrepreneur().getJwtId().equals(AuthUtils.getCurrentJwtId())) {
            throw new WrongEntrepreneurException("The task was created by another entrepreneur");
        }
        BestSolution bestSolution = task.getBestSolution();
        return new TaskResponseWithSolutionList(
                task.getTaskType().getTitle(),
                TaskExecutionStatus.valueOf(task.getExecutionStatus()).getValue(),
                task.getDeadline(),
                task.getDecisionsRequiredNumber(),
                (int) task.getSolutions()
                        .stream()
                        .filter(solution -> !solution.isDraft())
                        .filter(solution -> solution.getDeliveryDate() != null)
                        .count(),
                getCountGradeSolutions(task),
                task.getFormulation(),
                getFileResponse(task),
                task.getSolutions()
                        .stream()
                        .filter(solution -> !solution.isDraft())
                        .sorted(Comparator
                                        .comparing(Solution::getDeliveryDate)
                                        .thenComparing(Solution::getId))
                        .map(solution -> new SolutionDto(
                                solution.getId(),
                                solution.getGrade() != null ? solution.getGrade().getScore() : null,
                                solution.getSolutionComplaint() != null))
                        .collect(Collectors.toList()),
                bestSolution == null ? null : bestSolution.getSolution().getId()
        );
    }

    public CurrentTaskResponse getCurrentTask() {
        try {
            CurrentTaskEntity response = taskRepository.getCurrentTask(AuthUtils.getCurrentJwtId());
            return new CurrentTaskResponse(
                    response.getReceiptDate(),
                    response.getSolutionTime(),
                    response.getTaskId()
            );
        } catch (EmptyResultDataAccessException ex) {
            return null;
        }
    }

    @Transactional
    public void chooseBestSolution(Long solutionId) {
        Solution solution = solutionRepository.getSolutionById(solutionId);
        Task task = solution.getTask();
        if (!task.getEntrepreneur().getJwtId().equals(AuthUtils.getCurrentJwtId())) {
            throw new WrongEntrepreneurException("The task was created by another entrepreneur");
        }
        if (task.getBestSolution() != null) {
            throw new BestSolutionAlreadyChosenException("The best solution has already been chosen");
        }
        if (!TaskExecutionStatus.GRADED.toString().equals(task.getExecutionStatus())) {
            throw new WrongTaskExecutionStatusException("All solutions must be evaluated");
        }
        if (!task.isSpecialTask()) {
            throw new TaskIsNotSpecialException("The task is not marked like a \"special\"");
        }
        boolean isSolutionWithGradeExist = task.getSolutions()
                .stream()
                .anyMatch(sol -> sol.getGrade() != null);
        if (!isSolutionWithGradeExist) {
            throw new ComplainsToAllSolutionsException("All solutions of this task has been complained");
        }
        BestSolution bestSolution = new BestSolution(solution.getStudent(), solution);
        bestSolutionRepository.saveBestSolution(bestSolution);
        task.setBestSolution(bestSolution);
        task.setExecutionStatus(TaskExecutionStatus.EVALUATED.toString());
    }
}