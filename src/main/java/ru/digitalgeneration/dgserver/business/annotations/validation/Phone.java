package ru.digitalgeneration.dgserver.business.annotations.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Documented
@Constraint(validatedBy = PhoneConstraintValidator.class)
@Retention(RUNTIME)
@Target({FIELD, PARAMETER})
public @interface Phone {
    String message() default "The phone format must match the template +7 (xxx) xxx-xx-xx, where x are digits";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}