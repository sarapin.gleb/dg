package ru.digitalgeneration.dgserver.business.annotations.validation;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
@RequiredArgsConstructor
public class PhoneConstraintValidator implements ConstraintValidator<Phone, String> {

    private ConstraintValidatorContext constraintValidatorContext;

    @Override
    public boolean isValid(String phoneField, ConstraintValidatorContext constraintValidatorContext) {
        if (phoneField == null) {
            return false;
        }
        return phoneField.matches("^\\+7\\s\\(\\d{3}\\)\\s\\d{3}-\\d{2}-\\d{2}$");
    }

    public boolean phoneValidation(String phone) {
        return isValid(phone, constraintValidatorContext);
    }
}