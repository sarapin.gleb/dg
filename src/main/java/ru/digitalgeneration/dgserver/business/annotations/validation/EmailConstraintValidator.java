package ru.digitalgeneration.dgserver.business.annotations.validation;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
@RequiredArgsConstructor
public class EmailConstraintValidator implements ConstraintValidator<Email, String> {

    private ConstraintValidatorContext constraintValidatorContext;

    @Override
    public boolean isValid(String emailField, ConstraintValidatorContext constraintValidatorContext) {
        if (emailField == null) {
            return false;
        }
        return emailField.matches(".+@.+\\..+");
    }

    public boolean emailValidation(String email) {
        return isValid(email, constraintValidatorContext);
    }
}