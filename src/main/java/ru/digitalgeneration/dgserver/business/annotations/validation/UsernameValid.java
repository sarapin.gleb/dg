package ru.digitalgeneration.dgserver.business.annotations.validation;

import org.apache.commons.lang3.ObjectUtils;
import ru.digitalgeneration.dgserver.business.interfaces.HasUsernameAndType;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Documented
@Constraint(validatedBy = UsernameValid.UsernameValidator.class)
@Retention(RUNTIME)
@Target({TYPE})
public @interface UsernameValid {

    String message() default "Invalid login format";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    class UsernameValidator implements ConstraintValidator<UsernameValid, HasUsernameAndType> {

        @Override
        public boolean isValid(HasUsernameAndType hasUsernameAndType, ConstraintValidatorContext constraintValidatorContext) {
            if (hasUsernameAndType == null) {
                return false;
            }
            var username = hasUsernameAndType.getUsername();
            var type = hasUsernameAndType.getLoginType();
            if (ObjectUtils.anyNull(username, type)) {
                return false;
            }
            switch (type) {
                case EMAIL:
                    return username.matches(".+@.+\\..+");
                case PHONE:
                    return username.matches("^\\+7\\s\\(\\d{3}\\)\\s\\d{3}-\\d{2}-\\d{2}$");
                default:
                    return false;
            }
        }
    }
}