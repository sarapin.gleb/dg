package ru.digitalgeneration.dgserver.business.annotations.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Documented
@Constraint(validatedBy = EmailConstraintValidator.class)
@Retention(RUNTIME)
@Target({FIELD, PARAMETER})
public @interface Email {
    String message() default "The email format is wrong";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}