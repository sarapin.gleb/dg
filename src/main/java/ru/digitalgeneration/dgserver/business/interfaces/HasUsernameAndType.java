package ru.digitalgeneration.dgserver.business.interfaces;

import ru.digitalgeneration.dgserver.business.entities.enums.LoginType;

public interface HasUsernameAndType {

    LoginType getLoginType();

    String getUsername();
}