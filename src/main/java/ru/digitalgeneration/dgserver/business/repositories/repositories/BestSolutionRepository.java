package ru.digitalgeneration.dgserver.business.repositories.repositories;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import ru.digitalgeneration.dgserver.business.entities.BestSolution;
import ru.digitalgeneration.dgserver.business.repositories.jpa_repositories.BestSolutionJpaRepository;

import java.util.List;

@Repository
@RequiredArgsConstructor
public class BestSolutionRepository {

    private final BestSolutionJpaRepository bestSolutionRepository;

    public void saveBestSolution(BestSolution bestSolution) {
        bestSolutionRepository.saveAndFlush(bestSolution);
    }

    public List<BestSolution> getUserBestSolutions(String jwtId) {
        return bestSolutionRepository.findAllByStudentJwtIdOrderBySolutionDateDesc(jwtId);
    }

    public List<BestSolution> getUserBestSolutions(Integer openId) {
        return bestSolutionRepository.findAllByStudentOpenIdOrderBySolutionDateDesc(openId);
    }
}