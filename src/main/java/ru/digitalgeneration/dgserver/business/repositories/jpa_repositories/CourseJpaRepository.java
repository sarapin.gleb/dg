package ru.digitalgeneration.dgserver.business.repositories.jpa_repositories;

import lombok.NonNull;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.digitalgeneration.dgserver.business.entities.Course;

import java.util.List;

@Repository
public interface CourseJpaRepository extends JpaRepository<Course, Long> {

    @Override
    @NonNull
    @EntityGraph(type = EntityGraph.EntityGraphType.FETCH, attributePaths = {"image", "tags"})
    List<Course> findAll();
}