package ru.digitalgeneration.dgserver.business.repositories.jpa_repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.digitalgeneration.dgserver.business.entities.Feature;
import java.util.List;

@Repository
public interface FeatureJpaRepository extends JpaRepository<Feature, Long> {

    List<Feature> findAllByOrderByReleaseDate();
}