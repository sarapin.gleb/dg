package ru.digitalgeneration.dgserver.business.repositories.repositories;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import ru.digitalgeneration.dgserver.business.entities.TaskTypeGradeTag;
import ru.digitalgeneration.dgserver.business.repositories.jpa_repositories.TaskTypeGradeTagJpaRepository;

import java.util.List;

@Repository
@RequiredArgsConstructor
public class TaskTypeGradeTagRepository {

    private final TaskTypeGradeTagJpaRepository repository;

    public List<TaskTypeGradeTag> getTaskTypeGradeTags(List<Long> taskTypeGradeTags) {
        return repository.findAllByIdIn(taskTypeGradeTags);
    }
}