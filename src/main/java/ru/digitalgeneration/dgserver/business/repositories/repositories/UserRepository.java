package ru.digitalgeneration.dgserver.business.repositories.repositories;

import com.mashape.unirest.http.exceptions.UnirestException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.webjars.NotFoundException;
import ru.digitalgeneration.dgserver.business.annotations.UserProfileChange;
import ru.digitalgeneration.dgserver.business.connections.BrokerConnection;
import ru.digitalgeneration.dgserver.business.dto.UserEmailUuidDto;
import ru.digitalgeneration.dgserver.business.dto.responses.BalanceResponse;
import ru.digitalgeneration.dgserver.business.dto.responses.CodeComparingForPasswordRecoveryResponse;
import ru.digitalgeneration.dgserver.business.entities.User;
import ru.digitalgeneration.dgserver.business.entities.sql_mapping_entities.LeaderboardEntity;
import ru.digitalgeneration.dgserver.business.entities.sql_mapping_entities.UserGradeTagEntity;
import ru.digitalgeneration.dgserver.business.exceptions.not_found_exceptions.UserNotFoundException;
import ru.digitalgeneration.dgserver.business.projection.UserEmailUuidProjection;
import ru.digitalgeneration.dgserver.business.repositories.jpa_repositories.UserJpaRepository;
import ru.digitalgeneration.dgserver.security.logic.dto.TokenPayload;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Repository
@RequiredArgsConstructor
public class UserRepository {

    private final UserJpaRepository userRepository;
    private final BrokerConnection brokerConnection;
    @PersistenceContext
    private final EntityManager entityManager;

    public User saveUserChanges(User user) {
        return userRepository.saveAndFlush(user);
    }

    public User getUserByJwtId(String jwtId) {
        return userRepository.findByJwtId(jwtId).orElseThrow(UserNotFoundException::new);
    }

    public User getUserByOpenId(Integer openId) {
        return userRepository.findByOpenId(openId).orElseThrow(UserNotFoundException::new);
    }

    public User getUserWithSolutionsByJwtId(String jwtId) {
        return userRepository.findUserWithSolutionsByJwtId(jwtId).orElseThrow(UserNotFoundException::new);
    }

    public User getUserWithSolutionsWithGradesByJwtId(String jwtId) {
        return userRepository.findUserWithSolutionsWithGradesByJwtId(jwtId).orElseThrow(UserNotFoundException::new);
    }

    public User getUserWithTasksByJwtId(String jwtId) {
        return userRepository.findUserWithTasksByJwtId(jwtId).orElseThrow(UserNotFoundException::new);
    }

    public User getUserWithStudentCoursesByJwtId(String jwtId) {
        return userRepository.findUserWithStudentCoursesByJwtId(jwtId).orElseThrow(UserNotFoundException::new);
    }

    @Transactional
    public TokenPayload getTokenPayloadByEmail(String email) { //TODO перенести логику в сервис
        User user = userRepository.findByEmail(email).orElseThrow(UserNotFoundException::new);
        user.setLastLoginDate(LocalDateTime.now());
        userRepository.saveAndFlush(user);
        return new TokenPayload(user.getRoles());
    }

    @Transactional
    public TokenPayload getTokenPayloadByPhone(String phone) { //TODO перенести логику в сервис
        User user = userRepository.findByPhone(phone).orElseThrow(UserNotFoundException::new);
        user.setLastLoginDate(LocalDateTime.now());
        userRepository.saveAndFlush(user);
        return new TokenPayload(user.getRoles());
    }

    @Transactional
    public TokenPayload getTokenPayloadByVkId(Integer vkUserId) { //TODO перенести логику в сервис
        User user = userRepository.findByVkId(vkUserId).orElseThrow(UserNotFoundException::new);
        user.setLastLoginDate(LocalDateTime.now());
        userRepository.saveAndFlush(user);
        return new TokenPayload(user.getRoles());
    }

    public User getUserByUuid(UUID uuid) {
        return userRepository.findByUuid(uuid).orElseThrow(UserNotFoundException::new);
    }

    public User getUserByEmail(String email) {
        return userRepository.findByEmail(email).orElseThrow(UserNotFoundException::new);
    }

    public User getUserByPhone(String phone) {
        return userRepository.findByPhone(phone).orElseThrow(UserNotFoundException::new);
    }

    public UUID getUuidByEmail(String email) {
        return userRepository
                .findUserUuidByEmail(email)
                .orElseThrow(() -> new NotFoundException("User with email: " + email + " not found"))
                .getUuid();
    }

    public UserEmailUuidDto getUserEmailAndUuidByJwtId(String jwtId) { //TODO перенести логику в сервис
        UserEmailUuidProjection projection = userRepository.findUserEmailUuidByJwtId(jwtId);
        String email = projection.getEmail().orElseThrow(() -> new NotFoundException("Email not found"));
        return new UserEmailUuidDto(email, projection.getUuid());
    }

    public BalanceResponse getBalance(String jwtId) { //TODO перенести логику в сервис
        User user = userRepository.findByJwtId(jwtId).orElseThrow(UserNotFoundException::new);
        return new BalanceResponse(user.getPointBalance());
    }

    @Transactional
    @UserProfileChange
    public User changePasswordInPersonalArea(String jwtId, String newHashedPassword) { //TODO перенести логику в сервис
        User user = userRepository.findByJwtId(jwtId).orElseThrow(UserNotFoundException::new);
        user.setPassword(newHashedPassword);
        return userRepository.saveAndFlush(user);
    }

    public boolean isPhoneExist(String phone) {
        return userRepository.existsByPhone(phone);
    }

    @Transactional
    public void generatePhoneVerificationCode(String jwtId) { //TODO перенести логику в сервис
        User user = userRepository.findByJwtId(jwtId).orElseThrow(UserNotFoundException::new);
        String code = generateRandomCode();
        sendPhoneVerificationSms(user.getPhone(), code);
        user.setLastNonceVerificationCode(code);
        userRepository.saveAndFlush(user);
    }

    private String generateRandomCode() {
        int min = 1000;
        int max = 9999;
        int random = (int) Math.floor(Math.random() * (max - min + 1) + min);
        return Integer.toString(random);
    }

    private void sendPhoneVerificationSms(String phone, String code) {
        try {
            brokerConnection.sendPhoneVerificationSms(phone, code);
        } catch (UnirestException e) {
            e.printStackTrace();
        }
    }

    @Transactional
    public void generatePasswordRecoveryCode(String phone) { //TODO перенести логику в сервис
        User user = userRepository.findByPhone(phone).orElseThrow(UserNotFoundException::new);
        String code = generateRandomCode();
        sendPasswordRecoverySms(phone, code);
        user.setLastNonceVerificationCode(code);
        userRepository.saveAndFlush(user);
    }

    private void sendPasswordRecoverySms(String phone, String code) {
        try {
            brokerConnection.sendPasswordRecoverySms(phone, code);
        } catch (UnirestException e) {
            e.printStackTrace();
        }
    }

    @Transactional
    public CodeComparingForPasswordRecoveryResponse compareCodesForPasswordRecovering(String phone, String code) { //TODO перенести логику в сервис
        User user = userRepository.findByPhone(phone).orElseThrow(UserNotFoundException::new);
        if (user.getLastNonceVerificationCode() != null && user.getLastNonceVerificationCode().equals(code)) {
            user.setLastNonceVerificationCode(null);
            return new CodeComparingForPasswordRecoveryResponse(true, user.getUuid());
        }
        return new CodeComparingForPasswordRecoveryResponse(false, null);
    }

    public List<UserGradeTagEntity> getUserInfoWithGradeTagCount(String jwtId) {
        Query query = entityManager.createNativeQuery(
                "SELECT " +
                        "gt.value AS tag, " +
                        "count(*) AS count FROM solutions s " +
                        "JOIN users u on s.student_id = u.id " +
                        "JOIN tasks t on s.task_id = t.id " +
                        "JOIN task_types tt on t.task_type_id = tt.id " +
                        "JOIN task_types_grade_tags ttgt on tt.id = ttgt.task_type_id " +
                        "JOIN grade_tags gt on ttgt.grade_tag_id = gt.id " +
                        "WHERE u.jwt_id = (:jwtId) AND grade_id IS NOT NULL " +
                        "GROUP BY tag", "UserGradeTagMapping"
        );
        query.setParameter("jwtId", jwtId);
        return query.getResultList();
    }

    public List<LeaderboardEntity> getLeaderboard() {
        Query query = entityManager.createNativeQuery(
                "SELECT " +
                        "open_id, " +
                        "f_name, " +
                        "s_name, " +
                        "m_name, " +
                        "count(*) AS best_solutions_count, " +
                        "row_number() over (ORDER BY count(*) DESC) AS position " +
                        "FROM best_solutions " +
                        "JOIN users u on u.id = best_solutions.student_id " +
                        "GROUP BY open_id, f_name, s_name, m_name", "LeaderboardMapping"
        );
        return query.getResultList();
    }

    public List<LeaderboardEntity> getMonthlyLeaderboard() {
        Query query = entityManager.createNativeQuery(
                "SELECT " +
                        "open_id, " +
                        "f_name, " +
                        "s_name, " +
                        "m_name, " +
                        "count(*) AS best_solutions_count, " +
                        "row_number() over (ORDER BY count(*) DESC) AS position " +
                        "FROM best_solutions " +
                        "JOIN users u on u.id = best_solutions.student_id " +
                        "WHERE solution_date > now() - INTERVAL '1 MONTH' " +
                        "GROUP BY open_id, f_name, s_name, m_name", "LeaderboardMapping"
        );
        return query.getResultList();
    }
}