package ru.digitalgeneration.dgserver.business.repositories.repositories;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.digitalgeneration.dgserver.business.dto.requests.FavoritePageRequest;
import ru.digitalgeneration.dgserver.business.entities.FavoritePage;
import ru.digitalgeneration.dgserver.business.projection.UserEmailPhoneProjection;
import ru.digitalgeneration.dgserver.business.repositories.jpa_repositories.FavoritePageJpaRepository;
import ru.digitalgeneration.dgserver.business.repositories.jpa_repositories.UserJpaRepository;

@Repository
@RequiredArgsConstructor
public class FavoritePageRepository {

    private final FavoritePageJpaRepository favoritePageRepository;
    private final UserJpaRepository userRepository;

    @Transactional
    public void saveEmailAndUserFavoritePage(FavoritePageRequest favoritePageRequest) { //TODO перенести логику в сервис
        FavoritePage favoritePage = new FavoritePage(favoritePageRequest.getEmail(), favoritePageRequest.getPageUrl());
        favoritePageRepository.saveAndFlush(favoritePage);
    }

    @Transactional
    public void saveAuthorizedUserFavoritePage(String pageUrl, String jwtId) { //TODO перенести логику в сервис
        UserEmailPhoneProjection projection = userRepository.findUserEmailPhoneByJwtId(jwtId);
        String contact = projection.getEmail() == null ? projection.getPhone() : projection.getEmail();
        FavoritePage favoritePage = new FavoritePage(contact, pageUrl);
        favoritePageRepository.saveAndFlush(favoritePage);
    }
}