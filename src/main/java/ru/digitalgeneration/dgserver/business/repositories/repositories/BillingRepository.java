package ru.digitalgeneration.dgserver.business.repositories.repositories;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import org.webjars.NotFoundException;
import ru.digitalgeneration.dgserver.business.entities.Transaction;
import ru.digitalgeneration.dgserver.business.repositories.jpa_repositories.BillingJpaRepository;

@Repository
@RequiredArgsConstructor
public class BillingRepository {

    private final BillingJpaRepository billingRepository;

    public Transaction saveTransaction(Transaction transaction) {
        return billingRepository.saveAndFlush(transaction);
    }

    public Transaction getTransactionByPaymentId(String paymentId) {
        return billingRepository.findByPaymentId(paymentId).orElseThrow(() -> new NotFoundException("Transaction not found"));
    }
}