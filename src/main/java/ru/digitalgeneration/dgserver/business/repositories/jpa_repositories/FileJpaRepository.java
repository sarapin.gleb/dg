package ru.digitalgeneration.dgserver.business.repositories.jpa_repositories;

import lombok.NonNull;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.digitalgeneration.dgserver.business.entities.File;

import java.util.List;

@Repository
public interface FileJpaRepository extends JpaRepository<File, Long> {

    @Override
    @NonNull
    @EntityGraph(type = EntityGraph.EntityGraphType.FETCH, attributePaths = {"solution.task", "solution.student"})
    List<File> findAllById(@NonNull Iterable<Long> fileIdList);
}