package ru.digitalgeneration.dgserver.business.repositories.repositories;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import ru.digitalgeneration.dgserver.business.entities.File;
import ru.digitalgeneration.dgserver.business.repositories.jpa_repositories.FileJpaRepository;

import java.util.List;

@Repository
@RequiredArgsConstructor
public class FileRepository {

    private final FileJpaRepository repository;

    public List<File> saveFiles(List<File> fileEntityList) {
        return repository.saveAll(fileEntityList);
    }

    public List<File> getFilesByIdList(List<Long> fileIdList) {
        return repository.findAllById(fileIdList);
    }

    public void removeFiles(List<File> files) {
        repository.deleteAll(files);
    }
}