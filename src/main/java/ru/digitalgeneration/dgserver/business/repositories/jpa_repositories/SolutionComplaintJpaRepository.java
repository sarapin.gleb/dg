package ru.digitalgeneration.dgserver.business.repositories.jpa_repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.digitalgeneration.dgserver.business.entities.SolutionComplaint;

@Repository
public interface SolutionComplaintJpaRepository extends JpaRepository<SolutionComplaint, Long> {
}