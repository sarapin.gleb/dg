package ru.digitalgeneration.dgserver.business.repositories.repositories;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import ru.digitalgeneration.dgserver.business.entities.Course;
import ru.digitalgeneration.dgserver.business.entities.sql_mapping_entities.CourseTagEntity;
import ru.digitalgeneration.dgserver.business.entities.sql_mapping_entities.StageResponseEntity;
import ru.digitalgeneration.dgserver.business.exceptions.not_found_exceptions.CourseNotFoundException;
import ru.digitalgeneration.dgserver.business.projection.StudentCourseDataProjection;
import ru.digitalgeneration.dgserver.business.repositories.jpa_repositories.CourseJpaRepository;
import ru.digitalgeneration.dgserver.business.repositories.jpa_repositories.StudentCourseJpaRepository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Repository
@RequiredArgsConstructor
public class CourseRepository {

    private final CourseJpaRepository courseRepository;
    private final StudentCourseJpaRepository studentCourseRepository;
    @PersistenceContext
    private final EntityManager entityManager;

    public Course getCourseById(Long courseId) {
        return courseRepository.findById(courseId).orElseThrow(CourseNotFoundException::new);
    }

    public StudentCourseDataProjection getStudentCourseData(Long courseId, String jwtId) {
        return studentCourseRepository.findStudentCourseDataProjectionByCourseIdAndUserJwtId(courseId, jwtId);
    }

    public StageResponseEntity getStageById(Long stageId, String jwtId) {
        Query query = entityManager.createNativeQuery(
                "SELECT " +
                        "s.id AS stages_id, " +
                        "s.title AS stages_title, " +
                        "s.description, " +
                        "s.course_id, " +
                        "c.title AS course_title, " +
                        "CASE " +
                        "WHEN l.id IS NOT NULL THEN 'lecture' " +
                        "WHEN t.id IS NOT NULL THEN 'test' " +
                        "WHEN ip.id IS NOT NULL THEN 'intermediate_page' " +
                        "WHEN cp.id IS NOT NULL THEN  'congratulation_page' END AS stage_type, " +
                        "video, " +
                        "questions, " +
                        "coalesce(ip.task_type, cp.task_type) AS task_type_id, " +
                        "sc.last_open_stage, " +
                        "sc.last_viewed_stage, " +
                        "sc.timing, " +
                        "s.prev_stage_id, " +
                        "s_prev.title AS prev_stage_title, " +
                        "s.next_stage_id, " +
                        "s_next.title AS next_stage_title FROM stages s " +
                        "LEFT JOIN lectures l on s.id = l.stage_id " +
                        "LEFT JOIN tests t on s.id = t.stage_id " +
                        "LEFT JOIN intermediate_pages ip on s.id = ip.stage_id " +
                        "LEFT JOIN congratulation_pages cp on s.id = cp.stage_id " +
                        "LEFT JOIN stages s_prev on s.prev_stage_id = s_prev.id " +
                        "LEFT JOIN stages s_next on s.next_stage_id = s_next.id " +
                        "JOIN courses c on c.id = s.course_id " +
                        "JOIN students_courses sc on c.id = sc.courses_id " +
                        "JOIN users u on u.id = sc.users_id " +
                        "WHERE s.id = (:stage_id) AND u.jwt_id = (:jwtId)", "StageResponseMapping"
        );
        query.setParameter("stage_id", stageId);
        query.setParameter("jwtId", jwtId);
        return (StageResponseEntity) query.getSingleResult();
    }

    public List<CourseTagEntity> getUserCoursesByTags(String[] tags, String jwtId) {
        String queryRequestBeginning = "SELECT DISTINCT courses.id AS c_id, " +
                "title, " +
                "null AS pn, " +
                "true AS purchase_status, " +
                "image_path, " +
                "image_file_name, " +
                "author, " +
                "all_courses_tags.tags_set AS tag_values, " +
                "price, " +
                "completeness_status, " +
                "sc.last_action_date AS last_action_date FROM courses " +
                "JOIN images i on i.id = courses.image_id " +
                "JOIN users u on u.jwt_id = ?0 " +
                "JOIN students_courses sc on sc.users_id = u.id AND sc.courses_id = courses.id " +
                "JOIN courses_tags ct on courses.id = ct.course_id " +
                "JOIN tags t on t.id = ct.tag_id " +
                "JOIN (SELECT course_id, string_agg(value, ', ') AS tags_set FROM courses_tags JOIN tags t on t.id = courses_tags.tag_id GROUP BY course_id) all_courses_tags on courses.id = all_courses_tags.course_id " +
                "WHERE jwt_id = ?0 ";
        Query query;
        if (tags.length > 0) {
            StringBuilder queryRequestWithTail = new StringBuilder(queryRequestBeginning).append("AND (");
            List<String> tagList = Arrays.stream(tags).collect(Collectors.toList());
            AtomicInteger i = new AtomicInteger(1);
            tagList.forEach(tag -> queryRequestWithTail.append("t.value = ?").append(i.getAndIncrement()).append(" OR "));
            String queryRequestWithoutEnd = queryRequestWithTail.substring(0, queryRequestWithTail.length() - 4);
            String queryRequest = queryRequestWithoutEnd + ") ORDER BY last_action_date DESC";
            query = entityManager.createNativeQuery(queryRequest, "CourseTagMapping");
            i.set(1);
            tagList.forEach(tag -> query.setParameter(i.getAndIncrement(), tag));
        } else {
            String queryRequest = queryRequestBeginning + " ORDER BY last_action_date DESC";
            query = entityManager.createNativeQuery(queryRequest, "CourseTagMapping");
        }
        query.setParameter(0, jwtId);
        return query.getResultList();
    }

    public List<CourseTagEntity> getCoursesByTags(String[] tags, String jwtId) {
        String lastActionDate;
        String completenessStatus;
        String userJoins;
        if (!jwtId.equals("anonymousUser")) {
            lastActionDate = "sc.last_action_date";
            completenessStatus = "completeness_status";
            userJoins = "JOIN users u on u.jwt_id = ?0 LEFT JOIN students_courses sc on sc.users_id = u.id AND sc.courses_id = courses.id ";
        } else {
            lastActionDate = null;
            completenessStatus = Boolean.FALSE.toString();
            userJoins = "";
        }
        String queryRequestBeginning = "SELECT DISTINCT courses.id AS c_id, " +
                "purchase_frequency.purchases_number AS pn, " +
                "courses.id = ANY (SELECT courses_id FROM students_courses JOIN users u on u.id = students_courses.users_id WHERE jwt_id = ?0) AS purchase_status, " +
                "title, " +
                "image_path, " +
                "image_file_name, " +
                "author, " +
                "all_courses_tags.tags_set AS tag_values, " +
                "price, " +
                completenessStatus + " AS completeness_status, " +
                lastActionDate + " AS last_action_date FROM courses " +
                "JOIN images i on i.id = courses.image_id " +
                userJoins +
                "LEFT JOIN (SELECT courses_id, count(users_id) AS purchases_number FROM students_courses GROUP BY courses_id) purchase_frequency on courses.id = purchase_frequency.courses_id " +
                "JOIN courses_tags ct on courses.id = ct.course_id " +
                "JOIN tags t on t.id = ct.tag_id " +
                "JOIN (SELECT course_id, string_agg(value, ', ') AS tags_set FROM courses_tags JOIN tags t on t.id = courses_tags.tag_id GROUP BY course_id) all_courses_tags on courses.id = all_courses_tags.course_id ";
        Query query;
        if (tags.length > 0) {
            StringBuilder queryRequestWithTail = new StringBuilder(queryRequestBeginning).append("WHERE ");
            List<String> tagList = Arrays.stream(tags).collect(Collectors.toList());
            AtomicInteger i = new AtomicInteger(1);
            tagList.forEach(tag -> queryRequestWithTail.append("t.value = ?").append(i.getAndIncrement()).append(" OR "));
            String queryRequestWithoutEnd = queryRequestWithTail.substring(0, queryRequestWithTail.length() - 4);
            String queryRequest = queryRequestWithoutEnd + " ORDER BY purchase_status, last_action_date DESC, pn DESC NULLS LAST";
            query = entityManager.createNativeQuery(queryRequest, "CourseTagMapping");
            i.set(1);
            tagList.forEach(tag -> query.setParameter(i.getAndIncrement(), tag));
        } else {
            String queryRequest = queryRequestBeginning + " ORDER BY purchase_status, last_action_date DESC, pn DESC NULLS LAST";
            query = entityManager.createNativeQuery(queryRequest, "CourseTagMapping");
        }
        query.setParameter(0, jwtId);
        return query.getResultList();
    }

    public boolean isCourseBoughtByUser(Long courseId, String jwtId) {
        return studentCourseRepository.existsStudentCourseByCourseIdAndUserJwtId(courseId, jwtId);
    }

    public List<Course> getCourses() {
        return courseRepository.findAll();
    }
}