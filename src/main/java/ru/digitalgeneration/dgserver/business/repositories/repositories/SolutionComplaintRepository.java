package ru.digitalgeneration.dgserver.business.repositories.repositories;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import ru.digitalgeneration.dgserver.business.entities.SolutionComplaint;
import ru.digitalgeneration.dgserver.business.repositories.jpa_repositories.SolutionComplaintJpaRepository;

@Repository
@RequiredArgsConstructor
public class SolutionComplaintRepository {

    private final SolutionComplaintJpaRepository repository;

    public void saveComplaint(SolutionComplaint complaint) {
        repository.saveAndFlush(complaint);
    }
}