package ru.digitalgeneration.dgserver.business.repositories.repositories;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import ru.digitalgeneration.dgserver.business.entities.TaskComplaint;
import ru.digitalgeneration.dgserver.business.repositories.jpa_repositories.TaskComplaintJpaRepository;

@Repository
@RequiredArgsConstructor
public class TaskComplaintRepository {

    private final TaskComplaintJpaRepository repository;

    public void saveComplaint(TaskComplaint complaint) {
        repository.saveAndFlush(complaint);
    }

    public boolean isComplaintExist(String jwtId, Long taskId) {
        return repository.existsByStudentJwtIdAndTaskId(jwtId, taskId);
    }
}