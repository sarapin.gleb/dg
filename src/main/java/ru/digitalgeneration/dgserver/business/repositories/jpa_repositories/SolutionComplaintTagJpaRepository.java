package ru.digitalgeneration.dgserver.business.repositories.jpa_repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.digitalgeneration.dgserver.business.entities.SolutionComplaintTag;

import java.util.List;

@Repository
public interface SolutionComplaintTagJpaRepository extends JpaRepository<SolutionComplaintTag, Long> {

    List<SolutionComplaintTag> findAllByOrderById();

    List<SolutionComplaintTag> findAllByIdIn(List<Long> tagIdList);
}