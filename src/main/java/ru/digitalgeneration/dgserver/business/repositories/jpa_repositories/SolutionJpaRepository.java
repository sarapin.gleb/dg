package ru.digitalgeneration.dgserver.business.repositories.jpa_repositories;

import lombok.NonNull;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.digitalgeneration.dgserver.business.entities.Solution;

import java.util.List;
import java.util.Optional;

@Repository
public interface SolutionJpaRepository extends JpaRepository<Solution, Long> {

    @Override
    @NonNull
    @EntityGraph(type = EntityGraph.EntityGraphType.FETCH, attributePaths = {"task.taskType", "grade.tags.gradeTag"})
    Optional<Solution> findById(@NonNull Long id);

    @EntityGraph(type = EntityGraph.EntityGraphType.FETCH, attributePaths = {"task.taskType", "files"})
    Optional<Solution> findByStudentJwtIdAndTaskId(String jwtId, Long taskId);

    @EntityGraph(type = EntityGraph.EntityGraphType.FETCH, attributePaths = {"task.taskType", "files"})
    Optional<Solution> findByStudentJwtIdAndTaskIdAndDraftIsTrue(String jwtId, Long taskId);

    @EntityGraph(type = EntityGraph.EntityGraphType.FETCH, attributePaths = {"task.taskType", "task.bestSolution.solution", "grade.tags.gradeTag"})
    List<Solution> findAllByStudentJwtIdAndDraftIsFalse(String jwtId);

    @EntityGraph(type = EntityGraph.EntityGraphType.FETCH, attributePaths = {"task.taskType"})
    List<Solution> findAllByStudentJwtIdAndDraftIsTrue(String jwtId);

    boolean existsByStudentJwtIdAndTaskId(String jwtId, Long taskId);

    boolean existsByTaskEntrepreneurJwtIdAndId(String jwtId, Long solutionId);

    List<Solution> findAllByTaskIdIn(List<Long> taskIdList);
}