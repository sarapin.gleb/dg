package ru.digitalgeneration.dgserver.business.repositories.jpa_repositories;

import lombok.NonNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.digitalgeneration.dgserver.business.entities.Transaction;

import java.util.List;
import java.util.Optional;

@Repository
public interface BillingJpaRepository extends JpaRepository<Transaction, Long> {

    @Override
    @NonNull
    <S extends Transaction> S saveAndFlush(@NonNull S s);

    List<Transaction> findAllByPaymentStatusIs(String paymentStatus);

    Optional<Transaction> findByPaymentId(String paymentId);
}