package ru.digitalgeneration.dgserver.business.repositories.jpa_repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.digitalgeneration.dgserver.business.entities.TaskComplaint;

@Repository
public interface TaskComplaintJpaRepository extends JpaRepository<TaskComplaint, Long> {

    boolean existsByStudentJwtIdAndTaskId(String jwtId, Long taskId);
}