package ru.digitalgeneration.dgserver.business.repositories.repositories;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import ru.digitalgeneration.dgserver.business.entities.Stage;
import ru.digitalgeneration.dgserver.business.exceptions.not_found_exceptions.StageNotFoundException;
import ru.digitalgeneration.dgserver.business.repositories.jpa_repositories.StageJpaRepository;

@Repository
@RequiredArgsConstructor
public class StageRepository {

    private final StageJpaRepository repository;

    public Stage getStageById(Long stageId) {
        return repository.findById(stageId).orElseThrow(StageNotFoundException::new);
    }
}