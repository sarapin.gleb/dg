package ru.digitalgeneration.dgserver.business.repositories.jpa_repositories;

import lombok.NonNull;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.digitalgeneration.dgserver.business.entities.User;
import ru.digitalgeneration.dgserver.business.projection.UserEmailPhoneProjection;
import ru.digitalgeneration.dgserver.business.projection.UserEmailUuidProjection;
import ru.digitalgeneration.dgserver.business.projection.UserJwtIdProjection;
import ru.digitalgeneration.dgserver.business.projection.UserUuidProjection;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface UserJpaRepository extends JpaRepository<User, Long> {

    @Override
    @NonNull
    @EntityGraph(type = EntityGraph.EntityGraphType.FETCH, attributePaths = {"utmParameters"})
    <S extends User> S saveAndFlush(@NonNull S s);

    @EntityGraph(type = EntityGraph.EntityGraphType.FETCH, attributePaths = {"utmParameters"})
    Optional<User> findByEmail(String email);

    Optional<User> findByPhone(String phone);

    Optional<User> findByVkId(Integer vkId);

    Optional<User> findByOpenId(Integer openId);

    UserJwtIdProjection findUserJwtIdByEmail(String email);

    UserJwtIdProjection findUserJwtIdByPhone(String phone);

    @EntityGraph(type = EntityGraph.EntityGraphType.FETCH, attributePaths = {"utmParameters"})
    Optional<User> findByJwtId(String jwtId);

    @EntityGraph(type = EntityGraph.EntityGraphType.FETCH, attributePaths = {"solutions.task.taskType"})
    Optional<User> findUserWithSolutionsByJwtId(String jwtId);

    @EntityGraph(type = EntityGraph.EntityGraphType.FETCH, attributePaths = {"solutions.task.taskType", "solutions.task.bestSolution.solution", "solutions.grade.tags.gradeTag"})
    Optional<User> findUserWithSolutionsWithGradesByJwtId(String jwtId);

    @EntityGraph(type = EntityGraph.EntityGraphType.FETCH, attributePaths = {"entrepreneurTasks"})
    Optional<User> findUserWithTasksByJwtId(String jwtId);

    @EntityGraph(type = EntityGraph.EntityGraphType.FETCH, attributePaths = {
            "studentCourses.course.image",
            "studentCourses.lastOpenStage.lecture",
            "studentCourses.lastOpenStage.test",
            "studentCourses.lastOpenStage.intermediatePage",
            "studentCourses.lastOpenStage.congratulationPage",
    })
    Optional<User> findUserWithStudentCoursesByJwtId(String jwtId);

    Optional<User> findByUuid(UUID uuid);

    Optional<UserUuidProjection> findUserUuidByEmail(String email);

    UserEmailUuidProjection findUserEmailUuidByJwtId(String jwtId);

    UserEmailPhoneProjection findUserEmailPhoneByJwtId(String jwtId);

    boolean existsByJwtId(String jwtId);

    boolean existsByVkId(Integer vkId);

    boolean existsByEmail(String email);

    boolean existsByPhone(String phone);

    UserJwtIdProjection findUserJwtIdByVkId(Integer vkId);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("update User user set user.lastActionDate = ?1 where user.jwtId = ?2")
    void setLastActionDateForUser(LocalDateTime lastActionDate, String username);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("update User user set user.lastChangingProfileDate = ?1 where user.jwtId = ?2 or user.email = ?2 or user.phone =?2")
    void setLastChangingProfileDateForUser(LocalDateTime lastChangingProfileDate, String username);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("update User user set user.vkId = ?1, user.pointBalance = user.pointBalance + ?2 where user.jwtId = ?3")
    void addVkIdAndBonusForUser(Integer vkId, Integer bonusPoints, String jwtId);
}