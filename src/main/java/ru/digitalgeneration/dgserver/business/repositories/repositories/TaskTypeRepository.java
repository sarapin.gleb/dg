package ru.digitalgeneration.dgserver.business.repositories.repositories;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import ru.digitalgeneration.dgserver.business.entities.TaskType;
import ru.digitalgeneration.dgserver.business.exceptions.not_found_exceptions.TaskTypeNotFoundException;
import ru.digitalgeneration.dgserver.business.projection.TaskTypeProjection;
import ru.digitalgeneration.dgserver.business.repositories.jpa_repositories.TaskTypeJpaRepository;

import java.util.List;

@Repository
@RequiredArgsConstructor
public class TaskTypeRepository {

    private final TaskTypeJpaRepository taskTypeJpaRepository;

    public List<TaskType> getTaskTypeList() {
        return taskTypeJpaRepository.findAllByOrderById();
    }

    public TaskType getTaskTypeById(Long taskTypeId) {
        return taskTypeJpaRepository.findById(taskTypeId).orElseThrow(TaskTypeNotFoundException::new);
    }

    public TaskType getTaskTypeWithTestAndTaskValueAndStudentsById(Long taskTypeId) {
        return taskTypeJpaRepository.findTaskTypeWithTestAndTaskValueAndStudentsById(taskTypeId).orElseThrow(TaskTypeNotFoundException::new);
    }

    public TaskTypeProjection getTaskTypeProjectionById(Long taskTypeId) {
        return taskTypeJpaRepository.findTaskTypeProjectionById(taskTypeId).orElseThrow(TaskTypeNotFoundException::new);
    }
}