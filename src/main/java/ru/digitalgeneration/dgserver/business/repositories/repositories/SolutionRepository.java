package ru.digitalgeneration.dgserver.business.repositories.repositories;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import ru.digitalgeneration.dgserver.business.entities.Solution;
import ru.digitalgeneration.dgserver.business.exceptions.not_found_exceptions.SolutionNotFoundException;
import ru.digitalgeneration.dgserver.business.repositories.jpa_repositories.SolutionJpaRepository;

import java.util.List;

@Repository
@RequiredArgsConstructor
public class SolutionRepository {

    private final SolutionJpaRepository solutionJpaRepository;

    public void saveSolution(Solution solution) {
        solutionJpaRepository.saveAndFlush(solution);
    }

    public Solution getSolutionById(Long solutionId) {
        return solutionJpaRepository.findById(solutionId).orElseThrow(SolutionNotFoundException::new);
    }

    public Solution getSolutionByStudentJwtIdAndTaskId(String jwtId, Long taskId) {
        return solutionJpaRepository.findByStudentJwtIdAndTaskId(jwtId, taskId).orElseThrow(SolutionNotFoundException::new);
    }

    public Solution getSolutionByStudentJwtIdAndTaskIdAndDraftIsTrue(String jwtId, Long taskId) {
        return solutionJpaRepository.findByStudentJwtIdAndTaskIdAndDraftIsTrue(jwtId, taskId).orElseThrow(SolutionNotFoundException::new);
    }

    public boolean existsByStudentJwtIdAndTaskId(String jwtId, Long taskId) {
        return solutionJpaRepository.existsByStudentJwtIdAndTaskId(jwtId, taskId);
    }

    public List<Solution> getSolutionsByStudentJwtIdAndDraftIsFalse(String jwtId) {
        return solutionJpaRepository.findAllByStudentJwtIdAndDraftIsFalse(jwtId);
    }

    public List<Solution> getSolutionsByStudentJwtIdAndDraftIsTrue(String jwtId) {
        return solutionJpaRepository.findAllByStudentJwtIdAndDraftIsTrue(jwtId);
    }

    public boolean isTaskCreatedByEntrepreneur(String jwtId, Long solutionId) {
        return solutionJpaRepository.existsByTaskEntrepreneurJwtIdAndId(jwtId, solutionId);
    }

    public List<Solution> getSolutionsByTaskIdList(List<Long> taskIdList) {
        return solutionJpaRepository.findAllByTaskIdIn(taskIdList);
    }
}