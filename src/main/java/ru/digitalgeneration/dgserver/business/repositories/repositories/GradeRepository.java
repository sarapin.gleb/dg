package ru.digitalgeneration.dgserver.business.repositories.repositories;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import ru.digitalgeneration.dgserver.business.entities.Grade;
import ru.digitalgeneration.dgserver.business.repositories.jpa_repositories.GradeJpaRepository;

@Repository
@RequiredArgsConstructor
public class GradeRepository {

    private final GradeJpaRepository repository;

    public void saveGrade(Grade grade) {
        repository.saveAndFlush(grade);
    }
}