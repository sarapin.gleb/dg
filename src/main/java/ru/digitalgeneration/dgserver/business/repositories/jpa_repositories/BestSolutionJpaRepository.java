package ru.digitalgeneration.dgserver.business.repositories.jpa_repositories;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.digitalgeneration.dgserver.business.entities.BestSolution;

import java.util.List;

@Repository
public interface BestSolutionJpaRepository extends JpaRepository<BestSolution, Long> {

    @EntityGraph(type = EntityGraph.EntityGraphType.FETCH, attributePaths = {"solution.task.taskType", "solution.grade.tags.gradeTag"})
    List<BestSolution> findAllByStudentJwtIdOrderBySolutionDateDesc(String jwtId);

    @EntityGraph(type = EntityGraph.EntityGraphType.FETCH, attributePaths = {"solution.task.taskType", "solution.grade.tags.gradeTag"})
    List<BestSolution> findAllByStudentOpenIdOrderBySolutionDateDesc(Integer openId);
}