package ru.digitalgeneration.dgserver.business.repositories.repositories;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import ru.digitalgeneration.dgserver.business.entities.SolutionComplaintTag;
import ru.digitalgeneration.dgserver.business.entities.Tag;
import ru.digitalgeneration.dgserver.business.entities.TaskComplaintTag;
import ru.digitalgeneration.dgserver.business.repositories.jpa_repositories.CourseTagJpaRepository;
import ru.digitalgeneration.dgserver.business.repositories.jpa_repositories.SolutionComplaintTagJpaRepository;
import ru.digitalgeneration.dgserver.business.repositories.jpa_repositories.TaskComplaintTagJpaRepository;

import java.util.List;

@Repository
@RequiredArgsConstructor
public class TagRepository {

    private final CourseTagJpaRepository courseTagRepository;
    private final TaskComplaintTagJpaRepository taskComplaintTagRepository;
    private final SolutionComplaintTagJpaRepository solutionComplaintTagRepository;

    public List<Tag> getTagList() {
        return courseTagRepository.findAllByOrderByValue();
    }

    public List<TaskComplaintTag> getTaskComplaintTags() {
        return taskComplaintTagRepository.findAllByOrderById();
    }

    public List<SolutionComplaintTag> getSolutionComplaintTags() {
        return solutionComplaintTagRepository.findAllByOrderById();
    }
}