package ru.digitalgeneration.dgserver.business.repositories.jpa_repositories;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.digitalgeneration.dgserver.business.entities.StudentCourse;
import ru.digitalgeneration.dgserver.business.projection.CourseWithStagesProjection;
import ru.digitalgeneration.dgserver.business.projection.StudentCourseDataProjection;

import java.util.List;
import java.util.Optional;

@Repository
public interface StudentCourseJpaRepository extends JpaRepository<StudentCourse, Long> {

    boolean existsStudentCourseByCourseIdAndUserJwtId(Long courseId, String jwtId);

    Optional<StudentCourse> findByCourseIdAndUserJwtId(Long courseId, String jwtId);

    CourseWithStagesProjection findCourseWithStagesProjectionByCourseIdAndUserJwtId(Long courseId, String jwtId);

    StudentCourseDataProjection findStudentCourseDataProjectionByCourseIdAndUserJwtId(Long courseId, String jwtId);

    @EntityGraph(type = EntityGraph.EntityGraphType.FETCH, attributePaths = {"course"})
    List<StudentCourse> findAllByUserJwtId(String jwtId);
}