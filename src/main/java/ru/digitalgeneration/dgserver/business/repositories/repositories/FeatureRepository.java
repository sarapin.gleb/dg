package ru.digitalgeneration.dgserver.business.repositories.repositories;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import ru.digitalgeneration.dgserver.business.entities.Feature;
import ru.digitalgeneration.dgserver.business.repositories.jpa_repositories.FeatureJpaRepository;

import java.util.List;

@Repository
@RequiredArgsConstructor
public class FeatureRepository {

    private final FeatureJpaRepository repository;

    public List<Feature> getFeatureListOrderedByReleaseDate() {
        return repository.findAllByOrderByReleaseDate();
    }
}