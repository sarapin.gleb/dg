package ru.digitalgeneration.dgserver.business.repositories.jpa_repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.digitalgeneration.dgserver.business.entities.TaskComplaintTag;

import java.util.List;

@Repository
public interface TaskComplaintTagJpaRepository extends JpaRepository<TaskComplaintTag, Long> {

    List<TaskComplaintTag> findAllByOrderById();

    List<TaskComplaintTag> findAllByIdIn(List<Long> tagIdList);
}