package ru.digitalgeneration.dgserver.business.repositories.repositories;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import ru.digitalgeneration.dgserver.business.entities.UtmParameters;
import ru.digitalgeneration.dgserver.business.repositories.jpa_repositories.UtmParametersJpaRepository;

@Repository
@RequiredArgsConstructor
public class UtmParametersRepository {

    private final UtmParametersJpaRepository repository;

    public void saveUtmParameters(UtmParameters utmParameters) {
        repository.saveAndFlush(utmParameters);
    }
}