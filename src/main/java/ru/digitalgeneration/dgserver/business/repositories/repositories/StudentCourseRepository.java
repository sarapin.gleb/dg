package ru.digitalgeneration.dgserver.business.repositories.repositories;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import ru.digitalgeneration.dgserver.business.entities.StudentCourse;
import ru.digitalgeneration.dgserver.business.exceptions.not_found_exceptions.StudentCourseNotFoundException;
import ru.digitalgeneration.dgserver.business.projection.CourseWithStagesProjection;
import ru.digitalgeneration.dgserver.business.repositories.jpa_repositories.StudentCourseJpaRepository;

import java.util.List;

@Repository
@RequiredArgsConstructor
public class StudentCourseRepository {

    private final StudentCourseJpaRepository repository;

    public void saveStudentCourse(StudentCourse studentCourse) {
        repository.saveAndFlush(studentCourse);
    }

    public StudentCourse getStudentCourseByCourseIdAndUserJwtId(Long courseId, String jwtId) {
        return repository.findByCourseIdAndUserJwtId(courseId, jwtId).orElseThrow(StudentCourseNotFoundException::new);
    }

    public CourseWithStagesProjection getCourseWithStagesByCourseIdAndJwtId(Long courseId, String jwtId) {
        return repository.findCourseWithStagesProjectionByCourseIdAndUserJwtId(courseId, jwtId);
    }

    public List<StudentCourse> getStudentCoursesByJwtId(String jwtId) {
        return repository.findAllByUserJwtId(jwtId);
    }
}