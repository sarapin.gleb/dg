package ru.digitalgeneration.dgserver.business.repositories.jpa_repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.digitalgeneration.dgserver.business.entities.TaskTypeGradeTag;

import java.util.List;

@Repository
public interface TaskTypeGradeTagJpaRepository extends JpaRepository<TaskTypeGradeTag, Long> {

    List<TaskTypeGradeTag> findAllByIdIn(List<Long> taskTypeGradeTags);
}