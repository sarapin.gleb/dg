package ru.digitalgeneration.dgserver.business.repositories.jpa_repositories;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.digitalgeneration.dgserver.business.entities.Task;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface TaskJpaRepository extends JpaRepository<Task, Long> {

    @EntityGraph(type = EntityGraph.EntityGraphType.FETCH, attributePaths = {"solutions", "taskType"})
    List<Task> findAllByEntrepreneurJwtIdOrderByCreationDateDesc(String jwtId);

    @EntityGraph(type = EntityGraph.EntityGraphType.FETCH, attributePaths = {"solutions"})
    Optional<Task> findByIdAndEntrepreneurJwtId(Long taskId, String jwtId);

    List<Task> findAllByExecutionStatusAndDeadlineBefore(String executionStatus, LocalDateTime now);

    List<Task> findAllByExecutionStatus(String executionStatus);

    @EntityGraph(type = EntityGraph.EntityGraphType.FETCH, attributePaths = {"files"})
    List<Task> findAllByTaskTypeId(Long taskTypeId);
}