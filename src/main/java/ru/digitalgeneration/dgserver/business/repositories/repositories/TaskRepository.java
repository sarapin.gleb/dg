package ru.digitalgeneration.dgserver.business.repositories.repositories;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import ru.digitalgeneration.dgserver.business.entities.Task;
import ru.digitalgeneration.dgserver.business.entities.sql_mapping_entities.CurrentTaskEntity;
import ru.digitalgeneration.dgserver.business.exceptions.not_found_exceptions.TaskNotFoundException;
import ru.digitalgeneration.dgserver.business.repositories.jpa_repositories.TaskJpaRepository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
@RequiredArgsConstructor
public class TaskRepository {

    private final TaskJpaRepository taskJpaRepository;
    @PersistenceContext
    private final EntityManager entityManager;

    public void saveTask(Task task) {
        taskJpaRepository.saveAndFlush(task);
    }

    public Task getTaskById(Long taskId) {
        return taskJpaRepository.findById(taskId).orElseThrow(TaskNotFoundException::new);
    }

    public List<Task> getTasksByEntrepreneurJwtId(String jwtId) {
        return taskJpaRepository.findAllByEntrepreneurJwtIdOrderByCreationDateDesc(jwtId);
    }

    public Task getTaskByIdAndEntrepreneurJwtId(Long taskId, String jwtId) {
        return taskJpaRepository.findByIdAndEntrepreneurJwtId(taskId, jwtId).orElseThrow(TaskNotFoundException::new);
    }

    public CurrentTaskEntity getCurrentTask(String jwtId) {
        Query query = entityManager.createNativeQuery(
                "SELECT " +
                        "receipt_date, " +
                        "solution_time, " +
                        "task_id FROM solutions s " +
                        "JOIN users u on s.student_id = u.id " +
                        "JOIN tasks t on s.task_id = t.id " +
                        "JOIN task_types tt on t.task_type_id = tt.id " +
                        "WHERE u.jwt_id = (:jwtId) AND receipt_date + solution_time >= now() AND s.draft = true", "CurrentTaskMapping"
        );
        query.setParameter("jwtId", jwtId);
        return (CurrentTaskEntity) query.getSingleResult();
    }

    public List<Task> getTasksByTaskTypeId(Long taskTypeId) {
        return taskJpaRepository.findAllByTaskTypeId(taskTypeId);
    }
}