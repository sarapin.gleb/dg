package ru.digitalgeneration.dgserver.business.repositories.jpa_repositories;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.digitalgeneration.dgserver.business.entities.TaskType;
import ru.digitalgeneration.dgserver.business.projection.TaskTypeProjection;

import java.util.List;
import java.util.Optional;

@Repository
public interface TaskTypeJpaRepository extends JpaRepository<TaskType, Long> {

    @EntityGraph(type = EntityGraph.EntityGraphType.FETCH, attributePaths = {"image", "taskValue", "students"})
    List<TaskType> findAllByOrderById();

    @EntityGraph(type = EntityGraph.EntityGraphType.FETCH, attributePaths = {"test", "taskValue", "students"})
    Optional<TaskType> findTaskTypeWithTestAndTaskValueAndStudentsById(Long id);

    Optional<TaskTypeProjection> findTaskTypeProjectionById(Long taskTypeId);
}