package ru.digitalgeneration.dgserver.business.repositories.repositories;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import ru.digitalgeneration.dgserver.business.entities.SolutionComplaintTag;
import ru.digitalgeneration.dgserver.business.repositories.jpa_repositories.SolutionComplaintTagJpaRepository;

import java.util.List;

@Repository
@RequiredArgsConstructor
public class SolutionComplaintTagRepository {

    private final SolutionComplaintTagJpaRepository repository;

    public List<SolutionComplaintTag> getSolutionComplaintTagsByIdList(List<Long> tagIdList) {
        return repository.findAllByIdIn(tagIdList);
    }
}