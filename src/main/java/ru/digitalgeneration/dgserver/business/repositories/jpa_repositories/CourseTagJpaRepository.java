package ru.digitalgeneration.dgserver.business.repositories.jpa_repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.digitalgeneration.dgserver.business.entities.Tag;

import java.util.List;

@Repository
public interface CourseTagJpaRepository extends JpaRepository<Tag, Long> {

    List<Tag> findAllByOrderByValue();
}