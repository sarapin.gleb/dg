package ru.digitalgeneration.dgserver.business.repositories.repositories;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import ru.digitalgeneration.dgserver.business.entities.TaskComplaintTag;
import ru.digitalgeneration.dgserver.business.repositories.jpa_repositories.TaskComplaintTagJpaRepository;

import java.util.List;

@Repository
@RequiredArgsConstructor
public class TaskComplaintTagRepository {

    private final TaskComplaintTagJpaRepository repository;

    public List<TaskComplaintTag> getTaskComplaintTagsByIdList(List<Long> tagIdList) {
        return repository.findAllByIdIn(tagIdList);
    }
}