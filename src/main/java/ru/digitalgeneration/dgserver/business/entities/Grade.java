package ru.digitalgeneration.dgserver.business.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "grades")
@Entity
public class Grade extends BaseEntity {
    private Integer score;
    private String feedback;
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "grades_task_types_grade_tags",
            joinColumns = @JoinColumn(name = "grade_id"),
            inverseJoinColumns = @JoinColumn(name = "task_types_grade_tag_id")
    )
    private Set<TaskTypeGradeTag> tags = new HashSet<>();
}