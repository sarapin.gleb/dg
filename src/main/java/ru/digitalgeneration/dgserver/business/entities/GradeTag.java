package ru.digitalgeneration.dgserver.business.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "grade_tags")
@Entity
public class GradeTag extends BaseEntity {
    private String value;
    @OneToMany(mappedBy = "gradeTag", cascade = CascadeType.ALL)
    private List<TaskTypeGradeTag> taskTypeGradeTagList = new ArrayList<>();
}