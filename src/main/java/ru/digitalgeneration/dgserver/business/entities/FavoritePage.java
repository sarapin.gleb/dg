package ru.digitalgeneration.dgserver.business.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "favorite_pages")
@Entity
public class FavoritePage extends BaseEntity {
    private String email;
    private String pageUrl;
}