package ru.digitalgeneration.dgserver.business.entities;

import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import ru.digitalgeneration.dgserver.business.entities.enums.TaskExecutionStatus;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "tasks")
@Entity
@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
public class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "entrepreneur_id")
    private User entrepreneur;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "task_type_id")
    private TaskType taskType;
    @Type(type = "jsonb")
    private Object formulation;
    private String executionStatus;
    @Column(columnDefinition = "TIMESTAMP")
    private LocalDateTime creationDate;
    @Column(columnDefinition = "TIMESTAMP")
    private LocalDateTime deadline;
    private Double priority;
    private Integer decisionsRequiredNumber;
    private boolean draft;
    @OneToOne(cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinColumn(name = "best_solution_id")
    private BestSolution bestSolution;
    private boolean specialTask;
    @OneToMany(mappedBy = "task", cascade = CascadeType.ALL)
    private List<TaskComplaint> taskComplaints = new ArrayList<>();
    @OneToMany(mappedBy = "task", cascade = CascadeType.ALL)
    private List<Solution> solutions = new ArrayList<>();
    @OneToMany(mappedBy = "task", cascade = CascadeType.ALL)
    private List<File> files = new ArrayList<>();

    public Task(User entrepreneur, TaskType taskType, Object formulation, Integer decisionsRequiredNumber, boolean specialTask) {

        final LocalDateTime now = LocalDateTime.now();

        this.entrepreneur = entrepreneur;
        this.taskType = taskType;
        this.formulation = formulation;
        this.executionStatus = TaskExecutionStatus.IN_PROGRESS.toString();
        this.creationDate = now;
        this.deadline = now.plusWeeks(3);
        this.priority = (double) (ChronoUnit.DAYS.between(this.creationDate, this.deadline) / decisionsRequiredNumber);
        this.decisionsRequiredNumber = decisionsRequiredNumber;
        this.draft = false;
        this.specialTask = specialTask;
    }
}