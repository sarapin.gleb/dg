package ru.digitalgeneration.dgserver.business.entities.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public enum PointPrice {

    POINT_PRICE(1);

    private final Integer value;
}