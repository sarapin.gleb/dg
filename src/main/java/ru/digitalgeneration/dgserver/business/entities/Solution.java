package ru.digitalgeneration.dgserver.business.entities;

import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "solutions")
@Entity
@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
public class Solution {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "student_id")
    private User student;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "task_id")
    private Task task;
    @Type(type = "jsonb")
    private Object value;
    @Column(columnDefinition = "TIMESTAMP")
    private LocalDateTime receiptDate;
    @Column(columnDefinition = "TIMESTAMP")
    private LocalDateTime deliveryDate;
    private boolean draft;
    @OneToOne(cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinColumn(name = "grade_id")
    private Grade grade;
    @OneToOne(cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinColumn(name = "complaint_id")
    private SolutionComplaint solutionComplaint;
    @OneToMany(mappedBy = "solution", cascade = CascadeType.ALL)
    private List<File> files = new ArrayList<>();

    public Solution(User student, Task task) {
        this.student = student;
        this.task = task;
        this.value = task.getTaskType().getSolution();
        this.receiptDate = LocalDateTime.now();
        this.draft = true;
    }
}