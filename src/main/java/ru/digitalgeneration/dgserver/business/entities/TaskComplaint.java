package ru.digitalgeneration.dgserver.business.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "task_complaints")
@Entity
public class TaskComplaint extends BaseEntity {
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "student_id")
    private User student;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "task_id")
    private Task task;
    private String value;
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "task_complaints_task_complaint_tags",
            joinColumns = @JoinColumn(name = "task_complaint_id"),
            inverseJoinColumns = @JoinColumn(name = "task_complaint_tag_id")
    )
    private Set<TaskComplaintTag> tags = new HashSet<>();
}