package ru.digitalgeneration.dgserver.business.entities;

import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.*;
import ru.digitalgeneration.dgserver.business.entities.enums.LoginType;
import ru.digitalgeneration.dgserver.business.entities.enums.Role;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.*;

@Getter
@Setter
@NoArgsConstructor
@Table(name = "users")
@Entity
@TypeDefs({
        @TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
})
public class User extends BaseEntity {
    @Column(columnDefinition = "TIMESTAMP")
    @CreationTimestamp
    private LocalDateTime registrationDate;
    private String fName;
    private String sName;
    private String mName;
    private String email;
    private Boolean emailVerified;
    private boolean unsubscribed;
    private String phone;
    private Boolean phoneVerified;
    private boolean phoneFirstConfirmation;
    private String password;
    private Integer vkId;
    private String jwtId;
    @GeneratedValue
    @Column(insertable = false, updatable = false, columnDefinition = "serial")
    private Integer openId;
    @Column(columnDefinition = "TIMESTAMP")
    private LocalDateTime lastChangingProfileDate;
    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    private Set<String> roles = new HashSet<>();
    @Column(columnDefinition = "TIMESTAMP")
    private LocalDateTime lastPointsPurchaseDate;
    private Integer pointBalance;
    private boolean permanentlyBlocked;
    @Column(columnDefinition = "TIMESTAMP")
    private LocalDateTime unlockDate;
    private String region;
    private UUID uuid;
    @Column(columnDefinition = "TIMESTAMP")
    private LocalDateTime lastLoginDate;
    @Column(columnDefinition = "TIMESTAMP")
    private LocalDateTime lastActionDate;
    private String lastNonceVerificationCode;
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private List<StudentCourse> studentCourses = new ArrayList<>();
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private List<Transaction> transactions = new ArrayList<>();
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "students_task_types",
            joinColumns = @JoinColumn(name = "student_id"),
            inverseJoinColumns = @JoinColumn(name = "task_type_id")
    )
    private Set<TaskType> taskTypes = new HashSet<>();
    @OneToMany(mappedBy = "entrepreneur", cascade = CascadeType.MERGE)
    private List<Task> entrepreneurTasks = new ArrayList<>();
    @OneToMany(mappedBy = "student", cascade = CascadeType.ALL)
    private List<TaskComplaint> taskComplaints = new ArrayList<>();
    @OneToMany(mappedBy = "student", cascade = CascadeType.MERGE)
    private List<Solution> solutions = new ArrayList<>();
    @OneToMany(mappedBy = "student", cascade = CascadeType.MERGE)
    private List<BestSolution> bestSolutions = new ArrayList<>();
    @OneToOne(cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinColumn(name = "utm_parameters_id")
    private UtmParameters utmParameters;
    private boolean admin;

    public User(LoginType loginType, String login, String hashedPassword, String role, UUID uuid, String jwtId, UtmParameters utmParameters) {
        String email = null;
        String phone = null;
        if (loginType.equals(LoginType.EMAIL)) {
            email = login;
        }
        if (loginType.equals(LoginType.PHONE)) {
            phone = login;
        }

        final LocalDateTime now = LocalDateTime.now();

        this.fName = null;
        this.sName = null;
        this.mName = null;
        this.email = email;
        this.emailVerified = false;
        this.phone = phone;
        this.phoneVerified = false;
        this.password = hashedPassword;
        this.vkId = null;
        this.jwtId = jwtId;
        this.lastChangingProfileDate = now;
        this.roles = Set.of(Role.valueOf(role).toString());
        this.lastPointsPurchaseDate = null;
        this.pointBalance = 0;
        this.permanentlyBlocked = false;
        this.unlockDate = null;
        this.region = null;
        this.uuid = uuid;
        this.lastLoginDate = now;
        this.lastActionDate = now;
        this.lastNonceVerificationCode = null;
        this.utmParameters = utmParameters;
        this.admin = false;
    }

    public User(String fName, String sName, String email, Integer vkId, String role, Integer bonusPoints, String jwtId, UtmParameters utmParameters) {
        final LocalDateTime now = LocalDateTime.now();

        this.fName = fName;
        this.sName = sName;
        this.mName = null;
        this.email = email;
        this.emailVerified = false;
        this.phone = null;
        this.phoneVerified = false;
        this.password = null;
        this.vkId = vkId;
        this.jwtId = jwtId;
        this.lastChangingProfileDate = now;
        this.roles = Set.of(Role.valueOf(role).toString());
        this.lastPointsPurchaseDate = null;
        this.pointBalance = bonusPoints;
        this.permanentlyBlocked = false;
        this.unlockDate = null;
        this.region = null;
        this.uuid = UUID.randomUUID();
        this.lastLoginDate = now;
        this.lastActionDate = now;
        this.lastNonceVerificationCode = null;
        this.utmParameters = utmParameters;
        this.admin = false;
    }

    public boolean isNonLocked() {
        return true;
    }
}