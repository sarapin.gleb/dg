package ru.digitalgeneration.dgserver.business.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "task_types_grade_tags")
@Entity
public class TaskTypeGradeTag {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "task_type_id")
    private TaskType taskType;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "grade_tag_id")
    private GradeTag gradeTag;
    private String comment;
    @ManyToMany(mappedBy = "tags", fetch = FetchType.LAZY)
    private List<Grade> grades = new ArrayList<>();
}