package ru.digitalgeneration.dgserver.business.entities.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public enum Role {

    STUDENT("Студент"),
    ENTREPRENEUR("Предприниматель");

    private final String value;
}