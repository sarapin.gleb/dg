package ru.digitalgeneration.dgserver.business.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "files")
@Entity
public class File {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String filePath;
    private String fileName;
    private Integer orderNumber;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "task_id")
    private Task task;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "solution_id")
    private Solution solution;

    public File(String filePath, String fileName, Integer orderNumber, Task task) {
        this.filePath = filePath;
        this.fileName = fileName;
        this.orderNumber = orderNumber;
        this.task = task;
    }

    public File(String filePath, String fileName, Integer orderNumber, Solution solution) {
        this.filePath = filePath;
        this.fileName = fileName;
        this.orderNumber = orderNumber;
        this.solution = solution;
    }
}