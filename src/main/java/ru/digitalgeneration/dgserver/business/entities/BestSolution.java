package ru.digitalgeneration.dgserver.business.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@Table(name = "best_solutions")
@Entity
public class BestSolution extends BaseEntity {
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "student_id")
    private User student;
    @OneToOne(cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinColumn(name = "solution_id")
    private Solution solution;
    @Column(columnDefinition = "TIMESTAMP")
    @CreationTimestamp
    private LocalDateTime solutionDate;

    public BestSolution(User student, Solution solution) {
        this.student = student;
        this.solution = solution;
    }
}