package ru.digitalgeneration.dgserver.business.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@Table(name = "transactions")
@Entity
public class Transaction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String tinkoffPaymentStatus;
    private Integer amount;
    private String errorCode;
    private Integer points;
    @Column(columnDefinition = "TIMESTAMP")
    @CreationTimestamp
    private LocalDateTime creationDate;
    @Column(columnDefinition = "TIMESTAMP")
    @UpdateTimestamp
    private LocalDateTime lastModifiedDate;
    private String paymentId;
    private String paymentStatus;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    public Transaction(Integer amount, Integer points, User user) {
        this.amount = amount;
        this.points = points;
        this.user = user;
    }
}