package ru.digitalgeneration.dgserver.business.entities;

import com.vladmihalcea.hibernate.type.interval.PostgreSQLIntervalType;
import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "task_types")
@Entity
@TypeDefs({
        @TypeDef(name = "jsonb", typeClass = JsonBinaryType.class),
        @TypeDef(typeClass = PostgreSQLIntervalType.class, defaultForType = Duration.class)
})
public class TaskType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "test_id")
    private Test test;
    @OneToOne(cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinColumn(name = "task_value_id")
    private TaskValue taskValue;
    @OneToOne(cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinColumn(name = "image_id")
    private Image image;
    private String title;
    private String briefDescription;
    @Type(type = "jsonb")
    private Object formulation;
    @Type(type = "jsonb")
    private Object solution;
    private Integer reward;
    private Integer price;
    private Duration solutionTime;
    @ManyToMany(mappedBy = "taskTypes", fetch = FetchType.LAZY)
    private List<User> students = new ArrayList<>();
    @OneToMany(mappedBy = "taskType", cascade = CascadeType.ALL)
    private List<TaskTypeGradeTag> taskTypeGradeTagList = new ArrayList<>();
    @OneToMany(mappedBy = "taskType", cascade = CascadeType.MERGE)
    private List<Task> tasks = new ArrayList<>();
}