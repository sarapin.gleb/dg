package ru.digitalgeneration.dgserver.business.entities.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public enum GradeTag {

    ANALYST("Аналитик"),
    TELEPATH("Телепат"),
    ENTREPRENEUR("Предприниматель"),
    INSIDER("Инсайдер");

    private final String value;
}