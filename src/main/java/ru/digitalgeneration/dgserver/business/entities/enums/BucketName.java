package ru.digitalgeneration.dgserver.business.entities.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public enum BucketName {
    DG_BUCKET("digitalgeneration");

    private final String bucketName;
}