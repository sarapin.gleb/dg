package ru.digitalgeneration.dgserver.business.entities.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public enum TaskExecutionStatus {

    COMPLETED("Completed"),
    GRADED("Completed"),
    IN_PROGRESS("In progress"),
    EVALUATED("Evaluated"),
    NOT_DONE("Not done"),
    CANCELED("Canceled");

    private final String value;
}