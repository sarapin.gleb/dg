package ru.digitalgeneration.dgserver.business.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Table(name = "tests")
@Entity
public class Test extends BaseEntity {
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "stage_id")
    private Stage stage;
    private String questions;
    @OneToMany(mappedBy = "test", cascade = CascadeType.MERGE)
    private List<TaskType> taskTypes = new ArrayList<>();
}