package ru.digitalgeneration.dgserver.business.entities.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public enum PaymentStatus {

    PAID("Paid"),
    IN_PROCESS("In the process of payment"),
    CANCELED("Canceled");

    private final String value;
}