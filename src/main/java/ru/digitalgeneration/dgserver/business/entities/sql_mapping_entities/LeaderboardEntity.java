package ru.digitalgeneration.dgserver.business.entities.sql_mapping_entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.digitalgeneration.dgserver.business.entities.BaseEntity;

import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.SqlResultSetMapping;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SqlResultSetMapping(name = "LeaderboardMapping", classes = {
        @ConstructorResult(targetClass = LeaderboardEntity.class,
                columns = {
                        @ColumnResult(name = "open_id", type = Integer.class),
                        @ColumnResult(name = "f_name"),
                        @ColumnResult(name = "s_name"),
                        @ColumnResult(name = "m_name"),
                        @ColumnResult(name = "best_solutions_count", type = int.class),
                        @ColumnResult(name = "position", type = int.class)
                })
})
public class LeaderboardEntity extends BaseEntity {
    private Integer openId;
    private String fName;
    private String sName;
    private String mName;
    private int bestSolutionsCount;
    private int position;

}