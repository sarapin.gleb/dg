package ru.digitalgeneration.dgserver.business.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@Table(name = "features")
@Entity
public class Feature extends BaseEntity {
    @Column(columnDefinition = "TIMESTAMP")
    private LocalDateTime releaseDate;
    private String title;
    private String description;
}