package ru.digitalgeneration.dgserver.business.entities.enums;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public enum LoginType {

    EMAIL("email"),
    PHONE("phone");

    @JsonValue
    private final String value;
}