package ru.digitalgeneration.dgserver.business.entities.sql_mapping_entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.digitalgeneration.dgserver.business.entities.BaseEntity;

import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.SqlResultSetMapping;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SqlResultSetMapping(name = "UserGradeTagMapping", classes = {
        @ConstructorResult(targetClass = UserGradeTagEntity.class,
                columns = {
                        @ColumnResult(name = "tag"),
                        @ColumnResult(name = "count", type = Integer.class)
                })
})
public class UserGradeTagEntity extends BaseEntity {
    private String tag;
    private Integer count;
}