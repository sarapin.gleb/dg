package ru.digitalgeneration.dgserver.business.entities.sql_mapping_entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SqlResultSetMapping(name = "StageResponseMapping", classes = {
        @ConstructorResult(targetClass = StageResponseEntity.class,
                columns = {
                        @ColumnResult(name = "stages_id", type = Long.class),
                        @ColumnResult(name = "stages_title"),
                        @ColumnResult(name = "description"),
                        @ColumnResult(name = "course_id", type = Long.class),
                        @ColumnResult(name = "course_title"),
                        @ColumnResult(name = "stage_type"),
                        @ColumnResult(name = "video"),
                        @ColumnResult(name = "questions"),
                        @ColumnResult(name = "task_type_id", type = Long.class),
                        @ColumnResult(name = "last_open_stage", type = Long.class),
                        @ColumnResult(name = "last_viewed_stage", type = Long.class),
                        @ColumnResult(name = "timing", type = Integer.class),
                        @ColumnResult(name = "prev_stage_id", type = Long.class),
                        @ColumnResult(name = "prev_stage_title"),
                        @ColumnResult(name = "next_stage_id", type = Long.class),
                        @ColumnResult(name = "next_stage_title")
                })
})
public class StageResponseEntity {
    @Id
    private Long id;
    private String title;
    private String description;
    private Long courseId;
    private String courseTitle;
    private String stageType;
    private String video;
    private String questions;
    private Long taskTypeId;
    private Long lastOpenStage;
    private Long lastViewedStage;
    private Integer timing;
    private Long prevStageId;
    private String prevStageTitle;
    private Long nextStageId;
    private String nextStageTitle;
}