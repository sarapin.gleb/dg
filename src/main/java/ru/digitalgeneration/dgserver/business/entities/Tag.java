package ru.digitalgeneration.dgserver.business.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Table(name = "tags")
@Entity
public class Tag extends BaseEntity {
    private String value;
    @ManyToMany(mappedBy = "tags", fetch = FetchType.LAZY)
    private List<Course> courses = new ArrayList<>();
}