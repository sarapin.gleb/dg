package ru.digitalgeneration.dgserver.business.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "task_complaint_tags")
@Entity
public class TaskComplaintTag {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String value;
    @ManyToMany(mappedBy = "tags", fetch = FetchType.LAZY)
    private List<TaskComplaint> taskComplaints = new ArrayList<>();
}