package ru.digitalgeneration.dgserver.business.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "solution_complaints")
@Entity
public class SolutionComplaint extends BaseEntity {
    private String value;
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "solution_complaints_solution_complaint_tags",
            joinColumns = @JoinColumn(name = "solution_complaint_id"),
            inverseJoinColumns = @JoinColumn(name = "solution_complaint_tag_id")
    )
    private Set<SolutionComplaintTag> tags = new HashSet<>();
}