package ru.digitalgeneration.dgserver.business.entities.sql_mapping_entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.digitalgeneration.dgserver.business.entities.BaseEntity;

import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.SqlResultSetMapping;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SqlResultSetMapping(name = "CourseTagMapping", classes = {
        @ConstructorResult(targetClass = CourseTagEntity.class,
                columns = {
                        @ColumnResult(name = "c_id", type = Long.class),
                        @ColumnResult(name = "pn", type = Integer.class),
                        @ColumnResult(name = "purchase_status", type = boolean.class),
                        @ColumnResult(name = "title"),
                        @ColumnResult(name = "image_path"),
                        @ColumnResult(name = "image_file_name"),
                        @ColumnResult(name = "author"),
                        @ColumnResult(name = "tag_values"),
                        @ColumnResult(name = "price"),
                        @ColumnResult(name = "completeness_status", type = Boolean.class),
                        @ColumnResult(name = "last_action_date", type = LocalDateTime.class)
                })
})
public class CourseTagEntity extends BaseEntity {
    private Long courseId;
    private Integer purchasesNumber;
    private boolean purchaseStatus;
    private String title;
    private String imagePath;
    private String imageFileName;
    private String author;
    private String tags;
    private Integer price;
    private Boolean completenessStatus;
    private LocalDateTime lastActionDate;
}