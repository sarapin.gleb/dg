package ru.digitalgeneration.dgserver.business.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.time.Duration;

@Getter
@Setter
@NoArgsConstructor
@Table(name = "stages")
@Entity
public class Stage {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String title;
    private Integer orderNumber;
    private String description;
    private Duration duration;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "course_id")
    private Course course;
    private Long prevStageId;
    private Long nextStageId;
    @OneToOne(mappedBy = "stage", fetch = FetchType.LAZY)
    @Fetch(FetchMode.JOIN)
    private Lecture lecture;
    @OneToOne(mappedBy = "stage", fetch = FetchType.LAZY)
    @Fetch(FetchMode.JOIN)
    private Test test;
    @OneToOne(mappedBy = "stage", fetch = FetchType.LAZY)
    @Fetch(FetchMode.JOIN)
    private IntermediatePage intermediatePage;
    @OneToOne(mappedBy = "stage", fetch = FetchType.LAZY)
    @Fetch(FetchMode.JOIN)
    private CongratulationPage congratulationPage;
}