package ru.digitalgeneration.dgserver.business.entities.sql_mapping_entities;

import com.vladmihalcea.hibernate.type.interval.PostgreSQLIntervalType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.digitalgeneration.dgserver.business.entities.BaseEntity;

import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.SqlResultSetMapping;
import java.time.Duration;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SqlResultSetMapping(name = "CurrentTaskMapping", classes = {
        @ConstructorResult(targetClass = CurrentTaskEntity.class,
                columns = {
                        @ColumnResult(name = "receipt_date", type = LocalDateTime.class),
                        @ColumnResult(name = "solution_time", type = PostgreSQLIntervalType.class),
                        @ColumnResult(name = "task_id", type = Long.class)
                })
})
public class CurrentTaskEntity extends BaseEntity {
    LocalDateTime receiptDate;
    Duration solutionTime;
    Long taskId;
}