package ru.digitalgeneration.dgserver.business.projection;

import java.util.Optional;
import java.util.UUID;

public interface UserEmailUuidProjection {
    Optional<String> getEmail();

    UUID getUuid();
}