package ru.digitalgeneration.dgserver.business.projection;

public interface UserEmailPhoneProjection {

    String getEmail();

    String getPhone();
}