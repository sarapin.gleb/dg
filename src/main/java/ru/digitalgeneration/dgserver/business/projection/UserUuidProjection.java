package ru.digitalgeneration.dgserver.business.projection;

import java.util.UUID;

public interface UserUuidProjection {

    UUID getUuid();
}