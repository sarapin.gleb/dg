package ru.digitalgeneration.dgserver.business.projection;

public interface TaskTypeProjection {

    String getTitle();

    Object getFormulation();

    Integer getPrice();
}