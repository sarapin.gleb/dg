package ru.digitalgeneration.dgserver.business.projection;

import org.springframework.beans.factory.annotation.Value;

public interface StudentCourseDataProjection {

    boolean isCompletenessStatus();

    @Value("#{target.lastOpenStage.id}")
    Long getLastOpenStageId();
}