package ru.digitalgeneration.dgserver.business.projection;

import org.springframework.beans.factory.annotation.Value;
import ru.digitalgeneration.dgserver.business.entities.Stage;

import java.util.List;

public interface CourseWithStagesProjection {

    @Value("#{target.course.title}")
    String getCourseTitle();

    Stage getLastOpenStage();

    @Value("#{target.course.stages}")
    List<Stage> getStages();
}