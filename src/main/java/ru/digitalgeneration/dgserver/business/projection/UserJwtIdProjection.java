package ru.digitalgeneration.dgserver.business.projection;

public interface UserJwtIdProjection {
    String getJwtId();
}