package ru.digitalgeneration.dgserver.business.dto.responses;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Value;

import java.time.LocalDateTime;
import java.util.List;

@Value
@Schema(title = "Взятая задача ответ для студента")
public class TaskResponseForStudent {

    @Schema(title = "Идентификатор задачи")
    Long id;

    @Schema(title = "Название типа задач")
    String title;

    @Schema(title = "Статус выполнения")
    boolean isExecuted;

    @Schema(title = "Оценка")
    Integer score;

    @Schema(title = "Список тегов к оценке")
    List<TagResponse> gradeTags;

    @Schema(title = "Наличие отзыва")
    Boolean isFeedbackExist;

    @Schema(title = "Дата и время получения задачи")
    LocalDateTime receiptDate;

    @Schema(title = "Являляется ли решение лучшим")
    boolean isSolutionBest;
}