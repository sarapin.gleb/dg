package ru.digitalgeneration.dgserver.business.dto.requests;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Value;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Value
@Schema(title = "Создание жалобы на решение")
public class SolutionComplaintRequest {

    @NotNull
    @Schema(title = "Идентификатор решения", required = true, example = "1")
    Long solutionId;

    @NotBlank
    @Schema(title = "Текст жалобы", required = true, example = "Оскорбительные высказывания")
    String value;

    @NotNull
    @Schema(title = "Список ID тегов к жалобам на решение", required = true, example = "[1, 2]")
    List<Long> tagIdList;
}