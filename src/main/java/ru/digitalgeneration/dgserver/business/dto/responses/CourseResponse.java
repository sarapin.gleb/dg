package ru.digitalgeneration.dgserver.business.dto.responses;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Value;
import ru.digitalgeneration.dgserver.business.dto.StageDto;

import java.util.List;

@Value
@Schema(title = "Информация о курсе")
public class CourseResponse {

    @Schema(title = "Название курса")
    String title;

    @Schema(title = "Ссылка на вводный видеоролик")
    String video;

    @Schema(title = "Автор курса")
    String author;

    @Schema(title = "Список тегов курса")
    List<String> tags;

    @Schema(title = "Продолжительность курса")
    String duration;

    @Schema(title = "Стоимость курса")
    Integer price;

    @Schema(title = "Описание курса")
    String description;

    @Schema(title = "Получаемые навыки")
    String receivedSkills;

    @Schema(title = "Программа курса")
    List<StageDto> courseProgram;

    @Schema(title = "Куплен ли курс")
    boolean isCourseBought;

    @Schema(title = "Пройден ли курс")
    boolean completenessStatus;

    @Schema(title = "Идентификатор последнего открытого этапа")
    Long lastOpenedStageId;
}