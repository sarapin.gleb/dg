package ru.digitalgeneration.dgserver.business.dto.responses;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Value;

@Value
@Schema(title = "Тип задач ответ для студента")
public class TaskTypeForStudentResponse {

    @Schema(title = "Идентификатор типа задач")
    Long id;

    @Schema(title = "Ссылка на картинку типа задач в облаке S3")
    String imageUrl;

    @Schema(title = "Название типа задач")
    String title;

    @Schema(title = "Ценность типа задач")
    String taskValue;

    @Schema(title = "Открыт ли тип задач студенту")
    boolean isOpenForStudent;
}