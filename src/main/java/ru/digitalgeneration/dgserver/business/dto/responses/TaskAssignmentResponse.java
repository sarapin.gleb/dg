package ru.digitalgeneration.dgserver.business.dto.responses;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Value;

import java.time.LocalDateTime;

@Value
@Schema(title = "Назначение студента на задачу ответ")
public class TaskAssignmentResponse {

    @Schema(title = "JSON формы решения")
    Object solutionForm;

    @Schema(title = "Дата и время получения задачи")
    LocalDateTime receiptDate;
}