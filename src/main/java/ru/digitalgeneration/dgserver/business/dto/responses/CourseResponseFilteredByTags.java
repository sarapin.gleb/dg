package ru.digitalgeneration.dgserver.business.dto.responses;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Value;
import java.util.List;

@Value
@Schema(title = "Информация о курсе, отфильтрованного по тегам")
public class CourseResponseFilteredByTags {

    @Schema(title = "Идентификатор курса")
    Long id;

    @Schema(title = "Название курса")
    String title;

    @Schema(title = "Ссылка на картинку курса в облаке S3")
    String imageUrl;

    @Schema(title = "Автор курса")
    String author;

    @Schema(title = "Список тегов курса")
    List<String> tags;

    @Schema(title = "Стоимость курса")
    Integer price;

    @Schema(title = "Куплен ли курс")
    boolean purchaseStatus;

    @Schema(title = "Пройден ли курс")
    Boolean completenessStatus;
}