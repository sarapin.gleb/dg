package ru.digitalgeneration.dgserver.business.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Value;

@Value
@Schema(title = "Информация об этапе")
public class StageWithTypeDto {

    @Schema(title = "Идентификатор этапа")
    Long stageId;

    @Schema(title = "Название этапа")
    String title;

    @Schema(title = "Тип этапа")
    String type;

    @Schema(title = "Порядковый номер этапа в курсе")
    Integer orderNumber;
}