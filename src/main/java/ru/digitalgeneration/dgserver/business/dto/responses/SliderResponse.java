package ru.digitalgeneration.dgserver.business.dto.responses;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Value;
import ru.digitalgeneration.dgserver.business.dto.TaskDtoForSlider;

import java.time.Duration;
import java.util.List;

@Value
@Schema(title = "Задачи для слайдера ответ")
public class SliderResponse {

    @Schema(title = "Название типа задач")
    String title;

    @Schema(title = "Идентификатор курса, открывающего данный тип задач")
    Long courseId;

    @Schema(title = "Название курса, открывающего данный тип задач")
    String courseTitle;

    @Schema(title = "Время на решение")
    Duration solutionTime;

    @Schema(title = "Вознаграждение для студента")
    Integer reward;

    @Schema(title = "Идентификатор ценности типа задач")
    Long taskValueId;

    @Schema(title = "Ценность типа задач")
    String taskValue;

    @Schema(title = "Верифицирован ли студент (регистрация через VK, либо подтверждение телефона)")
    boolean isStudentVerified;

    @Schema(title = "Открыт ли тип задач студенту")
    boolean isOpenForStudent;

    @Schema(title = "Список задач")
    List<TaskDtoForSlider> tasks;
}