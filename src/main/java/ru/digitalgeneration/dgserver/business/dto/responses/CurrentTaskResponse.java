package ru.digitalgeneration.dgserver.business.dto.responses;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Value;

import java.time.Duration;
import java.time.LocalDateTime;

@Value
@Schema(title = "Информация о решаемой задаче")
public class CurrentTaskResponse {

    @Schema(title = "Дата и время взятия задачи")
    LocalDateTime receiptDate;

    @Schema(title = "Время, данное на решение задачи")
    Duration solutionTime;

    @Schema(title = "Идентификатор задачи")
    Long taskId;
}