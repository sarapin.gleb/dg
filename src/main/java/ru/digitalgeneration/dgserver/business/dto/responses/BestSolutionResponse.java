package ru.digitalgeneration.dgserver.business.dto.responses;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Value;

import java.util.List;

@Value
@Schema(title = "Лучшее решение ответ")
public class BestSolutionResponse {

    @Schema(title = "Идентификатор задачи")
    Long id;

    @Schema(title = "Название задачи")
    String title;

    @Schema(title = "Балл за решение")
    Integer score;

    @Schema(title = "Список тегов к оценке")
    List<String> gradeTags;

    @Schema(title = "Наличие отзыва")
    Boolean isFeedbackExist;
}