package ru.digitalgeneration.dgserver.business.dto.responses;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Value;

@Value
@Schema(title = "Тег к оценке ответ")
public class TagResponse {

    @Schema(title = "Значение тега")
    String title;

    @Schema(title = "Комментарий к тегу")
    String comment;
}