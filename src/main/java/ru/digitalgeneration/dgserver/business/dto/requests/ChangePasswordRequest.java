package ru.digitalgeneration.dgserver.business.dto.requests;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Value;
import ru.digitalgeneration.dgserver.business.entities.enums.LoginType;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Value
@Schema(title = "Запрос на изменение пароля")
public class ChangePasswordRequest {

    @NotBlank
    @Schema(title = "Новый пароль", required = true, example = "qwerty")
    String newPassword;

    @NotNull
    @Schema(title = "Тип авторизации", required = true, example = "email")
    LoginType loginType;

    @NotBlank
    @Schema(title = "Логин пользователя", required = true, example = "test@test.test")
    String login;

    @NotNull
    @Schema(title = "Идентификатор пользователя", required = true, example = "01234567-89ab-cdef-0123-456789abcdef")
    UUID uuid;
}