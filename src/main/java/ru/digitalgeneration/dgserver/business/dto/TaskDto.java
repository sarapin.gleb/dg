package ru.digitalgeneration.dgserver.business.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Value;

import java.util.List;

@Value
@Schema(title = "Взятая задача")
public class TaskDto {

    @Schema(title = "Идентификатор задачи")
    Long id;

    @Schema(title = "Название типа задач")
    String title;

    @Schema(title = "Оценка")
    Integer score;

    @Schema(title = "Список тегов к оценке")
    List<String> gradeTags;

    @Schema(title = "Наличие отзыва")
    Boolean isFeedbackExist;

    @Schema(title = "Являляется ли решение лучшим")
    boolean isSolutionBest;
}