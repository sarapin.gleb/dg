package ru.digitalgeneration.dgserver.business.dto.responses;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Value;

import java.util.UUID;

@Value
@Schema(title = "Сравнение кода для восстановления пароля через телефон")
public class CodeComparingForPasswordRecoveryResponse {

    @Schema(title = "Результат сравнения кода")
    boolean comparingResult;

    @Schema(title = "UUID пользователя")
    UUID uuid;
}