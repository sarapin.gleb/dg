package ru.digitalgeneration.dgserver.business.dto;

import lombok.Value;

@Value
public class NotEnoughPointsExceptionPayload {
    String message;
    String serviceTitle;
    Integer missingPoints;
}