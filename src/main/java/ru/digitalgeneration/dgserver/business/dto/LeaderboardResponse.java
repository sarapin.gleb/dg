package ru.digitalgeneration.dgserver.business.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Value;
import ru.digitalgeneration.dgserver.business.dto.responses.LeaderboardRowResponse;

import java.util.List;

@Value
@Schema(title = "Лидерборд, ответ")
public class LeaderboardResponse {

    @Schema(title = "Место текущего пользователя в рейтинге")
    Integer ratingPosition;

    @Schema(title = "Имя текущего пользователя")
    String fName;

    @Schema(title = "Фамилия текущего пользователя")
    String sName;

    @Schema(title = "Отчество текущего пользователя")
    String mName;

    @Schema(title = "Количество решений текущего пользователя, признанных лучшими")
    Integer bestSolutionsCount;

    @Schema(title = "Заработанная текущим пользователем сумма денег в рублях")
    Double moneyEarnedAmount;

    @Schema(title = "OpenId текущего пользователя, показываем его, если нет имени, и по нему получаем другие данные о пользователе на публичной странице")
    Integer openId;

    @Schema(title = "Лидерборд")
    List<LeaderboardRowResponse> users;

    public LeaderboardResponse(Integer ratingPosition, String fName, String sName, String mName, Integer bestSolutionsCount, Double moneyEarnedAmount, Integer openId, List<LeaderboardRowResponse> users) {
        this.bestSolutionsCount = bestSolutionsCount;
        this.fName = fName;
        this.mName = mName;
        this.sName = sName;
        this.openId = openId;
        this.moneyEarnedAmount = moneyEarnedAmount;
        this.ratingPosition = ratingPosition;
        this.users = users;
    }

    public LeaderboardResponse(String fName, String sName, String mName, Integer openId, List<LeaderboardRowResponse> users) {
        // TODO сделать так чтобы не прописывать строки с null'ами, поскольку они и так должны становиться null, если их не определять в этом методе
        this.bestSolutionsCount = null;
        this.fName = fName;
        this.mName = mName;
        this.sName = sName;
        this.openId = openId;
        this.moneyEarnedAmount = null;
        this.ratingPosition = null;
        this.users = users;
    }
}