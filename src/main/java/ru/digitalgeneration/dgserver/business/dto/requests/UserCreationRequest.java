package ru.digitalgeneration.dgserver.business.dto.requests;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Value;
import ru.digitalgeneration.dgserver.business.annotations.validation.UsernameValid;
import ru.digitalgeneration.dgserver.business.entities.enums.LoginType;
import ru.digitalgeneration.dgserver.business.interfaces.HasUsernameAndType;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Value
@UsernameValid
@Schema(title = "Создание нового пользователя")
public class UserCreationRequest implements HasUsernameAndType {

    @NotNull
    @Schema(title = "Тип авторизации", required = true, example = "email")
    LoginType loginType;

    @JsonProperty("login")
    @NotBlank
    @Schema(title = "Логин пользователя", description = "E-mail или телефон", required = true, example = "test@test.test")
    String username;

    @NotBlank
    @Schema(title = "Пароль", required = true, example = "qwerty")
    String password;

    @NotBlank
    @Schema(title = "Роль", required = true, example = "STUDENT")
    String role;

    @NotBlank
    @Schema(title = "Верификационная ссылка", required = true, example = "https://digitalgeneration.ru/test")
    String linkToConfirmationPage;

    @Schema(title = "Название рекламной площадки", example = "google")
    String utmSource;

    @Schema(title = "Тип рекламы", example = "social_cpc")
    String utmMedium;

    @Schema(title = "Название рекламной кампании", example = "test")
    String utmCampaign;

    @Schema(title = "Ключевое слово, с которого начался показ объявления", example = "test")
    String utmTerm;

    @Schema(title = "Дополнительная информация", example = "test")
    String utmContent;
}