package ru.digitalgeneration.dgserver.business.dto.responses;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Value;

@Value
@Schema(title = "Данные о пользователе")
public class UserResponse {

    @Schema(title = "E-mail")
    String email;

    @Schema(title = "Верифицирован ли e-mail")
    Boolean emailVerified;

    @Schema(title = "Телефон")
    String phone;

    @Schema(title = "Верифицирован ли телефон")
    Boolean phoneVerified;

    @Schema(title = "Имя")
    String fName;

    @Schema(title = "Фамилия")
    String sName;

    @Schema(title = "Отчество")
    String mName;

    @Schema(title = "Привязан ли VK к аккаунту")
    Boolean isVkExist;

    @Schema(title = "Подтверждал ли пользователь телефон когда-либо ранее")
    Boolean phoneFirstConfirmation;

    @Schema(title = "Количество полученных за решения тегов \"Аналитик\"")
    Integer analyst;

    @Schema(title = "Количество полученных за решения тегов \"Телепат\"")
    Integer telepath;

    @Schema(title = "Количество полученных за решения тегов \"Предприниматель\"")
    Integer entrepreneur;

    @Schema(title = "Количество полученных за решения тегов \"Инсайдер\"")
    Integer insider;

    @Schema(title = "Позиция в рейтинге")
    Integer ratingPosition;

    @Schema(title = "Количество решений пользователя, признанных лучшими")
    Integer bestSolutionsCount;

    @Schema(title = "Открытый id пользователя, получаем по нему данные о пользователе с аккаунтов других пользователей не раскрывая jwtId (публичная страница, лучшие решения)")
    Integer openId;
}