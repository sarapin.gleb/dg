package ru.digitalgeneration.dgserver.business.dto.requests;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Value;

import javax.validation.constraints.NotNull;

@Value
@Schema(title = "Создание задачи")
public class TaskRequest {

    @NotNull
    @Schema(title = "Идентификатор типа задачи", required = true, example = "1")
    Long taskTypeId;

    @NotNull
    @Schema(title = "JSON с формулировкой задачи", required = true, example = "{\"test\": \"test\"}")
    Object formulation;

    @NotNull
    @Schema(title = "Количество заказанных решений", required = true, example = "2")
    Integer decisionsRequiredNumber;
}