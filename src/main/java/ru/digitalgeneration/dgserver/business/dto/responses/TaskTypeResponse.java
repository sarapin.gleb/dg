package ru.digitalgeneration.dgserver.business.dto.responses;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Value;

import java.time.Duration;

@Value
@Schema(title = "Тип задач ответ")
public class TaskTypeResponse {

    @Schema(title = "Название типа задач")
    String title;

    @Schema(title = "Идентификатор курса, открывающего данный тип задач")
    Long courseId;

    @Schema(title = "Название курса, открывающего данный тип задач")
    String courseTitle;

    @Schema(title = "Время на решение")
    Duration solutionTime;

    @Schema(title = "Вознаграждение для студента")
    Integer reward;

    @Schema(title = "Идентификатор ценности типа задач")
    Long taskValueId;

    @Schema(title = "Ценность типа задач")
    String taskValue;
}