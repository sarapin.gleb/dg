package ru.digitalgeneration.dgserver.business.dto.requests;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Value;
import ru.digitalgeneration.dgserver.business.annotations.validation.Email;

import javax.validation.constraints.NotBlank;

@Value
@Schema(title = "Страница, заинтересовавшая пользователя")
public class FavoritePageRequest {

    @Email
    @Schema(title = "E-mail", required = true, example = "test@test.test")
    String email;

    @NotBlank
    @Schema(title = "Адрес страницы", required = true, example = "https://digitalgeneration.ru/test")
    String pageUrl;
}