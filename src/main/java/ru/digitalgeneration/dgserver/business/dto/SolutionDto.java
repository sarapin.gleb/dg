package ru.digitalgeneration.dgserver.business.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Value;

@Value
@Schema(title = "Информация о решении")
public class SolutionDto {

    @Schema(title = "Идентификатор решения")
    Long id;

    @Schema(title = "Балл за решение")
    Integer score;

    @Schema(title = "Есть ли жалоба на решение")
    boolean isComplainted;
}