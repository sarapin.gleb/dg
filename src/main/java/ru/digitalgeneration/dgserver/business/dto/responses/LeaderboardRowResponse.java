package ru.digitalgeneration.dgserver.business.dto.responses;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Value;

@Value
@Schema(title = "Информация о пользователе для лидерборда, ответ")
public class LeaderboardRowResponse {

    @Schema(title = "Идентификатор пользователя")
    Integer userId;

    @Schema(title = "Имя")
    String fName;

    @Schema(title = "Фамилия")
    String sName;

    @Schema(title = "Отчество")
    String mName;

    @Schema(title = "Количество решений пользователя, признанных лучшими")
    Integer bestSolutionsCount;

    @Schema(title = "Заработанная сумма денег в рублях")
    Double moneyEarnedAmount;
}