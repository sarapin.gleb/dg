package ru.digitalgeneration.dgserver.business.dto.responses;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Value;

@Value
@Schema(title = "Тип задач ответ для предпринимателя")
public class TaskTypeForEntrepreneurResponse {

    @Schema(title = "Идентификатор типа задач")
    Long id;

    @Schema(title = "Краткое описание")
    String briefDescription;

    @Schema(title = "Ссылка на картинку типа задач в облаке S3")
    String imageUrl;

    @Schema(title = "Название типа задач")
    String title;

    @Schema(title = "Идентификатор ценности типа задач")
    Long taskValueId;

    @Schema(title = "Стоимость для предпринимателя")
    Integer price;
}