package ru.digitalgeneration.dgserver.business.dto.responses;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Value;

@Value
@Schema(title = "Начисление бонуса ответ")
public class BonusInfoResponse {

    @Schema(title = "Начислен ли бонус")
    boolean isBonusAccrued;
}