package ru.digitalgeneration.dgserver.business.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Value;
import ru.digitalgeneration.dgserver.business.dto.responses.FileResponse;

import java.util.List;

@Value
@Schema(title = "Задача для слайдера")
public class TaskDtoForSlider {

    @Schema(title = "Идентификатор задачи")
    Long id;

    @Schema(title = "JSON постановки задачи")
    Object formulation;

    @Schema(title = "Список файлов из постановки задачи")
    List<FileResponse> files;

    @Schema(title = "Удовлетворяет ли задача всем критериям \"специальной\"")
    boolean isSpecialTask;
}