package ru.digitalgeneration.dgserver.business.dto.responses;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Value;
import ru.digitalgeneration.dgserver.business.dto.SolutionDto;

import java.time.LocalDateTime;
import java.util.List;

@Value
@Schema(title = "Задача со списком решений ответ")
public class TaskResponseWithSolutionList {

    @Schema(title = "Название типа задач")
    String title;

    @Schema(title = "Статус выполнения задачи")
    String taskExecutionStatus;

    @Schema(title = "Крайний срок выполнения задачи")
    LocalDateTime deadline;

    @Schema(title = "Количество заказанных решений")
    Integer orderedSolutionsNumber;

    @Schema(title = "Количество полученных решений")
    Integer receivedSolutionsNumber;

    @Schema(title = "Количество оцененных решений")
    Integer evaluatedSolutionsNumber;

    @Schema(title = "JSON постановки задачи")
    Object formulation;

    @Schema(title = "Список файлов из постановки задачи")
    List<FileResponse> files;

    @Schema(title = "Список решений")
    List<SolutionDto> solutions;

    @Schema(title = "Идентификатор решения, признанного лучшим")
    Long bestSolutionId;
}