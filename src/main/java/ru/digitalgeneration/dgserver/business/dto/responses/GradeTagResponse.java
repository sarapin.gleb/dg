package ru.digitalgeneration.dgserver.business.dto.responses;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Value;

@Value
@Schema(title = "Теги к оценке ответ")
public class GradeTagResponse {

    @Schema(title = "Идентификатор тега")
    Long taskTypeGradeTagId;

    @Schema(title = "Значение тега")
    String value;

    @Schema(title = "Комментарий к тегу")
    String comment;
}