package ru.digitalgeneration.dgserver.business.dto.requests;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Value;

import java.util.Map;

@Value
@Schema(title = "Запрос на обновление транзакции")
public class UpdateTransactionRequest {

    @Schema(title = "Идентификатор терминала", required = true, example = "1234567890123")
    String TerminalKey;

    @Schema(title = "Идентификатор заказа в системе продавца", required = true, example = "12345")
    String OrderId;

    @Schema(title = "Успешность операции", required = true, example = "true")
    boolean Success;

    @Schema(title = "Статус платежа", required = true, example = "CONFIRMED")
    String Status;

    @Schema(title = "Идентификатор платежа в системе банка", required = true, example = "1234567890")
    long PaymentId;

    @Schema(title = "Код ошибки", required = true, example = "0")
    String ErrorCode;

    @Schema(title = "Сумма в копейках", required = true, example = "1000")
    int Amount;

    @Schema(title = "Идентификатор рекуррентного платежа", example = "12345")
    long RebillId;

    @Schema(title = "Идентификатор карты в системе банка. Передается только для сохраненной карты", example = "123456")
    long CardId;

    @Schema(title = "Замаскированный номер карты", required = true, example = "430000**0777")
    String Pan;

    @Schema(title = "Срок действия карты", required = true, example = "1122")
    String ExpDate;

    @Schema(title = "Подпись запроса", required = true, example = "d0815e288f121255d5d6b77831fb486cc5e9f91914a3f58a99b6118b54676d84")
    String Token;

    @Schema(title = "Дополнительные параметры платежа в формате «ключ:значение» (не более 20 пар)", example = "{\"test\": \"test\"}")
    Map<String, String> DATA;
}