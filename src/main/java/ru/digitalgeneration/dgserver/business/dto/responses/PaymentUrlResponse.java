package ru.digitalgeneration.dgserver.business.dto.responses;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Value;

@Value
@Schema(title = "Ссылка на форму оплаты ответ")
public class PaymentUrlResponse {

    @Schema(title = "Ссылка на форму оплаты")
    String paymentUrl;
}