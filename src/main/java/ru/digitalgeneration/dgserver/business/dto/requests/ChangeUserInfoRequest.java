package ru.digitalgeneration.dgserver.business.dto.requests;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Value;

@Value
@Schema(title = "Изменение информации о пользователе")
public class ChangeUserInfoRequest {

    @Schema(title = "E-mail", example = "test@test.test")
    String email;

    @Schema(title = "Телефон", example = "+7 (999) 999-99-99")
    String phone;

    @Schema(title = "Имя", example = "Иван")
    String fName;

    @Schema(title = "Фамилия", example = "Иванов")
    String sName;

    @Schema(title = "Отчество", example = "Иванович")
    String mName;
}