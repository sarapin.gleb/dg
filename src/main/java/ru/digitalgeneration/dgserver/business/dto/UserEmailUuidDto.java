package ru.digitalgeneration.dgserver.business.dto;

import lombok.Value;

import java.util.UUID;

@Value
public class UserEmailUuidDto {
    String email;
    UUID uuid;
}