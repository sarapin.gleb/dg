package ru.digitalgeneration.dgserver.business.dto.responses;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Value;
import ru.digitalgeneration.dgserver.business.dto.StageWithTypeDto;

import java.util.List;

@Value
@Schema(title = "Информация о курсе, с данными по этапам")
public class CourseWithStagesResponse {

    @Schema(title = "Название курса")
    String courseTitle;

    @Schema(title = "Идентификатор последнего открытого этапа")
    Long lastStage;

    @Schema(title = "Список этапов курса")
    List<StageWithTypeDto> stages;
}