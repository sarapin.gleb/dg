package ru.digitalgeneration.dgserver.business.dto.responses;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Value;

@Value
@Schema(title = "Форма постановки задачи ответ")
public class TaskTypeFormResponse {

    @Schema(title = "Название типа задач")
    String taskTypeTitle;

    @Schema(title = "JSON постановки задачи")
    Object formulation;

    @Schema(title = "Стоимость одного решения")
    Integer pricePerOneSolution;
}