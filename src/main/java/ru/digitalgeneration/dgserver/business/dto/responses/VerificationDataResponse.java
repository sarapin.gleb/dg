package ru.digitalgeneration.dgserver.business.dto.responses;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Value;

@Value
@Schema(title = "Данные о верификации")
public class VerificationDataResponse {

    @Schema(title = "Верифицирован ли e-mail")
    Boolean emailVerify;

    @Schema(title = "Верифицирован ли телефон")
    Boolean phoneVerify;
}