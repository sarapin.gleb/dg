package ru.digitalgeneration.dgserver.business.dto.requests;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Value;

import javax.validation.constraints.NotNull;
import java.util.List;

@Value
@Schema(title = "Запрос на удаление файлов")
public class RemoveFilesRequest {

    @NotNull
    @Schema(title = "Идентификатор задачи", required = true, example = "1")
    Long taskId;

    @NotNull
    @Schema(title = "Список ID удаляемых файлов", required = true, example = "[1, 2]")
    List<Long> files;
}