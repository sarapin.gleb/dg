package ru.digitalgeneration.dgserver.business.dto.requests;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Value;

import javax.validation.constraints.NotBlank;

@Value
@Schema(title = "Запрос на изменение пароля в личном кабинете")
public class ChangePasswordInPersonalAreaRequest {

    @NotBlank
    @Schema(title = "Новый пароль", required = true, example = "qwerty")
    String newPassword;

    @JsonCreator
    public ChangePasswordInPersonalAreaRequest(@JsonProperty("newPassword") String newPassword) {
        this.newPassword = newPassword;
    }
}