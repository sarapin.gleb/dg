package ru.digitalgeneration.dgserver.business.dto.responses;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Value;

@Value
@Schema(title = "Тег к жалобе ответ")
public class TagComplaintResponse {

    @Schema(title = "Идентификатор тега")
    Long id;

    @Schema(title = "Значение тега")
    String value;
}