package ru.digitalgeneration.dgserver.business.dto.responses;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Value;

@Value
@Schema(title = "Стоимость балла ответ")
public class PointPriceResponse {

    @Schema(title = "Стоимость балла")
    Integer pointPrice;
}