package ru.digitalgeneration.dgserver.business.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Value;

import java.time.Duration;

@Value
@Schema(title = "Данные об этапе")
public class StageDto {

    @Schema(title = "Название этапа")
    String title;

    @Schema(title = "Описание этапа")
    String description;

    @Schema(title = "Продолжительность этапа")
    Duration duration;
}