package ru.digitalgeneration.dgserver.business.dto.responses;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Value;

@Value
@Schema(title = "Статистика студента ответ")
public class StudentStatResponse {

    @Schema(title = "Имя пользователя")
    String name;

    @Schema(title = "Количество решенных задач")
    Integer solvedTasks;

    @Schema(title = "Количество пройденных курсов")
    Integer completedCourses;

    @Schema(title = "Количество часов обучения")
    Integer learningHours;

    @Schema(title = "Количество заработанных баллов")
    Integer receivedPoints;
}