package ru.digitalgeneration.dgserver.business.dto;

import lombok.Value;

@Value
public class StudentTemporarilyBlockedExceptionPayload {
    String message;
    Integer secondsToUnlock;
}