package ru.digitalgeneration.dgserver.business.dto.responses;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Value;

import java.time.LocalDateTime;

@Value
@Schema(title = "Поставленная задача ответ")
public class TaskResponseForEntrepreneur {

    @Schema(title = "Идентификатор задачи")
    Long id;

    @Schema(title = "Название типа задач")
    String title;

    @Schema(title = "Статус выполнения")
    String executionStatus;

    @Schema(title = "Крайний срок выполнения задачи")
    LocalDateTime deadline;

    @Schema(title = "Количество заказанных решений")
    Integer orderedSolutionsNumber;

    @Schema(title = "Количество полученных решений")
    Integer receivedSolutionsNumber;

    @Schema(title = "Количество оцененных решений")
    Integer evaluatedSolutionsNumber;

    @Schema(title = "Показывать ли для выбора лучшего решения")
    boolean isShowToChooseBestSolution;

    @Schema(title = "Выбрано ли лучшее решение")
    boolean isBestSolutionChosen;
}