package ru.digitalgeneration.dgserver.business.dto.responses;

import lombok.Value;

@Value
public class TinkoffInitResponse {
    String errorCode;
    String status;
    String paymentId;
    String paymentUrl;
}