package ru.digitalgeneration.dgserver.business.dto.responses;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Value;
import ru.digitalgeneration.dgserver.business.dto.TaskDto;

import java.util.List;

@Value
@Schema(title = "Оцененные задачи ответ")
public class EvaluatedTasksResponse {

    @Schema(title = "Открыты ли студенту какие-либо типы задач")
    boolean isOpenTaskTypesExist;

    @Schema(title = "Список оцененных задач")
    List<TaskDto> evaluatedTasks;
}