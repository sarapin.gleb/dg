package ru.digitalgeneration.dgserver.business.dto.responses;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Value;

@Value
@Schema(title = "Сравнение кода для верификации телефона")
public class CodeComparingForPhoneVerificationResponse {

    @Schema(title = "Результат сравнения кода")
    boolean comparingResult;

    @Schema(title = "Начислен ли бонус")
    boolean isBonusAccrued;
}