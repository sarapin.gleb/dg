package ru.digitalgeneration.dgserver.business.dto.responses;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Value;

import java.time.LocalDateTime;

@Value
@Schema(title = "Информация о планируемой фиче")
public class FeatureResponse {

    @Schema(title = "Дата релиза")
    LocalDateTime releaseDate;

    @Schema(title = "Название фичи")
    String title;

    @Schema(title = "Описание")
    String description;
}