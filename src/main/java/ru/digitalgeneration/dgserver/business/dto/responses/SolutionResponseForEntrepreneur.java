package ru.digitalgeneration.dgserver.business.dto.responses;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Value;
import ru.digitalgeneration.dgserver.business.dto.GradeDto;

import java.util.List;

@Value
@Schema(title = "Решение для предпринимателя ответ")
public class SolutionResponseForEntrepreneur {

    @Schema(title = "JSON решения")
    Object solution;

    @Schema(title = "Список файлов")
    List<FileResponse> files;

    @Schema(title = "Информация об оценке")
    GradeDto gradeDto;

    @Schema(title = "Жалоба на решение")
    String complaint;
}