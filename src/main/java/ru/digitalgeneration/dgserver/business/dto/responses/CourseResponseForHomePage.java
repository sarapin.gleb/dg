package ru.digitalgeneration.dgserver.business.dto.responses;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Value;

import java.time.Duration;
import java.util.List;

@Value
@Schema(title = "Информация о курсе для главной страницы")
public class CourseResponseForHomePage {

    @Schema(title = "Идентификатор курса")
    Long id;

    @Schema(title = "Название курса")
    String courseTitle;

    @Schema(title = "Ссылка на картинку курса в облаке S3")
    String imageUrl;

    @Schema(title = "Описание курса")
    String description;

    @Schema(title = "Список тегов курса")
    List<String> tags;

    @Schema(title = "Стоимость курса")
    Integer price;

    @Schema(title = "Количество открываемых типов задач")
    Integer openTaskTypesNumber;

    @Schema(title = "Количество лекций в курсе")
    Integer courseLecturesNumber;

    @Schema(title = "Количество тестов в курсе")
    Integer courseTestsNumber;

    @Schema(title = "Количество пройденных этапов")
    Integer passedStagesNumber;

    @Schema(title = "Время, необходимое для завершения курса")
    Duration requiredTime;
}