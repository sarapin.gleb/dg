package ru.digitalgeneration.dgserver.business.dto.responses;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Value;

@Value
@Schema(title = "Баланс ответ")
public class BalanceResponse {

    @Schema(title = "Баланс")
    Integer balance;
}