package ru.digitalgeneration.dgserver.business.dto.responses;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Value;

@Value
@Schema(title = "Информация о файле")
public class FileResponse {

    @Schema(title = "Идентификатор файла")
    Long id;

    @Schema(title = "Ссылка на файл в облаке S3")
    String linkToS3;

    @Schema(title = "Название файла")
    String fileName;
}