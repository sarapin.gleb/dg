package ru.digitalgeneration.dgserver.business.dto.requests;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Value;

import javax.validation.constraints.NotNull;

@Value
@Schema(title = "Запрос на отправку решения")
public class SolutionRequest {

    @NotNull
    @Schema(title = "Идентификатор задачи", required = true, example = "1")
    Long taskId;

    @NotNull
    @Schema(title = "JSON решения", required = true, example = "{\"test\": \"test\"}")
    Object solution;

    @NotNull
    @Schema(title = "Количество слов в разделе \"Вывод\" решения", required = true, example = "10")
    Integer conclusionWordsNumber;

    @NotNull
    @Schema(title = "Является ли черновиком", required = true, example = "true")
    boolean isDraft;
}