package ru.digitalgeneration.dgserver.business.dto.responses;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Value;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

@Value
@Schema(title = "Решение для студента ответ")
public class SolutionResponseForStudent {

    @Schema(title = "Название типа задач")
    String title;

    @Schema(title = "Дата и время сдачи решения")
    LocalDateTime deliveryDate;

    @Schema(title = "Оценка")
    Integer score;

    @Schema(title = "Список тегов к оценке")
    List<TagResponse> tags;

    @Schema(title = "Отзыв на решение")
    String feedback;

    @Schema(title = "JSON постановки задачи")
    Object formulation;

    @Schema(title = "Список файлов из постановки задачи")
    List<FileResponse> taskFiles;

    @Schema(title = "Время, отведенное на решение")
    Duration solutionTime;

    @Schema(title = "Вознаграждение для студента")
    Integer reward;

    @Schema(title = "JSON решения")
    Object solution;

    @Schema(title = "Список файлов, приложенных к решению")
    List<FileResponse> solutionFiles;
}