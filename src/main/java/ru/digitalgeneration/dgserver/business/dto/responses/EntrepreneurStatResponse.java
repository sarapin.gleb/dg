package ru.digitalgeneration.dgserver.business.dto.responses;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Value;

@Value
@Schema(title = "Статистика предпринимателя ответ")
public class EntrepreneurStatResponse {

    @Schema(title = "Имя пользователя")
    String name;

    @Schema(title = "Количество заказанных решений")
    Integer orderedSolutionsNumber;

    @Schema(title = "Количество полученных решений")
    Integer receivedSolutionsNumber;

    @Schema(title = "Количество оцененных решений")
    Integer evaluatedSolutionsNumber;
}