package ru.digitalgeneration.dgserver.business.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Value;
import ru.digitalgeneration.dgserver.business.dto.responses.TagResponse;

import java.util.List;

@Value
@Schema(title = "Информация об оценке")
public class GradeDto {

    @Schema(title = "Балл за решение")
    Integer score;

    @Schema(title = "Список тегов к оценке")
    List<TagResponse> tags;

    @Schema(title = "Отзыв на решение")
    String feedback;
}