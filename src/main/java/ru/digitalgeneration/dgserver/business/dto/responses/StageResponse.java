package ru.digitalgeneration.dgserver.business.dto.responses;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Value;

@Value
@Schema(title = "Страница этапа ответ")
public class StageResponse {

    @Schema(title = "Название этапа")
    String title;

    @Schema(title = "Тип этапа")
    String type;

    @Schema(title = "Описание этапа")
    String description;

    @Schema(title = "Контент")
    String content;

    @Schema(title = "Идентификатор типа задач, открываемого этим этапом")
    Long openTaskType;

    @Schema(title = "Идентификатор предыдущего этапа")
    Long prevStageId;

    @Schema(title = "Название предыдущего этапа")
    String prevStageTitle;

    @Schema(title = "Идентификатор следующего этапа")
    Long nextStageId;

    @Schema(title = "Название следующего этапа")
    String nextStageTitle;

    @Schema(title = "Идентификатор курса")
    Long courseId;

    @Schema(title = "Идентификатор последнего открытого этапа в данном курсе")
    Long lastOpenStage;

    @Schema(title = "Количество секунд от начала просмотра видео (если запрашиваемый этап является последним просмотренным)")
    Integer timing;
}