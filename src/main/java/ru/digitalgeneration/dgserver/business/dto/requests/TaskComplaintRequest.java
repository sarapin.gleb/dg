package ru.digitalgeneration.dgserver.business.dto.requests;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Value;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Value
@Schema(title = "Создание жалобы на задачу")
public class TaskComplaintRequest {

    @NotNull
    @Schema(title = "Идентификатор задачи", required = true, example = "1")
    Long taskId;

    @NotBlank
    @Schema(title = "Текст жалобы", required = true, example = "Оскорбительные высказывания")
    String value;

    @NotNull
    @Schema(title = "Список ID тегов к жалобам на задачу", required = true, example = "[1, 2]")
    List<Long> tagIdList;
}