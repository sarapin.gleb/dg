package ru.digitalgeneration.dgserver.business.dto.responses;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Value;

@Value
@Schema(title = "Незаконченный курс ответ")
public class CourseWithLastStageResponse {

    @Schema(title = "Идентификатор курса")
    Long courseId;

    @Schema(title = "Название курса")
    String courseTitle;

    @Schema(title = "Ссылка на картинку курса в облаке S3")
    String imageUrl;

    @Schema(title = "Идентификатор последнего доступного этапа (максимального, открытого в курсе)")
    Long lastStageId;

    @Schema(title = "Тип последнего доступного этапа")
    String stageType;

    @Schema(title = "Название последнего доступного этапа")
    String stageTitle;
}