package ru.digitalgeneration.dgserver.business.dto.responses;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Value;

import java.time.LocalDateTime;
import java.util.List;

@Value
@Schema(title = "Информация о курсе пользователя, отфильтрованного по тегам")
public class CourseResponseFilteredByTagsForUser {

    @Schema(title = "Идентификатор курса")
    Long id;

    @Schema(title = "Название курса")
    String title;

    @Schema(title = "Ссылка на картинку курса в облаке S3")
    String imageUrl;

    @Schema(title = "Автор курса")
    String author;

    @Schema(title = "Список тегов курса")
    List<String> tags;

    @Schema(title = "Пройден ли курс")
    boolean completenessStatus;

    @Schema(title = "Дата последнего взаимодействия с курсом")
    LocalDateTime lastActionDate;
}