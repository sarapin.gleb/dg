package ru.digitalgeneration.dgserver.business.dto.requests;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Value;

import javax.validation.constraints.NotNull;
import java.util.List;

@Value
@Schema(title = "Запрос на создание оценки")
public class GradeRequest {

    @NotNull
    @Schema(title = "Идентификатор решения", required = true, example = "1")
    Long solutionId;

    @NotNull
    @Schema(title = "Список ID тегов к оценке", required = true, example = "[1, 2]")
    List<Long> taskTypeGradeTags;

    @NotNull
    @Schema(title = "Оценка", required = true, example = "5")
    Integer score;

    @Schema(title = "Отзыв", example = "Хорошая работа")
    String feedback;
}