package ru.digitalgeneration.dgserver.business.config;

import com.mashape.unirest.http.exceptions.UnirestException;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.transaction.annotation.Transactional;
import ru.digitalgeneration.dgserver.business.connections.MailServiceConnection;
import ru.digitalgeneration.dgserver.business.entities.Task;
import ru.digitalgeneration.dgserver.business.entities.Transaction;
import ru.digitalgeneration.dgserver.business.entities.User;
import ru.digitalgeneration.dgserver.business.entities.enums.PaymentStatus;
import ru.digitalgeneration.dgserver.business.entities.enums.TaskExecutionStatus;
import ru.digitalgeneration.dgserver.business.repositories.jpa_repositories.BillingJpaRepository;
import ru.digitalgeneration.dgserver.business.repositories.jpa_repositories.TaskJpaRepository;
import ru.digitalgeneration.dgserver.business.repositories.jpa_repositories.UserJpaRepository;
import ru.digitalgeneration.dgserver.business.services.BillingService;
import ru.digitalgeneration.dgserver.business.services.MixpanelService;

import javax.mail.MessagingException;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.stream.Collectors;

@Configuration
@EnableScheduling
@RequiredArgsConstructor
public class SchedulerConfig {

    public static final int UPDATE_TRANSACTIONS_DATA_DELAY = 5 * 60 * 1000;
    public static final int UPDATE_EXPIRED_TASKS_DELAY = 24 * 60 * 60 * 1000;

    private static final Logger log = LogManager.getLogger();

    @Value("${scheduler.transactions.seconds_from_last_status_change}")
    private int secondsFromLastStatusChange;
    @Value("${scheduler.transactions.seconds_from_creation}")
    private int secondsFromCreation;

    private final BillingJpaRepository billingJpaRepository;
    private final BillingService billingService;
    private final UserJpaRepository userRepository;
    private final TaskJpaRepository taskRepository;
    private final MailServiceConnection mailServiceConnection;
    private final MixpanelService mixpanelService;

    @Transactional
    @Scheduled(fixedDelay = UPDATE_TRANSACTIONS_DATA_DELAY)
    public void updateTransactionsData() {
        log.trace("Update transactions data: scheduled method start");
        List<Transaction> transactions = billingJpaRepository.findAllByPaymentStatusIs(PaymentStatus.IN_PROCESS.getValue());
        transactions.forEach(transaction -> {
            if (transaction.getLastModifiedDate().plusSeconds(secondsFromLastStatusChange).isBefore(LocalDateTime.now())) {
                if (transaction.getLastModifiedDate().plusSeconds(secondsFromCreation).isBefore(LocalDateTime.now())) {
                    transaction.setPaymentStatus(PaymentStatus.CANCELED.getValue());
                    billingJpaRepository.saveAndFlush(transaction);
                    log.info("Update transactions data: transaction #" + transaction.getId() + " is cancelled");
                } else {
                    try {
                        String paymentStatus = billingService.updateTinkoffState(transaction);
                        User user = transaction.getUser();
                        if (paymentStatus.equals(PaymentStatus.PAID.getValue())) {
                            user.setPointBalance(user.getPointBalance() + transaction.getPoints());
                            user.setLastPointsPurchaseDate(LocalDateTime.now());
                            userRepository.saveAndFlush(user);
                            log.info("Update transactions data: transaction #" + transaction.getId() + " is paid");
                            if (user.getTransactions().stream().filter(tr -> tr.getPaymentStatus().equals(PaymentStatus.PAID.getValue())).count() == 1) {
                                mixpanelService.createFirstPointsPurchaseEvent(user, transaction.getId(), transaction.getPoints());
                            } else {
                                mixpanelService.createPointsPurchaseEvent(user, transaction.getId(), transaction.getPoints());
                            }
                        }
                    } catch (UnirestException | IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    @Transactional
    @Scheduled(fixedDelay = UPDATE_EXPIRED_TASKS_DELAY)
    public void updateExpiredTasks() { //TODO подумать, как возвращать деньги
        log.trace("Update tasks with an expired deadline: scheduled method start");
        List<Task> tasks = taskRepository.findAllByExecutionStatusAndDeadlineBefore(TaskExecutionStatus.IN_PROGRESS.toString(), LocalDateTime.now());
        taskRepository.saveAllAndFlush(tasks
                .stream()
                .peek(task -> task.setExecutionStatus(TaskExecutionStatus.NOT_DONE.toString()))
                .peek(task -> sendTaskNotDoneMail(
                        task.getId(),
                        task.getTaskType().getTitle(),
                        task.getEntrepreneur().getSName(),
                        task.getEntrepreneur().getFName(),
                        task.getEntrepreneur().getMName(),
                        task.getEntrepreneur().getEmail(),
                        task.getEntrepreneur().getPhone()))
                .collect(Collectors.toList()));
        log.trace("Update tasks with an expired deadline: scheduled method completed");
    }

    private void sendTaskNotDoneMail(Long taskId, String taskTitle, String sName, String fName, String mName, String email, String phone) {
        try {
            mailServiceConnection.sendTaskNotDoneMail(taskId, taskTitle, sName, fName, mName, email, phone);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    @Transactional
    @Scheduled(cron = "@daily")
    public void dailyTaskPriorityUpdate() {
        log.trace("Daily task priority update: scheduled method start");
        List<Task> tasks = taskRepository.findAllByExecutionStatus(TaskExecutionStatus.IN_PROGRESS.toString());
        taskRepository.saveAllAndFlush(tasks
                .stream()
                .peek(task -> task.setPriority((double) ChronoUnit.DAYS.between(LocalDateTime.now(), task.getDeadline()) / (task.getDecisionsRequiredNumber() - task.getSolutions().size())))
                .collect(Collectors.toList()));
        log.trace("Daily task priority update: scheduled method completed");
    }
}