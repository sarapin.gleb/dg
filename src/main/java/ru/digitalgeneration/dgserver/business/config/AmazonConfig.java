package ru.digitalgeneration.dgserver.business.config;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.digitalgeneration.dgserver.properties.S3Properties;

@Configuration
@RequiredArgsConstructor
public class AmazonConfig {

    private final S3Properties s3Properties;

    @Bean
    public AmazonS3Client amazonS3Client() {
        AWSCredentials awsCredentials =
                new BasicAWSCredentials(s3Properties.getCredentials().getAccessKey(), s3Properties.getCredentials().getSecretKey());
        return (AmazonS3Client) AmazonS3ClientBuilder.standard()
                .withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration(s3Properties.getServiceEndpoint(), s3Properties.getSigningRegion()))
                .withCredentials(new AWSStaticCredentialsProvider(awsCredentials))
                .build();
    }
}