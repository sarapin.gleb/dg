package ru.digitalgeneration.dgserver.business.aspects;

import lombok.RequiredArgsConstructor;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.webjars.NotFoundException;
import ru.digitalgeneration.dgserver.business.repositories.jpa_repositories.UserJpaRepository;
import ru.digitalgeneration.dgserver.business.utils.AuthUtils;
import ru.digitalgeneration.dgserver.security.config.DbUserDetails;

import java.time.LocalDateTime;
import java.util.Arrays;

@Aspect
@Component
@RequiredArgsConstructor
public class TimestampAspect {

    private final UserJpaRepository userJpaRepository;

    @AfterReturning(value = "execution(* ru.digitalgeneration.dgserver.security.logic.jwt.JwtTokenUtil.validateToken(..))", returning = "isTokenValid")
    public void updateTimestampAfterAuthorization(JoinPoint joinPoint, Boolean isTokenValid) {
        if (isTokenValid) {
            UserDetails userDetails = (UserDetails) Arrays.stream(joinPoint.getArgs()).filter(arg -> arg.getClass().equals(DbUserDetails.class)).findFirst().orElseThrow(() -> new NotFoundException("UserDetails not found"));
            userJpaRepository.setLastActionDateForUser(LocalDateTime.now(), userDetails.getUsername());
        }
    }

    @After(value = "@annotation(ru.digitalgeneration.dgserver.business.annotations.UserProfileChange)")
    public void updateTimestampAfterProfileChange(JoinPoint joinPoint) {
        String username = AuthUtils.getCurrentJwtId();
        if (username.equals("anonymousUser")) {
            username = (String) Arrays.stream(joinPoint.getArgs()).filter(arg -> arg.getClass().equals(String.class)).findFirst().orElseThrow(() -> new NotFoundException("Username not found"));
        }
        userJpaRepository.setLastChangingProfileDateForUser(LocalDateTime.now(), username);
    }
}