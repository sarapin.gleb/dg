package ru.digitalgeneration.dgserver.business.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.security.core.Authentication;
import ru.digitalgeneration.dgserver.business.entities.User;
import ru.digitalgeneration.dgserver.security.config.DbUserDetails;
import ru.digitalgeneration.dgserver.security.logic.services.DbUserDetailsService;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class AuthUtils {

    public static String getCurrentJwtId() {
        return getAuthentication().getName();
    }

    public static User getCurrentUser() {
        if (getAuthentication().getPrincipal().equals("anonymousUser")) {
            return null;
        } else {
            return ((DbUserDetails) getAuthentication().getPrincipal()).getUser();
        }
    }

    private static Authentication getAuthentication() {
        return DbUserDetailsService.getAuthentication();
    }

    public static boolean isAccountVerified() {
        var user = getCurrentUser();
        if (user != null) {
            return user.getVkId() != null || user.getPhoneVerified();
        } else {
            return false;
        }
    }
}