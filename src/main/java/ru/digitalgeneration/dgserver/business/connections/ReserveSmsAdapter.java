package ru.digitalgeneration.dgserver.business.connections;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import ru.digitalgeneration.dgserver.business.exceptions.InsufficientFundsException;

@Component
public class ReserveSmsAdapter {

    @Value("${broker.reserve.urls.get_token_url}")
    private String getTokenUrl;
    @Value("${broker.reserve.urls.send_sms_url}")
    private String sendSmsUrl;

    @Value("${broker.reserve.credentials.grant_type}")
    private String grantType;
    @Value("${broker.reserve.credentials.client_id}")
    private String clientId;
    @Value("${broker.reserve.credentials.client_secret}")
    private String clientSecret;

    private String token = null;

    public void sendSms(String phone, String mes) throws UnirestException {
        if (token == null) {
            token = (String) getTokenForSmsBroker().getBody().getObject().get("access_token");
        }
        HttpResponse<JsonNode> response = sendSmsApi(phone, mes);
        int statusCode = response.getStatus();
        if (statusCode == 401) {
            token = (String) getTokenForSmsBroker().getBody().getObject().get("access_token");
            response = sendSmsApi(phone, mes);
            statusCode = response.getStatus();
        }
        if (statusCode == 422) {
            throw new InsufficientFundsException("Unprocessable Entity, possibly insufficient funds on the account of the SMS broker: " + response.getBody().getObject().toString());
        }
    }

    public HttpResponse<JsonNode> getTokenForSmsBroker() throws UnirestException {
        return Unirest.post(getTokenUrl)
                .header(HttpHeaders.CONTENT_TYPE, "application/x-www-form-urlencoded")
                .field("grant_type", grantType)
                .field("client_id", clientId)
                .field("client_secret", clientSecret)
                .asJson();
    }

    public HttpResponse<JsonNode> sendSmsApi(String phone, String mes) throws UnirestException {
        return Unirest.post(sendSmsUrl)
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + token)
                .header(HttpHeaders.CONTENT_TYPE, "application/x-www-form-urlencoded")
                .field("sender", "DGeneration")
                .field("phones", "[\"" + phone + "\"]")
                .field("body", mes)
                .field("transliterate", "0")
                .asJson();
    }
}
