package ru.digitalgeneration.dgserver.business.connections;

import com.mashape.unirest.http.exceptions.UnirestException;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Component;
import ru.digitalgeneration.dgserver.business.annotations.validation.PhoneConstraintValidator;
import ru.digitalgeneration.dgserver.business.exceptions.InvalidPhoneFormatException;

@Component
@EnableAsync
@RequiredArgsConstructor
public class BrokerConnection {

    private static final Logger log = LogManager.getLogger();
    private final PhoneConstraintValidator phoneConstraintValidator;
    private final MainSmsAdapter mainSmsAdapter;
    private final ReserveSmsAdapter reserveSmsAdapter;

    public void sendPhoneVerificationSms(String phone, String code) throws UnirestException {
        sendSms(phone,
                "Ваш код для подтверждения номера телефона: " + code,
                "Send SMS with code: " + phone);
    }

    public void sendPasswordRecoverySms(String phone, String code) throws UnirestException {
        sendSms(phone,
                "Никому не сообщайте код для восстановления пароля: " + code,
                "Send SMS with code: " + phone);
    }

    public void sendSuccessfulPointsPurchaseSms(String phone, Integer points) throws UnirestException {
        sendSms(phone,
                "Вы приобрели " + points + " баллов платформы Цифровое поколение",
                "Send successful points purchase SMS: " + phone);
    }

    public void sendPasswordChangeConfirmationSms(String phone) throws UnirestException {
        sendSms(phone,
                "Пароль от вашей учетной записи успешно заменен. Если вы не меняли свой пароль срочно напишите об этом нам по адресу digitalgenerationsupport@mail.ru",
                "Send password change confirmation SMS: " + phone);
    }

    public void sendNewCoursePurchasingSms(String phone, String courseTitle) throws UnirestException {
        sendSms(phone,
                "Вы приобрели курс " + courseTitle,
                "Send new course purchasing SMS: " + phone);
    }

    @Async
    void sendSms(String phone, String mes, String logMessage) throws UnirestException {
        if (!phoneConstraintValidator.phoneValidation(phone)) {
            throw new InvalidPhoneFormatException("The phone format is wrong");
        }
        String formedPhone = phone.replaceAll("[^\\d]", "");

        try {
            try {
                log.info(logMessage + ", main method start");
                mainSmsAdapter.sendSms(formedPhone, mes);
                log.info(logMessage + ", main SMS method completed");
            } catch (Exception ex) {
                log.warn(logMessage + ", main SMS method failed");
                reserveSmsAdapter.sendSms(formedPhone, mes);
                log.info(logMessage + ", reserve SMS method completed");
            }
        } catch (Exception ex) {
            log.error(logMessage + ", SMS send failed");
            throw ex;
        }
    }
}