package ru.digitalgeneration.dgserver.business.connections;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import ru.digitalgeneration.dgserver.business.exceptions.InsufficientFundsException;

@Component
public class MainSmsAdapter {

    @Value("${broker.main.urls.get_token_url}")
    private String getTokenUrl;
    @Value("${broker.main.urls.send_sms_url}")
    private String sendSmsUrl;

    @Value("${broker.main.credentials.user}")
    private String user;
    @Value("${broker.main.credentials.password}")
    private String password;

    private String token = null;

    public void sendSms(String phone, String mes) throws UnirestException {
        if (token == null) {
            token = (String) getTokenForSmsBroker().getBody().getObject().get("access_token");
        }
        HttpResponse<JsonNode> response = sendSmsApi(phone, mes);

        int statusCode = response.getStatus();
        switch (statusCode) {
            case 403:
                token = (String) getTokenForSmsBroker().getBody().getObject().get("access_token");
                sendSmsApi(phone, mes).getStatus();
                break;
            case 402:
                throw new InsufficientFundsException("Unprocessable Entity, possibly insufficient funds on the account of the SMS broker: " + response.getBody().getObject().toString());
            case 426:
                throw new RuntimeException("Only template messages allowed " + response.getBody().getObject().toString());
            case 500:
                throw new RuntimeException("Broker server failed " + response.getBody().getObject().toString());
        }
    }

    public HttpResponse<JsonNode> getTokenForSmsBroker() throws UnirestException {
        return Unirest.post(getTokenUrl)
                .header(HttpHeaders.CONTENT_TYPE, "application/x-www-form-urlencoded")
                .field("user", user)
                .field("pass", password)
                .asJson();
    }

    public HttpResponse<JsonNode> sendSmsApi(String phone, String mes) throws UnirestException {
        return Unirest.post(sendSmsUrl)
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + token)
                .header(HttpHeaders.CONTENT_TYPE, "application/x-www-form-urlencoded")
                .field("user", user)
                .field("pass", password)
                .field("to", phone)
                .field("txt", mes)
                .asJson();
    }

}
