package ru.digitalgeneration.dgserver.business.connections;

import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Component;
import org.thymeleaf.context.Context;
import ru.digitalgeneration.dgserver.business.services.MailService;

import javax.mail.MessagingException;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.UUID;

@Component
@EnableAsync
@RequiredArgsConstructor
public class MailServiceConnection {

    private final MailService mailService;

    private static final Logger log = LogManager.getLogger();

    @Value("${spring.mail.link-to-course}")
    private String linkToCourse;

    @Value("${spring.mail.admin-email}")
    private String adminEmail;

    @Value("${spring.mail.subjects.registration_letter}")
    private String registrationLetterSubject;
    @Value("${spring.mail.subjects.password_recovery}")
    private String passwordRecoverySubject;
    @Value("${spring.mail.subjects.successful_points_purchase}")
    private String successfulPointsPurchaseSubject;
    @Value("${spring.mail.subjects.new_course_purchasing}")
    private String newCoursePurchasingSubject;
    @Value("${spring.mail.subjects.password_change_confirmation}")
    private String passwordChangeConfirmationSubject;
    @Value("${spring.mail.subjects.email_confirmation}")
    private String emailConfirmationSubject;
    @Value("${spring.mail.subjects.new_task_complaint}")
    private String newTaskComplaintSubject;
    @Value("${spring.mail.subjects.new_solution_complaint}")
    private String newSolutionComplaintSubject;
    @Value("${spring.mail.subjects.task_completed}")
    private String taskCompletedSubject;
    @Value("${spring.mail.subjects.task_evaluated}")
    private String taskEvaluatedSubject;
    @Value("${spring.mail.subjects.task_not_done}")
    private String taskNotDoneSubject;

    @Value("${spring.mail.templates.registration_letter}")
    private String registrationLetterTemplate;
    @Value("${spring.mail.templates.password_recovery}")
    private String passwordRecoveryTemplate;
    @Value("${spring.mail.templates.successful_points_purchase}")
    private String successfulPointsPurchaseTemplate;
    @Value("${spring.mail.templates.new_course_purchasing}")
    private String newCoursePurchasingTemplate;
    @Value("${spring.mail.templates.password_change_confirmation}")
    private String passwordChangeConfirmationTemplate;
    @Value("${spring.mail.templates.email_confirmation}")
    private String emailConfirmationTemplate;
    @Value("${spring.mail.templates.task_completed}")
    private String taskCompletedTemplate;
    @Value("${spring.mail.templates.task_evaluated}")
    private String taskEvaluatedTemplate;

    @Async
    public void sendVerificationMail(String linkToConfirmationPage, String role, String jwt, UUID uuid, String email) throws MessagingException, UnsupportedEncodingException {
        String verificationUrl = linkToConfirmationPage + "/" + role + "/" + jwt + "/" + uuid;
        Context context = new Context();
        context.setVariable("verification_url", verificationUrl);
        try {
            log.info("Send verification mail: " + email + ", method start");
            mailService.sendHtmlMail(email, registrationLetterSubject, registrationLetterTemplate, context);
            log.info("Send verification mail: " + email + ", method completed");
        } catch (MessagingException e) {
            throw new MessagingException(e.getMessage() + " error when sending a verification email");
        }
    }

    @Async
    public void sendPasswordRecoveryEmail(String email, String passwordRecoveryUrl) throws MessagingException {
        Context context = new Context();
        context.setVariable("password_recovery_url", passwordRecoveryUrl);
        context.setVariable("email", email);
        try {
            log.info("Send password recovery mail: " + email + ", method start");
            mailService.sendHtmlMail(email, passwordRecoverySubject, passwordRecoveryTemplate, context);
            log.info("Send password recovery mail: " + email + ", method completed");
        } catch (MessagingException | UnsupportedEncodingException e) {
            throw new MessagingException(e.getMessage() + " error when sending a password recovery email");
        }
    }

    @Async
    public void sendSuccessfulPointsPurchaseMail(String email, Integer points) throws MessagingException {
        Context context = new Context();
        context.setVariable("purchased_points", points);
        context.setVariable("email", email);
        try {
            log.info("Send successful points purchase mail: " + email + ", method start");
            mailService.sendHtmlMail(email, successfulPointsPurchaseSubject, successfulPointsPurchaseTemplate, context);
            log.info("Send successful points purchase mail: " + email + ", method completed");
        } catch (MessagingException | UnsupportedEncodingException e) {
            throw new MessagingException(e.getMessage() + " error when sending an email about a successful purchase of points");
        }
    }

    @Async
    public void sendNewCoursePurchasingMail(String email, Long courseId, String courseTitle) throws MessagingException {
        Context context = new Context();
        context.setVariable("course_title", courseTitle);
        context.setVariable("link_to_course", linkToCourse + courseId);
        context.setVariable("email", email);
        try {
            log.info("Send new course purchasing mail: " + email + ", method start");
            mailService.sendHtmlMail(email, newCoursePurchasingSubject, newCoursePurchasingTemplate, context);
            log.info("Send new course purchasing mail: " + email + ", method completed");
        } catch (MessagingException | UnsupportedEncodingException e) {
            throw new MessagingException(e.getMessage() + " error when sending an email about purchase a new course");
        }
    }

    @Async
    public void sendPasswordChangeConfirmationMail(String email) throws MessagingException {
        Context context = new Context();
        context.setVariable("email", email);
        try {
            log.info("Send password change confirmation mail: " + email + ", method start");
            mailService.sendHtmlMail(email, passwordChangeConfirmationSubject, passwordChangeConfirmationTemplate, context);
            log.info("Send password change confirmation mail: " + email + ", method completed");
        } catch (MessagingException | UnsupportedEncodingException e) {
            throw new MessagingException(e.getMessage() + " error when sending a password change confirmation email");
        }
    }

    @Async
    public void sendVerificationMailFromPersonalArea(String email, String linkToConfirmationPage, UUID uuid) throws MessagingException {
        String verificationUrl = linkToConfirmationPage + "/" + uuid;
        Context context = new Context();
        context.setVariable("verification_url", verificationUrl);
        try {
            log.info("Send verification mail from personal area: " + email + ", method start");
            mailService.sendHtmlMail(email, emailConfirmationSubject, emailConfirmationTemplate, context);
            log.info("Send verification mail from personal area: " + email + ", method completed");
        } catch (MessagingException | UnsupportedEncodingException e) {
            throw new MessagingException(e.getMessage() + " error when sending a verification email");
        }
    }

    @Async
    public void sendTaskCompletedMail(Long taskId, String taskTitle, String email) throws MessagingException {
        Context context = new Context();
        context.setVariable("task_id", taskId);
        context.setVariable("task_title", taskTitle);
        context.setVariable("email", email);
        try {
            log.info("Send task completed mail, method start");
            mailService.sendHtmlMail(email, taskCompletedSubject, taskCompletedTemplate, context);
            log.info("Send task completed mail, method completed");
        } catch (MessagingException | UnsupportedEncodingException e) {
            throw new MessagingException(e.getMessage() + " error when sending a task completed email");
        }
    }

    @Async
    public void sendSolutionEvaluatedMail(String email, Long taskId, String taskTitle) throws MessagingException {
        Context context = new Context();
        context.setVariable("task_id", taskId);
        context.setVariable("task_title", taskTitle);
        context.setVariable("email", email);
        try {
            log.info("Send solution evaluated mail, method start");
            mailService.sendHtmlMail(email, taskEvaluatedSubject, taskEvaluatedTemplate, context);
            log.info("Send solution evaluated mail, method completed");
        } catch (MessagingException | UnsupportedEncodingException e) {
            throw new MessagingException(e.getMessage() + " error when sending a solution evaluated email");
        }
    }

    @Async
    public void sendTaskComplaintMailToAdmin(String studentJwtId, Long taskId, List<String> complaintTags, String value) throws MessagingException {
        String emailBody = String.format("Поступила жалоба на задачу.\n JwtId студента: %s\n ID задачи: %s\n Теги к жалобе: %s\n Текст жалобы: %s",
                studentJwtId,
                taskId,
                complaintTags.stream().reduce("", (partialString, tag) -> partialString + tag + " "),
                value
        );
        try {
            log.info("Send task complaint mail to admin, method start");
            mailService.sendTextMail(adminEmail, newTaskComplaintSubject, emailBody, null, null);
            log.info("Send task complaint mail to admin, method completed");
        } catch (MessagingException e) {
            throw new MessagingException(e.getMessage() + " error when sending a task complaint email");
        }
    }

    @Async
    public void sendSolutionComplaintMailToAdmin(String entrepreneurJwtId, Long solutionId, List<String> complaintTags, String value) throws MessagingException {
        String emailBody = String.format("Поступила жалоба на решение.\n JwtId предпринимателя: %s\n ID решения: %s\n Теги к жалобе: %s\n Текст жалобы: %s",
                entrepreneurJwtId,
                solutionId,
                complaintTags.stream().reduce("", (partialString, tag) -> partialString + tag + " "),
                value
        );
        try {
            log.info("Send solution complaint mail to admin, method start");
            mailService.sendTextMail(adminEmail, newSolutionComplaintSubject, emailBody, null, null);
            log.info("Send solution complaint mail to admin, method completed");
        } catch (MessagingException e) {
            throw new MessagingException(e.getMessage() + " error when sending a solution complaint email");
        }
    }

    @Async
    public void sendTaskNotDoneMail(Long taskId, String taskTitle, String sName, String fName, String mName, String email, String phone) throws MessagingException {
        String emailBody = String.format(
                "Задание “№%s %s” пользователя %s %s %s провалено.\n" +
                        "Контакты пользователя:\n" +
                        "Email: %s\n" +
                        "Телефон: %s",
                taskId, taskTitle, sName, fName, mName, email, phone
        );
        try {
            log.info("Send task not done mail to admin, method start");
            mailService.sendTextMail(adminEmail, taskNotDoneSubject, emailBody, null, null);
            log.info("Send task not done mail to admin, method completed");
        } catch (MessagingException e) {
            throw new MessagingException(e.getMessage() + " error when sending a task not done email");
        }
    }
}