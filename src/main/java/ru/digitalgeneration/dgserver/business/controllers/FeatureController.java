package ru.digitalgeneration.dgserver.business.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.digitalgeneration.dgserver.business.dto.responses.FeatureResponse;
import ru.digitalgeneration.dgserver.business.services.FeatureService;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/rest")
@Validated
@Tag(name = "Фичи")
public class FeatureController {

    private final FeatureService featureService;

    @Operation(summary = "Получение списка планируемых фич")
    @ApiResponse(responseCode = "200", description = "Successful operation",
            content = @Content(
                    array = @ArraySchema(
                            schema = @Schema(implementation = FeatureResponse.class))))
    @GetMapping(path = "/features")
    public List<FeatureResponse> getFeatureList() {
        return featureService.getFeatureList();
    }
}