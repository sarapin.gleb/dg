package ru.digitalgeneration.dgserver.business.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.digitalgeneration.dgserver.business.annotations.JwtAuthSwagger;
import ru.digitalgeneration.dgserver.business.dto.requests.GradeRequest;
import ru.digitalgeneration.dgserver.business.dto.requests.TaskRequest;
import ru.digitalgeneration.dgserver.business.dto.responses.*;
import ru.digitalgeneration.dgserver.business.services.TaskService;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/rest")
@Validated
@Tag(name = "Задачи")
public class TaskController {

    private final TaskService taskService;

    @Secured(value = {"ROLE_ENTREPRENEUR"})
    @JwtAuthSwagger
    @Operation(summary = "Создание задачи")
    @ApiResponse(responseCode = "200", description = "Successful operation",
            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = BalanceResponse.class)))
    @PostMapping(path = "/tasks")
    @ResponseStatus(HttpStatus.CREATED)
    public BalanceResponse createTask(@RequestPart @Valid TaskRequest taskRequest, @RequestPart MultipartFile[] files) {
        return taskService.createTask(taskRequest, files);
    }

    @Secured(value = {"ROLE_ENTREPRENEUR"})
    @JwtAuthSwagger
    @Operation(summary = "Получение списка поставленных задач")
    @ApiResponse(responseCode = "200", description = "Successful operation",
            content = @Content(
                    array = @ArraySchema(
                            schema = @Schema(implementation = TaskResponseForEntrepreneur.class))))
    @GetMapping(path = "/entrepreneurs/tasks")
    public List<TaskResponseForEntrepreneur> getTasksSetByEntrepreneur(@RequestParam(required = false) Integer amount) {
        return taskService.getTasksSetByEntrepreneur(amount);
    }

    @Secured(value = {"ROLE_STUDENT"})
    @JwtAuthSwagger
    @Operation(summary = "Получение списка решённых задач")
    @ApiResponse(responseCode = "200", description = "Successful operation",
            content = @Content(
                    array = @ArraySchema(
                            schema = @Schema(implementation = TaskResponseForStudent.class))))
    @GetMapping(path = "/students/tasks")
    public List<TaskResponseForStudent> getSolvedTasks() {
        return taskService.getSolvedTasks();
    }

    @Secured(value = {"ROLE_ENTREPRENEUR"})
    @JwtAuthSwagger
    @Operation(summary = "Отмена задачи предпринимателем")
    @PutMapping(path = "/tasks/{taskId}")
    public void cancelTask(@PathVariable @NotNull Long taskId) {
        taskService.cancelTask(taskId);
    }

    @Secured(value = {"ROLE_ENTREPRENEUR"})
    @JwtAuthSwagger
    @Operation(summary = "Создание оценки")
    @PostMapping(path = "/grades")
    @ResponseStatus(HttpStatus.CREATED)
    public void createGrade(@RequestBody @Valid GradeRequest gradeRequest) {
        taskService.createGrade(gradeRequest);
    }

    @Secured(value = {"ROLE_STUDENT"})
    @JwtAuthSwagger
    @Operation(summary = "Получение списка задач для слайдера")
    @ApiResponse(responseCode = "200", description = "Successful operation",
            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = SliderResponse.class)))
    @GetMapping(path = "/tasks/task-types/{taskTypeId}")
    public SliderResponse getTaskListForSlider(@PathVariable @NotNull Long taskTypeId, @RequestParam Long[] viewedTasks) {
        return taskService.getTaskListForSlider(taskTypeId, viewedTasks);
    }

    @Operation(summary = "Получение данных по типу задач для слайдера")
    @ApiResponse(responseCode = "200", description = "Successful operation",
            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = TaskTypeResponse.class)))
    @GetMapping(path = "/task-types/{taskTypeId}/slider")
    public TaskTypeResponse getTaskTypeResponseForSlider(@PathVariable @NotNull Long taskTypeId) {
        return taskService.getTaskTypeResponseForSlider(taskTypeId);
    }

    @Secured(value = {"ROLE_STUDENT"})
    @JwtAuthSwagger
    @Operation(summary = "Назначение задачи на студента")
    @ApiResponse(responseCode = "200", description = "Successful operation",
            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = TaskAssignmentResponse.class)))
    @PutMapping(path = "/tasks/{taskId}/assignment")
    public TaskAssignmentResponse taskAssignment(@PathVariable @NotNull Long taskId) throws IOException {
        return taskService.taskAssignment(taskId);
    }

    @Secured(value = {"ROLE_ENTREPRENEUR"})
    @JwtAuthSwagger
    @Operation(summary = "Получение предпринимателем условия задачи и списка решений")
    @ApiResponse(responseCode = "200", description = "Successful operation",
            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = TaskResponseWithSolutionList.class)))
    @GetMapping(path = "/tasks/{taskId}/solutions")
    public TaskResponseWithSolutionList getTaskWithSolutionList(@PathVariable @NotNull Long taskId) {
        return taskService.getTaskWithSolutionList(taskId);
    }

    @Secured(value = {"ROLE_STUDENT"})
    @JwtAuthSwagger
    @Operation(summary = "Получение данных по решаемой задаче")
    @ApiResponse(responseCode = "200", description = "Successful operation",
            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = CurrentTaskResponse.class)))
    @GetMapping(path = "/tasks/current")
    public CurrentTaskResponse getCurrentTask() {
        return taskService.getCurrentTask();
    }

    @Secured(value = {"ROLE_ENTREPRENEUR"})
    @JwtAuthSwagger
    @Operation(summary = "Выбор лучшего решения")
    @PostMapping(path = "/solutions/best/{solutionId}")
    @ResponseStatus(HttpStatus.CREATED)
    public void chooseBestSolution(@PathVariable @NotNull Long solutionId) {
        taskService.chooseBestSolution(solutionId);
    }
}