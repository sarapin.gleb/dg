package ru.digitalgeneration.dgserver.business.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ru.digitalgeneration.dgserver.business.annotations.JwtAuthSwagger;
import ru.digitalgeneration.dgserver.business.dto.requests.SolutionComplaintRequest;
import ru.digitalgeneration.dgserver.business.dto.requests.TaskComplaintRequest;
import ru.digitalgeneration.dgserver.business.services.ComplaintService;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping("/rest")
@Validated
@Tag(name = "Жалобы")
public class ComplaintController {

    private final ComplaintService complaintService;

    @Secured(value = {"ROLE_STUDENT"})
    @JwtAuthSwagger
    @Operation(summary = "Создание жалобы на задачу")
    @PostMapping(path = "/tasks/complaints")
    @ResponseStatus(HttpStatus.CREATED)
    public void createTaskComplaint(@RequestBody @Valid TaskComplaintRequest complaintRequest) {
        complaintService.createTaskComplaint(complaintRequest);
    }

    @Secured(value = {"ROLE_ENTREPRENEUR"})
    @JwtAuthSwagger
    @Operation(summary = "Создание жалобы на решение")
    @PostMapping(path = "/solutions/complaints")
    @ResponseStatus(HttpStatus.CREATED)
    public void createSolutionComplaint(@RequestBody @Valid SolutionComplaintRequest complaintRequest) {
        complaintService.createSolutionComplaint(complaintRequest);
    }
}