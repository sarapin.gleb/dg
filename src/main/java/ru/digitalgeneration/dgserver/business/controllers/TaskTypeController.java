package ru.digitalgeneration.dgserver.business.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.digitalgeneration.dgserver.business.annotations.JwtAuthSwagger;
import ru.digitalgeneration.dgserver.business.dto.responses.TaskTypeForEntrepreneurResponse;
import ru.digitalgeneration.dgserver.business.dto.responses.TaskTypeForStudentResponse;
import ru.digitalgeneration.dgserver.business.dto.responses.TaskTypeFormResponse;
import ru.digitalgeneration.dgserver.business.services.TaskTypeService;

import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/rest")
@Validated
@Tag(name = "Типы задач")
public class TaskTypeController {

    private final TaskTypeService taskTypeService;

    @Operation(summary = "Получение предпринимателем всех типов задач")
    @ApiResponse(responseCode = "200", description = "Successful operation",
            content = @Content(
                    array = @ArraySchema(
                            schema = @Schema(implementation = TaskTypeForEntrepreneurResponse.class))))
    @GetMapping(path = "/entrepreneurs/task-types")
    public List<TaskTypeForEntrepreneurResponse> getTaskTypeListByEntrepreneur() {
        return taskTypeService.getTaskTypeListByEntrepreneur();
    }

    @Operation(summary = "Получение студентом всех типов задач")
    @ApiResponse(responseCode = "200", description = "Successful operation",
            content = @Content(
                    array = @ArraySchema(
                            schema = @Schema(implementation = TaskTypeForStudentResponse.class))))
    @GetMapping(path = "/students/task-types")
    public List<TaskTypeForStudentResponse> getTaskTypeListByStudent() {
        return taskTypeService.getTaskTypeListByStudent();
    }

    @Secured(value = {"ROLE_ENTREPRENEUR"})
    @JwtAuthSwagger
    @Operation(summary = "Получение предпринимателем формы постановки задачи")
    @ApiResponse(responseCode = "200", description = "Successful operation",
            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = TaskTypeFormResponse.class)))
    @GetMapping(path = "/task-types/{taskTypeId}")
    public TaskTypeFormResponse getTaskTypeForm(@PathVariable @NotNull Long taskTypeId) {
        return taskTypeService.getTaskTypeForm(taskTypeId);
    }
}