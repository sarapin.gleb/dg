package ru.digitalgeneration.dgserver.business.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ru.digitalgeneration.dgserver.business.annotations.JwtAuthSwagger;
import ru.digitalgeneration.dgserver.business.dto.LeaderboardResponse;
import ru.digitalgeneration.dgserver.business.dto.requests.ChangeUserInfoRequest;
import ru.digitalgeneration.dgserver.business.dto.requests.UserCreationRequest;
import ru.digitalgeneration.dgserver.business.dto.responses.*;
import ru.digitalgeneration.dgserver.business.services.UserService;
import ru.digitalgeneration.dgserver.security.logic.dto.AuthResponse;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping("/rest")
@Validated
@Tag(name = "Пользователи")
public class UserController {

    private final UserService userService;

    @Operation(summary = "Добавление нового пользователя")
    @ApiResponse(responseCode = "201", description = "Created",
            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = AuthResponse.class)))
    @PostMapping(path = "/users")
    @ResponseStatus(HttpStatus.CREATED)
    public AuthResponse createUser(@RequestBody @Valid UserCreationRequest userCreationRequest) throws IOException {
        return userService.addUser(userCreationRequest);
    }

    @Secured(value = {"ROLE_STUDENT", "ROLE_ENTREPRENEUR"})
    @JwtAuthSwagger
    @Operation(summary = "Получение пользователем данных о себе")
    @ApiResponse(responseCode = "200", description = "Successful operation",
            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = UserResponse.class)))
    @GetMapping(path = "/users")
    public UserResponse getUserInfo() {
        return userService.getUserInfo();
    }

    @Secured(value = {"ROLE_STUDENT", "ROLE_ENTREPRENEUR"})
    @JwtAuthSwagger
    @Operation(summary = "Изменение данных пользователя")
    @ApiResponse(responseCode = "200", description = "Successful operation",
            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = VerificationDataResponse.class)))
    @PutMapping(path = "/users")
    public VerificationDataResponse changeUserInfo(@RequestBody ChangeUserInfoRequest changeUserInfoRequest) {
        return userService.changeUserInfo(changeUserInfoRequest);
    }

    @Operation(summary = "Верификация электронной почты пользователя")
    @PutMapping(path = "/users/verification")
    public void verifyUserEmail(@RequestParam @NotNull UUID uuid) throws IOException {
        userService.verifyUserEmailByUuid(uuid);
    }

    @Secured(value = {"ROLE_STUDENT"})
    @JwtAuthSwagger
    @Operation(summary = "Покупка курса")
    @ApiResponse(responseCode = "200", description = "Successful operation",
            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = BalanceResponse.class)))
    @PutMapping(path = "/courses/{courseId}/purchase")
    public BalanceResponse coursePurchase(@PathVariable @NotNull Long courseId) throws IOException {
        return userService.coursePurchase(courseId);
    }

    @Secured(value = {"ROLE_STUDENT"})
    @JwtAuthSwagger
    @Operation(summary = "Получение всех этапов курса")
    @ApiResponse(responseCode = "200", description = "Successful operation",
            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = CourseWithStagesResponse.class)))
    @GetMapping(path = "/courses/{courseId}/stages")
    public CourseWithStagesResponse getStagesInCourse(@PathVariable @NotNull Long courseId) {
        return userService.getStagesInCourse(courseId);
    }

    @Secured(value = {"ROLE_STUDENT", "ROLE_ENTREPRENEUR"})
    @JwtAuthSwagger
    @Operation(summary = "Получение баланса баллов")
    @ApiResponse(responseCode = "200", description = "Successful operation",
            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = BalanceResponse.class)))
    @GetMapping(path = "/balance")
    public BalanceResponse getBalance() {
        return userService.getBalance();
    }

    @Secured(value = {"ROLE_STUDENT", "ROLE_ENTREPRENEUR"})
    @JwtAuthSwagger
    @Operation(summary = "Отправка письма со ссылкой на верификацию email")
    @PostMapping(path = "/users/verification")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void sendVerificationEmail(@RequestParam @NotBlank String linkToConfirmationPage) {
        userService.sendVerificationEmail(linkToConfirmationPage);
    }

    @Secured(value = {"ROLE_STUDENT", "ROLE_ENTREPRENEUR"})
    @JwtAuthSwagger
    @Operation(summary = "Генерация и отправка кода для подтверждения телефона")
    @PutMapping(path = "/codes")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void generatePhoneVerificationCode() {
        userService.generatePhoneVerificationCode();
    }

    @Secured(value = {"ROLE_STUDENT", "ROLE_ENTREPRENEUR"})
    @JwtAuthSwagger
    @Operation(summary = "Сравнение кода, введенного пользователем, с сохраненным в базе")
    @ApiResponse(responseCode = "200", description = "Successful operation",
            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = CodeComparingForPhoneVerificationResponse.class)))
    @GetMapping(path = "/codes")
    public CodeComparingForPhoneVerificationResponse compareCodes(@RequestParam @NotBlank String code) throws IOException {
        return userService.compareCodes(code);
    }

    @Secured(value = {"ROLE_STUDENT"})
    @JwtAuthSwagger
    @Operation(summary = "Прохождение этапа пользователем")
    @PutMapping(path = "/users/stages/{stageId}")
    public void completeStage(@PathVariable @NotNull Long stageId) {
        userService.completeStage(stageId);
    }

    @Secured(value = {"ROLE_STUDENT"})
    @JwtAuthSwagger
    @Operation(summary = "Сохранение тайминга видео")
    @PutMapping(path = "/users/stages/{stageId}/timing/{time}")
    public void saveTiming(@PathVariable @NotNull Long stageId, @PathVariable @NotNull Integer time) {
        userService.saveTiming(stageId, time);
    }

    @Operation(summary = "Отписка от рассылки")
    @ApiResponse(responseCode = "200", description = "Successful operation",
            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = String.class)))
    @GetMapping(path = "/users/unsubscribe")
    public String cancelSubscription(@RequestParam @NotBlank String email) {
        return userService.cancelSubscription(email);
    }

    @Secured(value = {"ROLE_STUDENT"})
    @JwtAuthSwagger
    @Operation(summary = "Получение студентом данных о своей статистике")
    @ApiResponse(responseCode = "200", description = "Successful operation",
            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = StudentStatResponse.class)))
    @GetMapping(path = "/students/statistics")
    public StudentStatResponse getStudentStatistics() {
        return userService.getStudentStatistics();
    }

    @Secured(value = {"ROLE_ENTREPRENEUR"})
    @JwtAuthSwagger
    @Operation(summary = "Получение предпринимателем данных о своей статистике")
    @ApiResponse(responseCode = "200", description = "Successful operation",
            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = EntrepreneurStatResponse.class)))
    @GetMapping(path = "/entrepreneurs/statistics")
    public EntrepreneurStatResponse getEntrepreneurStatistics() {
        return userService.getEntrepreneurStatistics();
    }

    @Secured(value = {"ROLE_STUDENT"})
    @JwtAuthSwagger
    @Operation(summary = "Получение данных об открытых студенту типах задач и оцененных задачах")
    @ApiResponse(responseCode = "200", description = "Successful operation",
            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = EvaluatedTasksResponse.class)))
    @GetMapping(path = "/tasks/evaluated")
    public EvaluatedTasksResponse getEvaluatedTasks(@RequestParam Integer amount) {
        return userService.getEvaluatedTasks(amount);
    }

    @Secured(value = {"ROLE_STUDENT"})
    @JwtAuthSwagger
    @Operation(summary = "Получение списка незавершенных курсов")
    @ApiResponse(responseCode = "200", description = "Successful operation",
            content = @Content(
                    array = @ArraySchema(
                            schema = @Schema(implementation = CourseWithLastStageResponse.class))))
    @GetMapping(path = "/courses/incomplete")
    public List<CourseWithLastStageResponse> getIncompleteCourses() {
        return userService.getIncompleteCourses();
    }

    @Secured(value = {"ROLE_STUDENT"})
    @JwtAuthSwagger
    @Operation(summary = "Получение списка лучших решений")
    @ApiResponse(responseCode = "200", description = "Successful operation",
            content = @Content(
                    array = @ArraySchema(
                            schema = @Schema(implementation = BestSolutionResponse.class))))
    @GetMapping(path = "/solutions/best")
    public List<BestSolutionResponse> getBestSolutions(@RequestParam(required = false) Integer amount, @RequestParam(required = false) Integer userId) {
        return userService.getBestSolutions(amount, userId);
    }

    @Secured(value = {"ROLE_STUDENT", "ROLE_ENTREPRENEUR"})
    @JwtAuthSwagger
    @Operation(summary = "Получение данных о другом пользователе")
    @ApiResponse(responseCode = "200", description = "Successful operation",
            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = UserResponse.class)))
    @GetMapping(path = "/users/{userId}")
    public UserResponse getAnotherUserInfo(@PathVariable @NotBlank String userId) {
        return userService.getAnotherUserInfo(userId);
    }

    @Secured(value = {"ROLE_STUDENT", "ROLE_ENTREPRENEUR"})
    @JwtAuthSwagger
    @Operation(summary = "Получение лидерборда за всё время")
    @ApiResponse(responseCode = "200", description = "Successful operation",
            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = LeaderboardResponse.class)))
    @GetMapping(path = "/leaders")
    public LeaderboardResponse getLeaderboard(@RequestParam(required = false) Integer amount) {
        return userService.getLeaderboard(amount);
    }

    @Secured(value = {"ROLE_STUDENT", "ROLE_ENTREPRENEUR"})
    @JwtAuthSwagger
    @Operation(summary = "Получение лидерборда за месяц")
    @ApiResponse(responseCode = "200", description = "Successful operation",
            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = LeaderboardResponse.class)))
    @GetMapping(path = "/leaders/month")
    public LeaderboardResponse getMonthlyLeaderboard(@RequestParam(required = false) Integer amount) {
        return userService.getMonthlyLeaderboard(amount);
    }
}