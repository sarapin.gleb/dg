package ru.digitalgeneration.dgserver.business.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ru.digitalgeneration.dgserver.business.annotations.JwtAuthSwagger;
import ru.digitalgeneration.dgserver.business.dto.requests.UpdateTransactionRequest;
import ru.digitalgeneration.dgserver.business.dto.responses.BalanceResponse;
import ru.digitalgeneration.dgserver.business.dto.responses.PaymentUrlResponse;
import ru.digitalgeneration.dgserver.business.services.BillingService;

import javax.validation.constraints.NotNull;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.concurrent.ExecutionException;

@RestController
@RequiredArgsConstructor
@RequestMapping("/rest")
@Validated
@Tag(name = "Оплата")
public class BillingController {

    private final BillingService billingService;

    @Secured(value = {"ROLE_STUDENT", "ROLE_ENTREPRENEUR"})
    @JwtAuthSwagger
    @Operation(summary = "Получение от Tinkoff ссылки на форму оплаты")
    @ApiResponse(responseCode = "200", description = "Successful operation",
            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = PaymentUrlResponse.class)))
    @PostMapping(path = "/payment")
    public PaymentUrlResponse createTransaction(@RequestParam @NotNull Integer pointsAmount) throws ExecutionException, InterruptedException {
        return billingService.createTransaction(pointsAmount);
    }

    @Secured(value = {"ROLE_STUDENT", "ROLE_ENTREPRENEUR"})
    @JwtAuthSwagger
    @Operation(summary = "Актуализация количества баллов")
    @ApiResponse(responseCode = "200", description = "Successful operation",
            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = BalanceResponse.class)))
    @PutMapping(path = "/balance/update")
    public BalanceResponse updateBalance() {
        return billingService.updateBalance();
    }

    @Operation(summary = "Обновление статуса транзакции")
    @ApiResponse(responseCode = "200", description = "Successful operation",
            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = Response.class)))
    @PostMapping(path = "/transactions/update")
    public Response updateTransaction(@RequestBody UpdateTransactionRequest updateTransactionRequest) throws IOException {
        billingService.updateTransaction(updateTransactionRequest);
        return Response.status(200).entity("OK").build();
    }
}