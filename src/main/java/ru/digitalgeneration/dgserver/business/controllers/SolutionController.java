package ru.digitalgeneration.dgserver.business.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ru.digitalgeneration.dgserver.business.annotations.JwtAuthSwagger;
import ru.digitalgeneration.dgserver.business.dto.requests.SolutionRequest;
import ru.digitalgeneration.dgserver.business.dto.responses.SolutionResponseForEntrepreneur;
import ru.digitalgeneration.dgserver.business.dto.responses.SolutionResponseForStudent;
import ru.digitalgeneration.dgserver.business.services.SolutionService;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.IOException;

@RestController
@RequiredArgsConstructor
@RequestMapping("/rest")
@Validated
@Tag(name = "Решения")
public class SolutionController {

    private final SolutionService solutionService;

    @Secured(value = {"ROLE_STUDENT"})
    @JwtAuthSwagger
    @Operation(summary = "Просмотр студентом решения с заданием")
    @ApiResponse(responseCode = "200", description = "Successful operation",
            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = SolutionResponseForStudent.class)))
    @GetMapping(path = "/solutions/tasks/{taskId}")
    public SolutionResponseForStudent getSolutionByTaskId(@PathVariable @NotNull Long taskId, @RequestParam(required = false) String userId) {
        return solutionService.getSolutionByTaskId(taskId, userId);
    }

    @Secured(value = {"ROLE_STUDENT"})
    @JwtAuthSwagger
    @Operation(summary = "Отправка решения")
    @PutMapping(path = "/solutions")
    public void sendSolution(@RequestBody @Valid SolutionRequest solutionRequest) throws IOException {
        solutionService.sendSolution(solutionRequest);
    }

    @Secured(value = {"ROLE_ENTREPRENEUR"})
    @JwtAuthSwagger
    @Operation(summary = "Получение решения предпринимателем")
    @ApiResponse(responseCode = "200", description = "Successful operation",
            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = SolutionResponseForEntrepreneur.class)))
    @GetMapping(path = "/solutions/{solutionId}")
    public SolutionResponseForEntrepreneur getSolutionById(@PathVariable @NotNull Long solutionId) {
        return solutionService.getSolutionById(solutionId);
    }
}