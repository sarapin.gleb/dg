package ru.digitalgeneration.dgserver.business.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ru.digitalgeneration.dgserver.business.annotations.JwtAuthSwagger;
import ru.digitalgeneration.dgserver.business.dto.responses.*;
import ru.digitalgeneration.dgserver.business.services.CourseService;

import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/rest")
@Validated
@Tag(name = "Курсы")
public class CourseController {

    private final CourseService courseService;

    @Operation(summary = "Получение информации о курсе")
    @ApiResponse(responseCode = "200", description = "Successful operation",
            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = CourseResponse.class)))
    @GetMapping(path = "/courses/{courseId}")
    public CourseResponse getCourseById(@PathVariable @NotNull Long courseId) {
        return courseService.getCourseById(courseId);
    }

    @Secured(value = {"ROLE_STUDENT"})
    @JwtAuthSwagger
    @Operation(summary = "Получение страницы этапа")
    @ApiResponse(responseCode = "200", description = "Successful operation",
            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = StageResponse.class)))
    @GetMapping(path = "/stages/{stageId}")
    public StageResponse getStageById(@PathVariable @NotNull Long stageId) {
        return courseService.getStageById(stageId);
    }

    @Secured(value = {"ROLE_STUDENT"})
    @JwtAuthSwagger
    @Operation(summary = "Получение списка курсов, купленных пользователем, фильтрованного по тегам")
    @ApiResponse(responseCode = "200", description = "Successful operation",
            content = @Content(
                    array = @ArraySchema(
                            schema = @Schema(implementation = CourseResponseFilteredByTagsForUser.class))))
    @GetMapping(path = "/courses/users")
    public List<CourseResponseFilteredByTagsForUser> getUserCourseList(@RequestParam String[] tags) {
        return courseService.getUserCourseList(tags);
    }

    @Operation(summary = "Получение списка курсов, фильтрованного по тегам")
    @ApiResponse(responseCode = "200", description = "Successful operation",
            content = @Content(
                    array = @ArraySchema(
                            schema = @Schema(implementation = CourseResponseFilteredByTags.class))))
    @GetMapping(path = "/courses/filtered")
    public List<CourseResponseFilteredByTags> getCourseList(@RequestParam String[] tags) {
        return courseService.getCourseList(tags);
    }
    
    @Operation(summary = "Получение списка курсов для главной страницы")
    @ApiResponse(responseCode = "200", description = "Successful operation",
            content = @Content(
                    array = @ArraySchema(
                            schema = @Schema(implementation = CourseResponseForHomePage.class))))
    @GetMapping(path = "/courses")
    public List<CourseResponseForHomePage> getCourses() {
        return courseService.getCourses();
    }
}