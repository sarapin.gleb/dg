package ru.digitalgeneration.dgserver.business.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ru.digitalgeneration.dgserver.business.annotations.JwtAuthSwagger;
import ru.digitalgeneration.dgserver.business.dto.requests.FavoritePageRequest;
import ru.digitalgeneration.dgserver.business.services.FavoritePageService;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

@RestController
@RequiredArgsConstructor
@RequestMapping("/rest")
@Validated
@Tag(name = "Интересные страницы")
public class FavoritePageController {

    private final FavoritePageService favoritePageService;

    @Operation(summary = "Сохранение почты пользователя и страницы, которая была ему интересна")
    @PostMapping(path = "/favorite-pages")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void saveEmailAndUserFavoritePage(@RequestBody @Valid FavoritePageRequest favoritePageRequest) {
        favoritePageService.saveEmailAndUserFavoritePage(favoritePageRequest);
    }

    @Secured(value = {"ROLE_STUDENT", "ROLE_ENTREPRENEUR"})
    @JwtAuthSwagger
    @Operation(summary = "Сохранение почты авторизованного пользователя и страницы, которая была ему интересна")
    @PostMapping(path = "/favorite-pages/users")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void saveAuthorizedUserFavoritePage(@RequestParam @NotBlank String pageUrl) {
        favoritePageService.saveAuthorizedUserFavoritePage(pageUrl);
    }
}