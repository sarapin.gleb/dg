package ru.digitalgeneration.dgserver.business.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.digitalgeneration.dgserver.business.annotations.JwtAuthSwagger;
import ru.digitalgeneration.dgserver.business.dto.requests.RemoveFilesRequest;
import ru.digitalgeneration.dgserver.business.dto.responses.FileResponse;
import ru.digitalgeneration.dgserver.business.services.FileService;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/rest")
@Validated
@Tag(name = "Файлы")
public class FileController {

    private final FileService fileService;

    @Secured(value = {"ROLE_STUDENT"})
    @JwtAuthSwagger
    @Operation(summary = "Отправка студентом файлов к решению")
    @ApiResponse(responseCode = "201", description = "Created",
            content = @Content(
                    array = @ArraySchema(
                            schema = @Schema(implementation = FileResponse.class))))
    @PostMapping(path = "/files")
    @ResponseStatus(HttpStatus.CREATED)
    public List<FileResponse> addFilesToSolution(@RequestPart @NotNull Long taskId, @RequestPart MultipartFile[] files) {
        return fileService.addFilesToSolution(taskId, files);
    }

    @Secured(value = {"ROLE_STUDENT"})
    @JwtAuthSwagger
    @Operation(summary = "Удаление файлов")
    @DeleteMapping(path = "/files")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void removeFiles(@RequestBody @Valid RemoveFilesRequest removeFilesRequest) {
        fileService.removeFiles(removeFilesRequest);
    }
}