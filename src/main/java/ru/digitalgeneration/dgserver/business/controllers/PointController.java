package ru.digitalgeneration.dgserver.business.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.digitalgeneration.dgserver.business.dto.responses.PointPriceResponse;
import ru.digitalgeneration.dgserver.business.services.PointService;

@RestController
@RequiredArgsConstructor
@RequestMapping("/rest")
@Tag(name = "Баллы")
public class PointController {

    private final PointService pointService;

    @Operation(summary = "Получение стоимости балла")
    @ApiResponse(responseCode = "200", description = "Successful operation",
            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = PointPriceResponse.class)))
    @GetMapping(path = "/point-price")
    public PointPriceResponse getPointPrice() {
        return pointService.getPointPrice();
    }
}