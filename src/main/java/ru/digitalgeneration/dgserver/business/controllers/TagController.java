package ru.digitalgeneration.dgserver.business.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.digitalgeneration.dgserver.business.annotations.JwtAuthSwagger;
import ru.digitalgeneration.dgserver.business.dto.responses.GradeTagResponse;
import ru.digitalgeneration.dgserver.business.dto.responses.TagComplaintResponse;
import ru.digitalgeneration.dgserver.business.services.TagService;

import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/rest")
@Validated
@Tag(name = "Теги")
public class TagController {

    private final TagService tagService;

    @Operation(summary = "Получение списка тегов к курсам")
    @ApiResponse(responseCode = "200", description = "Successful operation")
    @GetMapping(path = "/tags")
    public List<String> getTagList() {
        return tagService.getTagList();
    }

    @Secured(value = {"ROLE_STUDENT"})
    @JwtAuthSwagger
    @Operation(summary = "Получение тегов к жалобам на задачи")
    @ApiResponse(responseCode = "200", description = "Successful operation",
            content = @Content(
                    array = @ArraySchema(
                            schema = @Schema(implementation = TagComplaintResponse.class))))
    @GetMapping(path = "/tasks/complaints/tags")
    public List<TagComplaintResponse> getTaskComplaintTags() {
        return tagService.getTaskComplaintTags();
    }

    @Secured(value = {"ROLE_ENTREPRENEUR"})
    @JwtAuthSwagger
    @Operation(summary = "Получение тегов к жалобам на решение")
    @ApiResponse(responseCode = "200", description = "Successful operation",
            content = @Content(
                    array = @ArraySchema(
                            schema = @Schema(implementation = TagComplaintResponse.class))))
    @GetMapping(path = "/solutions/complaints/tags")
    public List<TagComplaintResponse> getSolutionComplaintTags() {
        return tagService.getSolutionComplaintTags();
    }

    @Secured(value = {"ROLE_ENTREPRENEUR"})
    @JwtAuthSwagger
    @Operation(summary = "Получение тегов к оценкам")
    @ApiResponse(responseCode = "200", description = "Successful operation",
            content = @Content(
                    array = @ArraySchema(
                            schema = @Schema(implementation = GradeTagResponse.class))))
    @GetMapping(path = "/grades/tags")
    public List<GradeTagResponse> getGradeTags(@RequestParam @NotNull Long solutionId) {
        return tagService.getGradeTags(solutionId);
    }
}