alter table task_types
    drop column intermediate_page_id;

alter table intermediate_pages
    add column task_type bigint
        constraint intermediate_pages_task_type_fk
            references task_types
            on update cascade;

comment on column intermediate_pages.task_type is 'Тип задач, который открывает данная промежуточная страница';

alter table congratulation_pages
    add column task_type bigint
        constraint congratulation_pages_task_type_fk
            references task_types
            on update cascade;

comment on column congratulation_pages.task_type is 'Тип задач, который открывает данная поздравительная страница';