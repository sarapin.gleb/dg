alter table grades
    drop column task_types_grade_tags_id;

create table if not exists grades_task_types_grade_tags
(
    grade_id                               bigint                   not null,
    task_types_grade_tag_id                bigint                   not null,
    primary key (grade_id, task_types_grade_tag_id),
    constraint grades_task_types_grade_tags_grade_id_fk
        foreign key (grade_id) references grades
            on update cascade on delete cascade,
    constraint grades_task_types_grade_tags_task_types_grade_tag_id_fk
        foreign key (task_types_grade_tag_id) references task_types_grade_tags
            on update cascade on delete cascade
);

comment on table grades_task_types_grade_tags is 'Таблица связи оценок и тегов к оценкам для оцениваемого типа задач';