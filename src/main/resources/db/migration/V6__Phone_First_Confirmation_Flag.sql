alter table users
    add column phone_first_confirmation boolean not null default false;

comment on column users.phone_first_confirmation is 'Отметка о том, был ли когда-то ранее подтвержден телефон';