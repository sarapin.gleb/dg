alter table users
    add column jwt_id varchar(254) not null default 0,
    alter column email drop not null;

create unique index users_phone_uindex
    on users (phone);
create unique index users_jwt_id_uindex
    on users (jwt_id);

comment on column users.jwt_id is 'Уникальный идентификатор, предназначенный для кодирования в jwt';