alter table users
    drop column viewed_task_id,
    drop column task_viewing_start_time;

create table if not exists students_viewed_tasks
(
    id                                       bigserial,
    student_id                               bigint                   not null,
    viewed_task_id                           bigint                   not null,
    task_viewing_start_time                  timestamp with time zone not null,
    primary key (id),
    constraint students_viewed_tasks_student_id_fk
        foreign key (student_id) references users
            on update cascade on delete cascade,
    constraint students_viewed_tasks_viewed_task_id_fk
        foreign key (viewed_task_id) references tasks
            on update cascade on delete cascade
);

comment on table students_viewed_tasks is 'Таблица связи студентов и задач, которые они просматривают';

comment on column students_viewed_tasks.id is 'ID записа в таблице';

comment on column students_viewed_tasks.student_id is 'ID студента';

comment on column students_viewed_tasks.viewed_task_id is 'ID просматриваемой задачи';

comment on column students_viewed_tasks.task_viewing_start_time is 'Время начала просмотра задачи';