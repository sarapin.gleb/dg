alter table users
    alter column id set data type bigint,
    alter column uuid type uuid using uuid::uuid;

alter table images
    alter column id set data type bigint;

alter table courses
    alter column id set data type bigint;

alter table tags
    alter column id set data type bigint;

alter table stages
    alter column id set data type bigint;

alter table lectures
    alter column id set data type bigint;

alter table tests
    alter column id set data type bigint;

alter table intermediate_pages
    alter column id set data type bigint;

alter table congratulation_pages
    alter column id set data type bigint;

alter table students_courses
    alter column id set data type bigint;

alter table favorite_pages
    alter column id set data type bigint;

alter table features
    alter column id set data type bigint;

alter table transactions
    alter column id set data type bigint;