create table if not exists task_values
(
    id                                       bigserial,
    value                                    text                     not null,
    primary key (id)
);

comment on table task_values is 'Ценности задач';

comment on column task_values.id is 'ID ценности задачи';

comment on column task_values.value is 'Ценность задачи';

create table if not exists task_types
(
    id                                       bigserial,
    test_id                                  bigint,
    intermediate_page_id                     bigint,
    task_value_id                            bigint,
    image_id                                 bigint,
    title                                    varchar(255)             not null,
    brief_description                        text                     not null,
    formulation                              jsonb                    not null,
    solution                                 jsonb                    not null,
    reward                                   integer                  not null,
    price                                    integer                  not null,
    solution_time                            interval                 not null,
    primary key (id),
    constraint task_types_test_id_fk
        foreign key (test_id) references tests
            on update cascade,
    constraint task_types_intermediate_page_id_fk
        foreign key (intermediate_page_id) references intermediate_pages
            on update cascade,
    constraint task_types_task_value_id_fk
        foreign key (task_value_id) references task_values
            on update cascade,
    constraint task_types_image_id_fk
        foreign key (image_id) references images
            on update cascade
);

comment on table task_types is 'Типы задач';

comment on column task_types.id is 'ID типа задач';

comment on column task_types.test_id is 'ID теста, разблокирующего данный тип задач';

comment on column task_types.intermediate_page_id is 'ID промежуточной страницы, ведущей к данному типу задач';

comment on column task_types.task_value_id is 'ID ценности задачи, соответствующей данному типу задач';

comment on column task_types.title is 'Название типа задач';

comment on column task_types.brief_description is 'Краткое описание';

comment on column task_types.formulation is 'Структура постановки задачи';

comment on column task_types.solution is 'Структура решения';

comment on column task_types.reward is 'Вознаграждение для студента';

comment on column task_types.price is 'Стоимость для предпринимателя';

comment on column task_types.solution_time is 'Время на решение';

create table if not exists students_task_types
(
    student_id                               bigint                   not null,
    task_type_id                             bigint                   not null,
    primary key (student_id, task_type_id),
    constraint students_task_types_student_id_fk
        foreign key (student_id) references users
            on update cascade on delete cascade,
    constraint students_task_types_task_type_id_fk
        foreign key (task_type_id) references task_types
            on update cascade on delete cascade
);

comment on table students_task_types is 'Таблица связи студентов и открытых для них типов задач';

create table if not exists grade_tags
(
    id                                       bigserial,
    value                                    varchar(100)             not null,
    primary key (id)
);

comment on table grade_tags is 'Тэги к типам задач';

comment on column grade_tags.id is 'ID тэга к типу задач';

comment on column grade_tags.value is 'Значение тега';

create table if not exists task_types_grade_tags
(
    id                                       bigserial,
    task_type_id                             bigint                   not null,
    grade_tag_id                             bigint                   not null,
    comment                                  varchar(255)             not null,
    primary key (id),
    constraint task_types_grade_tags_task_type_id_fk
        foreign key (task_type_id) references task_types
            on update cascade on delete cascade,
    constraint task_types_grade_tags_grade_tag_id_fk
        foreign key (grade_tag_id) references grade_tags
            on update cascade on delete cascade
);

comment on table task_types_grade_tags is 'Тэги для конкретных типов задач';

comment on column task_types_grade_tags.id is 'Идентификатор тэга для конкретного типа задач';

comment on column task_types_grade_tags.task_type_id is 'ID типа задач';

comment on column task_types_grade_tags.grade_tag_id is 'ID тэга к типу задач';

comment on column task_types_grade_tags.comment is 'Комментарий к тэгу для данного типа задач';

create table if not exists grades
(
    id                                       bigserial,
    score                                    integer                  not null,
    feedback                                 text,
    task_types_grade_tags_id                 bigint,
    primary key (id),
    constraint grades_task_types_grade_tags_id_fk
        foreign key (task_types_grade_tags_id) references task_types_grade_tags
            on update cascade
);

comment on table grades is 'Оценки';

comment on column grades.id is 'ID оценки';

comment on column grades.score is 'Балл за решение';

comment on column grades.feedback is 'Отзыв на решение';

comment on column grades.task_types_grade_tags_id is 'ID тэга, который заслужил студент, решив задачу';

create table if not exists tasks
(
    id                                       bigserial,
    entrepreneur_id                          bigint,
    task_type_id                             bigint,
    formulation                              jsonb                    not null,
    execution_status                         varchar(25)              not null,
    creation_date                            timestamp with time zone not null default now(),
    deadline                                 timestamp with time zone not null,
    priority                                 integer,
    decisions_required_number                integer                  not null,
    draft                                    boolean                  not null,
    primary key (id),
    constraint tasks_entrepreneur_id_fk
        foreign key (entrepreneur_id) references users
            on update cascade,
    constraint tasks_task_type_id_fk
        foreign key (task_type_id) references task_types
            on update cascade
);

comment on table tasks is 'Задачи';

comment on column tasks.id is 'ID задачи';

comment on column tasks.entrepreneur_id is 'ID предпринимателя в таблице users';

comment on column tasks.task_type_id is 'ID типа задачи';

comment on column tasks.formulation is 'Постановка задачи';

comment on column tasks.execution_status is 'Статус выполнения';

comment on column tasks.creation_date is 'Время создания задачи';

comment on column tasks.deadline is 'Крайний срок выполнения задачи';

comment on column tasks.priority is 'Приоритет';

comment on column tasks.decisions_required_number is 'Количество необходимых решений';

comment on column tasks.draft is 'Является ли черновиком';

create table if not exists task_complaints
(
    id                                       bigserial,
    student_id                               bigint                   not null,
    task_id                                  bigint                   not null,
    value                                    text                     not null,
    primary key (id),
    constraint task_complaints_student_id_fk
        foreign key (student_id) references users
            on update cascade on delete cascade,
    constraint task_complaints_task_id_fk
        foreign key (task_id) references tasks
            on update cascade on delete cascade
);

comment on table task_complaints is 'Жалобы на задачи';

comment on column task_complaints.id is 'ID жалобы';

comment on column task_complaints.student_id is 'ID студента';

comment on column task_complaints.task_id is 'ID задачи';

comment on column task_complaints.value is 'Жалоба';

create table if not exists task_complaint_tags
(
    id                                       bigserial,
    value                                    varchar(100)             not null,
    primary key (id)
);

comment on table task_complaint_tags is 'Тэги жалоб на задачи';

comment on column task_complaint_tags.id is 'ID тэга';

comment on column task_complaint_tags.value is 'Значение тега';

create table if not exists task_complaints_task_complaint_tags
(
    task_complaint_id                        bigint                   not null,
    task_complaint_tag_id                    bigint                   not null,
    primary key (task_complaint_id, task_complaint_tag_id),
    constraint task_complaints_task_complaint_tags_task_complaint_id_fk
        foreign key (task_complaint_id) references task_complaints
            on update cascade on delete cascade,
    constraint task_complaints_task_complaint_tags_task_complaint_tag_id_fk
        foreign key (task_complaint_tag_id) references task_complaint_tags
            on update cascade on delete cascade
);

comment on table task_complaints_task_complaint_tags is 'Таблица связи жалоб на задачи и тэгов для жалоб на задачи';

create table if not exists solutions
(
    id                                       bigserial,
    student_id                               bigint,
    task_id                                  bigint                   not null,
    value                                    jsonb                    not null,
    receipt_date                             timestamp with time zone,
    delivery_date                            timestamp with time zone,
    draft                                    boolean                  not null,
    grade_id                                 bigint,
    primary key (id),
    constraint solutions_student_id_fk
        foreign key (student_id) references users
            on update cascade,
    constraint solutions_task_id_fk
        foreign key (task_id) references tasks
            on update cascade on delete cascade,
    constraint solutions_grade_id_fk
        foreign key (grade_id) references grades
            on update cascade
);

comment on table solutions is 'Решения';

comment on column solutions.id is 'ID решения';

comment on column solutions.student_id is 'ID студента';

comment on column solutions.task_id is 'ID задачи';

comment on column solutions.value is 'Решение';

comment on column solutions.receipt_date is 'Время получения задачи';

comment on column solutions.delivery_date is 'Время сдачи задачи';

comment on column solutions.draft is 'Является ли черновиком';

comment on column solutions.grade_id is 'ID оценки';

create table if not exists files
(
    id                                       bigserial,
    file_path                                varchar(2048)            not null,
    file_name                                varchar(255)             not null,
    order_number                             integer                  not null,
    task_id                                  bigint,
    solution_id                              bigint,
    primary key (id),
    constraint files_task_id_fk
        foreign key (task_id) references tasks
            on update cascade on delete cascade,
    constraint files_solution_id_fk
        foreign key (solution_id) references solutions
            on update cascade on delete cascade
);

comment on table files is 'Прикрепленные файлы';

comment on column files.id is 'ID файла';

comment on column files.file_path is 'Ссылка на файл';

comment on column files.file_name is 'Название файла';

comment on column files.order_number is 'Порядковый номер';

comment on column files.task_id is 'ID задачи, к которой относится файл';

comment on column files.solution_id is 'ID решения, к которому относится файл';

create table if not exists solution_complaints
(
    id                                       bigserial,
    solution_id                              bigint                   not null,
    value                                    text                     not null,
    primary key (id),
    constraint solution_complaints_solution_id_fk
        foreign key (solution_id) references solutions
            on update cascade on delete cascade
);

comment on table solution_complaints is 'Жалобы на решения';

comment on column solution_complaints.id is 'ID жалобы';

comment on column solution_complaints.solution_id is 'ID решения';

comment on column solution_complaints.value is 'Жалоба';

create table if not exists solution_complaint_tags
(
    id                                       bigserial,
    value                                    varchar(100)             not null,
    primary key (id)
);

comment on table solution_complaint_tags is 'Тэги жалоб на решения';

comment on column solution_complaint_tags.id is 'ID тэга';

comment on column solution_complaint_tags.value is 'Значение тега';

create table if not exists solution_complaints_solution_complaint_tags
(
    solution_complaint_id                    bigint                   not null,
    solution_complaint_tag_id                bigint                   not null,
    primary key (solution_complaint_id, solution_complaint_tag_id),
    constraint s_complaints_s_complaint_tags_s_complaint_id_fk
        foreign key (solution_complaint_id) references solution_complaints
            on update cascade on delete cascade,
    constraint s_complaints_s_complaint_tags_s_complaint_tag_id_fk
        foreign key (solution_complaint_tag_id) references solution_complaint_tags
            on update cascade on delete cascade
);

comment on table solution_complaints_solution_complaint_tags is 'Таблица связи жалоб на решения и тэгов для жалоб на решения';

alter table users
    add column viewed_task_id          bigint
        constraint users_viewed_task_id_fk
            references tasks
            on update cascade,
    add column task_viewing_start_time timestamp with time zone;

comment on column users.viewed_task_id is 'ID просматриваемой задачи';

comment on column users.task_viewing_start_time is 'Время начала просмотра задачи';

comment on table users is 'Пользователи';

comment on table images is 'Изображения';

comment on table courses is 'Курсы';

comment on table tags is 'Тэги к курсам';

comment on table courses_tags is 'Таблица связи тэгов и курсов';

comment on table stages is 'Этапы';

comment on table lectures is 'Лекции';

comment on table tests is 'Тесты';

comment on table intermediate_pages is 'Промежуточные страницы';

comment on table congratulation_pages is 'Поздравительные страницы';

comment on table students_courses is 'Таблица связи студентов и курсов';

comment on table favorite_pages is 'Интересующие страницы';

comment on table features is 'Фичи';

comment on table transactions is 'Транзакции';