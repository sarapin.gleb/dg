alter table solutions
    add column complaint_id bigint
        constraint solutions_complaint_id_fk
            references solution_complaints
            on update cascade;

comment on column solutions.complaint_id is 'ID жалобы на решение';

alter table solution_complaints
    drop column solution_id;