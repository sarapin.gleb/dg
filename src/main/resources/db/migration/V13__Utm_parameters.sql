create table if not exists utm_parameters
(
    id                                       bigserial,
    utm_source                               varchar(254),
    utm_medium                               varchar(254),
    utm_campaign                             varchar(254),
    utm_term                                 varchar(254),
    utm_content                              varchar(254),
    primary key (id)
);

comment on table utm_parameters is 'UTM-метки пользователей';

comment on column utm_parameters.utm_source is 'Название рекламной площадки';

comment on column utm_parameters.utm_medium is 'Рекламная модель';

comment on column utm_parameters.utm_campaign is 'Название рекламной кампании';

comment on column utm_parameters.utm_term is 'Ключевое слово, с которого начался показ объявления';

comment on column utm_parameters.utm_content is 'Дополнительная информация, которую можно отслеживать, если совпадают другие параметры';

alter table users
    add column utm_parameters_id bigint
        constraint users_utm_parameters_id_fk
            references utm_parameters
            on update cascade on delete set null;

comment on column users.utm_parameters_id is 'ID набора UTM-меток';