create table if not exists best_solutions
(
    id                                       bigserial,
    student_id                               bigint                   not null,
    solution_id                              bigint                   not null,
    solution_date                            timestamp with time zone not null default now(),
    primary key (id),
    constraint best_solutions_student_id_fk
        foreign key (student_id) references users
            on update cascade on delete cascade,
    constraint best_solutions_solution_id_fk
        foreign key (solution_id) references solutions
            on update cascade on delete cascade
);

comment on table best_solutions is 'Лучшие решения';

comment on column best_solutions.id is 'ID лучшего решения';

comment on column best_solutions.student_id is 'ID студента';

comment on column best_solutions.student_id is 'ID решения';

comment on column best_solutions.solution_date is 'Дата и время присвоения решению статуса лучшего';

alter table tasks
    add column best_solution_id bigint
        constraint tasks_best_solution_id_fk
            references best_solutions,
    add column special_task boolean not null default false;

comment on column tasks.best_solution_id is 'ID лучшего решения';

comment on column tasks.special_task is 'Удовлетворяет ли задание всем критериям "специального":
1) Заказано не менее 2 решений;
2) Задача создана реальным предпринимателем;
3) Предприниматель оплатил решения баллами, купленными только за реальные деньги, а не полученными с помощью бонусной программы.';

alter table users
    add column admin boolean not null default true;

comment on column users.admin is 'Принадлежит ли аккаунт администратору';