drop table if exists students_viewed_tasks;

alter table tasks
    alter column priority set data type double precision;