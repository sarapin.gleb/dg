alter table users
    add column vk_id integer,
    alter column password drop not null;

create unique index users_vk_id_uindex
    on users (vk_id);

comment on column users.vk_id is 'ID пользователя в VK';