create table if not exists users
(
    id                                       serial
        constraint users_pk
            primary key,
    registration_date                        timestamp with time zone,
    f_name                                   varchar(50),
    s_name                                   varchar(50),
    m_name                                   varchar(50),
    email                                    varchar(254)             not null,
    email_verified                           boolean,
    phone                                    varchar,
    phone_verified                           boolean,
    password                                 varchar(100)             not null,
    last_changing_profile_date               timestamp with time zone,
    roles                                    jsonb                    not null,
    last_points_purchase_date                timestamp with time zone,
    point_balance                            integer                  not null,
    permanently_blocked                      boolean                  not null,
    unlock_date                              timestamp with time zone,
    region                                   varchar(60),
    uuid                                     varchar(36)              not null,
    last_login_date                          timestamp with time zone,
    last_action_date                         timestamp with time zone,
    last_nonce_verification_code             varchar(10)
);

comment on column users.id is 'ID пользователя в БД';

comment on column users.registration_date is 'Дата и время регистрации пользователя';

comment on column users.f_name is 'Имя';

comment on column users.s_name is 'Фамилия';

comment on column users.m_name is 'Отчество';

comment on column users.email is 'Электронная почта';

comment on column users.email_verified is 'Подтверждён ли email пользователя. Меняет значение false на true, когда пользователь подтверждает почту';

comment on column users.phone is 'Номер телефона в виде строки, состоящей исключительно из цифр (опционально плюса перед ними)';

comment on column users.phone_verified is 'Подтверждён ли телефон пользователя. Меняет значение false на true, когда пользователь подтверждает телефон';

comment on column users.password is 'Хэшированный пароль';

comment on column users.last_changing_profile_date is 'Дата и время последнего изменения профиля';

comment on column users.roles is 'Роли пользователя, массив, состоящий из 0 или более enum:
1) Студент
2) Предприниматель';

comment on column users.last_points_purchase_date is 'Дата и время последней покупки баллов';

comment on column users.point_balance is 'Баланс баллов';

comment on column users.permanently_blocked is 'Заблокирован ли аккаунт навсегда';


comment on column users.unlock_date is 'Дата и время разблокировки пользователя из временного бана';

comment on column users.region is 'Регион нахождения, из raw.githubusercontent.com/hflabs/region/master/region.csv';

comment on column users.uuid is 'ID пользователя (UUID 4, генерируется в приложении)';

comment on column users.last_login_date is 'Дата и время входа в аккаунт';

comment on column users.last_action_date is 'Дата и время последнего действия';

comment on column users.last_nonce_verification_code is 'Последний полученный одноразовый код верификации телефона';

create unique index users_email_uindex
    on users (email);

create table if not exists images
(
    id                                       serial
        constraint images_pk
            primary key,
    image_path                               varchar(255)             not null,
    image_file_name                          varchar(255)             not null
);

comment on column images.id is 'ID изображения в БД';

comment on column images.image_path is 'Ссылка на изображение';

comment on column images.image_file_name is 'Название изображения';

create table if not exists courses
(
    id                                       serial
        constraint courses_pk
            primary key,
    title                                    varchar(254)             not null,
    brief_description                        text                     not null,
    image_id                                 bigint
        constraint courses_image_id_fk
            references images
            on update cascade on delete set null,
    intro_video                              varchar(254)             not null,
    author                                   varchar(254)             not null,
    duration                                 varchar(100)             not null,
    received_skills                          varchar(254)             not null,
    detailed_description                     text                     not null,
    price                                    integer                  not null
);

comment on column courses.id is 'ID курса в БД';

comment on column courses.title is 'Название курса';

comment on column courses.brief_description is 'Краткое описание';

comment on column courses.image_id is 'Изображение';

comment on column courses.intro_video is 'Вводный видеоролик';

comment on column courses.author is 'Автор курса';

comment on column courses.duration is 'Продолжительность курса';

comment on column courses.received_skills is 'Получаемые навыки';

comment on column courses.detailed_description is 'Описание программы курса';

comment on column courses.price is 'Стоимость курса';

create table if not exists tags
(
    id                                       serial
        constraint tags_pk
            primary key,
    value                                    varchar(100)             not null
);

comment on column tags.id is 'ID тега в БД';

comment on column tags.value is 'Значение тега';

create table if not exists courses_tags
(
    course_id                                bigint
        constraint courses_tags_course_id_fk
            references courses
            on update cascade on delete cascade,
    tag_id                                   bigint
        constraint courses_tags_tag_id_fk
            references tags
            on update cascade on delete cascade,
    primary key (course_id, tag_id)
);

create table if not exists stages
(
    id                                       serial
        constraint stages_pk
            primary key,
    title                                    varchar(255)             not null,
    order_number                             integer                  not null,
    description                              text                     not null,
    duration                                 varchar(100)             not null,
    course_id                                bigint
        constraint stages_course_id_fk
        references courses
            on update cascade on delete cascade,
    prev_stage_id                            bigint,
    next_stage_id                            bigint
);

comment on column stages.id is 'ID этапа в БД';

comment on column stages.title is 'Название этапа';

comment on column stages.order_number is 'Порядковый номер этапа в курсе';

comment on column stages.description is 'Описание';

comment on column stages.duration is 'Примерное время прохождения этапа';

comment on column stages.prev_stage_id is 'ID предыдущего этапа';

comment on column stages.next_stage_id is 'ID следующего этапа';

create table if not exists lectures
(
    id                                       serial
        constraint lectures_pk
            primary key,
    stage_id                                 bigint
        constraint lectures_stage_id_fk
            references stages
            on update cascade on delete cascade,
    video                                    varchar(255)             not null
);

comment on column lectures.id is 'ID лекции в БД';

comment on column lectures.video is 'Ссылка на видео';

create table if not exists tests
(
    id                                       serial
        constraint tests_pk
            primary key,
    stage_id                                 bigint
        constraint tests_stage_id_fk
            references stages
            on update cascade on delete cascade,
    questions                                text                     not null
);

comment on column tests.id is 'ID теста в БД';

comment on column tests.questions is 'Список вопросов в формате JSON';

create table if not exists intermediate_pages
(
    id                                       serial
        constraint intermediate_pages_pk
            primary key,
    stage_id                                 bigint
        constraint intermediate_pages_stage_id_fk
            references stages
            on update cascade on delete cascade,
    text                                     text                     not null
);

comment on column intermediate_pages.id is 'ID промежуточной страницы в БД';

comment on column intermediate_pages.text is 'Текст';

create table if not exists congratulation_pages
(
    id                                       serial
        constraint congratulation_pages_pk
            primary key,
    stage_id                                 bigint
        constraint congratulation_pages_stage_id_fk
            references stages
            on update cascade on delete cascade,
    text                                     text                     not null
);

comment on column congratulation_pages.id is 'ID поздравительной страницы в БД';

comment on column congratulation_pages.text is 'Текст';

create table if not exists students_courses
(
    id                                       serial
        constraint students_courses_pk
            primary key,
    users_id                                 bigint
        constraint students_courses_users_id_fk
            references users
            on update cascade on delete cascade,
    completeness_status                      boolean                  not null,
    stage_id                                 bigint
        constraint students_courses_stage_id_fk
            references stages
            on update cascade on delete set null,
    timing                                   integer,
    courses_id                               bigint
        constraint students_courses_courses_id_fk
            references courses
            on update cascade on delete cascade,
    last_action_date                         timestamp with time zone
);

comment on column students_courses.id is 'ID курса студента в БД';

comment on column students_courses.users_id is 'ID пользователя';

comment on column students_courses.completeness_status is 'Статус завершенности курса';

comment on column students_courses.stage_id is 'ID этапа, на котором остановился студент';

comment on column students_courses.timing is 'Тайминг (если остановился на видеолекции) в секундах от начала видео';

comment on column students_courses.courses_id is 'ID курса';

comment on column students_courses.last_action_date is 'Дата и время последнего взаимодействия пользователя с курсом';

create table if not exists favorite_pages
(
    id                                       serial
        constraint favorite_pages_pk
            primary key,
    email                                    varchar(100)             not null,
    page_url                                 varchar(255)             not null
);

comment on column favorite_pages.id is 'ID';

comment on column favorite_pages.email is 'Почта пользователя, интересующегося страницей';

comment on column favorite_pages.page_url is 'Интересующая страница';

create table if not exists features
(
    id                                       serial
        constraint features_pk
            primary key,
    release_date                              timestamp with time zone,
    description                              text                     not null
);

comment on column features.id is 'ID фичи';

comment on column features.release_date is 'Дата и время планируемого релиза фичи';

comment on column features.description is 'Описание фичи';

create table if not exists transactions
(
    id                                       serial
        constraint transactions_pk
            primary key,
    user_id                                  bigint
        constraint transactions_user_id_fk
            references users
            on update cascade on delete set null,
    tinkoff_payment_status                   varchar(50),
    amount                                   integer                  not null,
    error_code                               varchar(50),
    points                                   integer                  not null,
    creation_date                            timestamp with time zone,
    last_modified_date                       timestamp with time zone,
    payment_id                               varchar(50),
    payment_status                           varchar(50)
);

comment on column transactions.id is 'ID транзакции';

comment on column transactions.tinkoff_payment_status is 'Тинькофф-статус транзакции';

comment on column transactions.amount is 'Сумма заказа';

comment on column transactions.error_code is 'Код ошибки';

comment on column transactions.points is 'Количество баллов к начислению';

comment on column transactions.creation_date is 'Дата создания транзакции';

comment on column transactions.last_modified_date is 'Дата последнего изменения транзакции';

comment on column transactions.payment_id is 'ID платежа, полученный от Tinkoff';

comment on column transactions.payment_status is 'Статус транзакции';