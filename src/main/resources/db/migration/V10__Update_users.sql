alter table users
    add column unsubscribed boolean not null default false;

comment on column users.unsubscribed is 'Отписан ли пользователь от email-рассылки';