create sequence users_open_id_seq
    minValue 123
    start 123;

alter table users
    add column open_id integer not null default nextval('users_open_id_seq');

create unique index users_open_id_uindex
    on users (open_id);

alter sequence users_open_id_seq
    owned by users.open_id;

comment on column users.open_id is 'Открытый id пользователя для лидерборда и публичной страницы';