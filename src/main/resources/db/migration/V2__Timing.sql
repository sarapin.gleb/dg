alter table students_courses
    drop column stage_id,
    add  column last_viewed_stage bigint
         constraint students_courses_last_viewed_stage_fk
            references stages
            on update cascade on delete set null,
    add  column last_open_stage   bigint
         constraint students_courses_last_open_stage_fk
            references stages
            on update cascade on delete set null;

comment on column students_courses.last_viewed_stage is 'ID последнего этапа (только лекция с видео), просмотренного пользователем';

comment on column students_courses.timing is 'Тайминг в секундах от начала видео (последнего, просмотренного пользователем)';

comment on column students_courses.last_open_stage is 'ID последнего открытого этапа';